﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
	public abstract class ABaseGizmos : MonoBehaviour
	{
        /// ======================================================================

        public EDrawMode mode = EDrawMode.Always;
        public Color color = Color.yellow;

        /// ======================================================================

        protected void OnDrawGizmos()
        {
            if (mode == EDrawMode.Always)
                Draw();
        }
        protected void OnDrawGizmosSelected()
        {
            if (mode == EDrawMode.Selected)
                Draw();
        }
        protected abstract void Draw();

        /// ======================================================================

        public enum EDrawMode { Always, Selected }

        /// ======================================================================
    }
}