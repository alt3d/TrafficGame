﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
	public class ABoxGizmos : ABaseGizmos
    {
        /// ======================================================================

        private BoxCollider _volume;
        private BoxCollider volume
        {
            get
            {
                if (_volume == null) _volume = GetComponent<BoxCollider>();
                return _volume;
            }
        }

        /// ======================================================================

        protected override void Draw()
        {
            UDraw.Box(volume, color);
        }

        /// ======================================================================
    }
}