﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
	public class ACapsuleGizmos : ABaseGizmos
    {
        /// ======================================================================

        private CapsuleCollider _volume;
        private CapsuleCollider volume
        {
            get
            {
                if (_volume == null) _volume = GetComponent<CapsuleCollider>();
                return _volume;
            }
        }

        /// ======================================================================

        protected override void Draw()
        {
            UDraw.Capsule(volume, color);
        }

        /// ======================================================================
    }
}