﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
    public class ACircleGizmos : ABaseGizmos
    {
        /// ======================================================================

        public float radius = 1f;

        /// ======================================================================

        protected override void Draw()
        {
            UDraw.Circle(transform.position, transform.up, transform.right, radius, color);
        }

        /// ======================================================================
    }
}
