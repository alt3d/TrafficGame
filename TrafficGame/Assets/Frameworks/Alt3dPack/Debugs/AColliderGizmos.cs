﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
	public class AColliderGizmos : ABaseGizmos
	{
        /// ======================================================================

        private Collider _volume;
        private Collider volume
        {
            get
            {
                if (_volume == null) _volume = GetComponent<Collider>();
                return _volume;
            }
        }

        /// ======================================================================

        protected override void Draw()
        {
            if (volume is BoxCollider) UDraw.Box(volume as BoxCollider, color);
            if (volume is SphereCollider) UDraw.Sphere(volume as SphereCollider, color);
            if (volume is CapsuleCollider) UDraw.Capsule(volume as CapsuleCollider, color);
        }

        /// ======================================================================
    }
}