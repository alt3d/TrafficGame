﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
    public class APointGizmos : ABaseGizmos
    {
        /// ======================================================================

        public float size = 1f;

        /// ======================================================================

        protected override void Draw()
        {
            UDraw.Point(transform.position, transform.up, transform.right, size, color);
        }

        /// ======================================================================
    }
}