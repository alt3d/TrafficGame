﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.Pack.Debugs
{
	public class ASphereGizmos : ABaseGizmos
	{
        /// ======================================================================

        private SphereCollider _volume;
        private SphereCollider volume
        {
            get
            {
                if (_volume == null) _volume = GetComponent<SphereCollider>();
                return _volume;
            }
        }

        /// ======================================================================

        protected override void Draw()
        {
            UDraw.Sphere(volume, color);
        }

        /// ======================================================================
    }
}