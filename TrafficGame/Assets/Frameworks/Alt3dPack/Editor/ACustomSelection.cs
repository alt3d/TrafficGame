﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
 
namespace Alt3d.Pack.Editors
{
    [InitializeOnLoad]
    public static class ACustomSelection
    {
        /// ======================================================================

        private const string menuPath = "Tools/Custom Selection";
        private const string makeSelectableKey = "#&s";
        private const string makeUnselectableKey = "#&u";

        private static bool enabled;

        /// ======================================================================

        static ACustomSelection()
        {
            Start();
        }

        [MenuItem(menuPath + "/Start", true)]
        public static bool CanStart()
        {
            return !enabled; // we can only start if we're stopped
        }

        [MenuItem(menuPath + "/Start")]
        public static void Start()
        {
            if (!enabled) // Might be a redundant check, but just in case someone wants to start it from code
            {
                enabled = true;
                EditorApplication.update += Update;
            }
        }

        [MenuItem(menuPath + "/Stop", true)]
        public static bool CanStop()
        {
            return enabled; // we can only stop if we're started
        }

        [MenuItem(menuPath + "/Stop")]
        public static void Stop()
        {
            if (enabled)
            {
                enabled = false;
                EditorApplication.update -= Update;
            }
        }

        [MenuItem(menuPath + "/Make Selection Unselectable " + makeUnselectableKey)]
        public static void MakeUnselectable()
        {
            foreach (var go in Selection.gameObjects)
            {
                // Add it only if it's not there
                if (go.GetComponent<AUnselectableInScene>() == null)
                    go.AddComponent<AUnselectableInScene>();
            }
        }

        [MenuItem(menuPath + "/Make Selection Selectable " + makeSelectableKey, true)]
        public static bool CanMakeSelectable()
        {
            return !enabled;
        }

        [MenuItem(menuPath + "/Make Selection Selectable " + makeSelectableKey)]
        public static void MakeSelectable()
        {
            Action<Component> destroy = Application.isPlaying ?
                (Action<Component>)Object.Destroy : Object.DestroyImmediate;

            foreach (var go in Selection.gameObjects)
            {
                destroy(go.GetComponent<AUnselectableInScene>());
            }
        }

        private static void Update()
        {
            var gos = Selection.gameObjects;
            if (gos != null && gos.Length > 0)
            {
                Selection.objects = gos.Where(g => g.GetComponent<AUnselectableInScene>() == null).ToArray();
            }
        }

        /// ======================================================================

    }
}