﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Alt3d.Pack.Editors
{
	public class AMissingComponentsFinder : MonoBehaviour
	{
        /// ======================================================================

        private const string MenuRoot = "Tools/Missing Components/";

        /// ======================================================================

        [MenuItem(MenuRoot + "Search in scene", false)]
        public static void FindMissingReferencesInCurrentScene()
        {
            var sceneObjects = GetSceneObjects();
            var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            FindMissingReferences(scene.name, sceneObjects);
        }

        [MenuItem(MenuRoot + "Search in all scenes", false)]
        public static void MissingSpritesInAllScenes()
        {
            foreach (var scene in EditorBuildSettings.scenes.Where(s => s.enabled))
            {
                UnityEditor.SceneManagement.EditorSceneManager.OpenScene(scene.path);
                FindMissingReferencesInCurrentScene();
            }
        }

        [MenuItem(MenuRoot + "Search in assets", false)]
        public static void MissingSpritesInAssets()
        {
            var allAssets = AssetDatabase.GetAllAssetPaths().Where(path => path.StartsWith("Assets/")).ToArray();
            var objs = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

            FindMissingReferences("Project", objs);
        }

        private static void FindMissingReferences(string context, GameObject[] objects)
        {
            foreach (var go in objects)
            {
                var components = go.GetComponents<Component>();

                foreach (var c in components)
                {
                    // Missing components will be null, we can't find their type, etc.
                    if (!c)
                    {
                        Debug.LogError("Missing Component in GO: " + GetFullPath(go), go);
                        continue;
                    }

                    SerializedObject so = new SerializedObject(c);
                    var sp = so.GetIterator();

                    // Iterate over the components' properties.
                    while (sp.NextVisible(true))
                    {
                        if (sp.propertyType == SerializedPropertyType.ObjectReference)
                        {
                            if (sp.objectReferenceValue == null
                                && sp.objectReferenceInstanceIDValue != 0)
                            {
                                ShowError(context, go, c.GetType().Name, ObjectNames.NicifyVariableName(sp.name));
                            }
                        }
                    }
                }
            }
        }

        private static GameObject[] GetSceneObjects()
        {
            // Use this method since GameObject.FindObjectsOfType will not return disabled objects.
            return Resources.FindObjectsOfTypeAll<GameObject>()
                .Where(go => string.IsNullOrEmpty(AssetDatabase.GetAssetPath(go))
                       && go.hideFlags == HideFlags.None).ToArray();
        }

        private static void ShowError(string context, GameObject go, string componentName, string propertyName)
        {
            var ERROR_TEMPLATE = "Missing Reference in: [{3}]{0}. Component: {1}, Property: {2}";

            Debug.LogError(string.Format(ERROR_TEMPLATE, GetFullPath(go), componentName, propertyName, context), go);
        }

        private static string GetFullPath(GameObject go)
        {
            return go.transform.parent == null
                ? go.name
                    : GetFullPath(go.transform.parent.gameObject) + "/" + go.name;
        }

        /// ======================================================================
    }
}