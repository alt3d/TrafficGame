﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEditor;

namespace Alt3d.Pack.Editors
{
	public class AReplaceObjectsEditor : EditorWindow
	{
        /// ======================================================================

        private GameObject target;

        private bool isSavePos = true;
        private bool isSaveRot = true;
        private bool isSaveScale = true;
        private bool isSaveName = true;
        private bool isSaveParent = true;

        /// ======================================================================

        private void OnGUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            target = (GameObject)EditorGUILayout.ObjectField("Target:", target, typeof(GameObject), true);

            EditorGUILayout.Space();
            isSavePos = EditorGUILayout.Toggle("Save position:", isSavePos);
            isSaveRot = EditorGUILayout.Toggle("Save rotation:", isSaveRot);
            isSaveScale = EditorGUILayout.Toggle("Save scale:", isSaveScale);

            EditorGUILayout.Space();
            isSaveName = EditorGUILayout.Toggle("Save name:", isSaveName);
            isSaveParent = EditorGUILayout.Toggle("Save parent:", isSaveParent);

            EditorGUILayout.Space();
            if (GUILayout.Button("Replace Selected"))
                RepaceObjects();
        }
        private void RepaceObjects()
        {
            if (target == null)
            {
                Debug.Log("Target go is null");
                return;
            }

            foreach (var old in Selection.transforms)
            {
                var trn = Instantiate(target).transform;

                if (isSavePos)
                    trn.position = old.position;

                if (isSaveRot)
                    trn.rotation = old.rotation;

                if (isSaveScale)
                    trn.localScale = old.localScale;

                if (isSaveParent)
                    trn.parent = old.parent;

                if (isSaveName)
                    trn.gameObject.name = old.gameObject.name;

            }

            foreach (var go in Selection.gameObjects)
            {
                DestroyImmediate(go);
            }
        }

        [MenuItem("Tools/Replace Objects")]
        private static void OpenWindow()
        {
            GetWindow<AReplaceObjectsEditor>("Replace Objects");
        }

        /// ======================================================================
    }
}