using UnityEngine;
using UnityEditor;

namespace Alt3d.Pack.Editors
{
    [CustomEditor(typeof(Transform))]
    public class ATransformInspector : Editor
    {
        /// ======================================================================

        public override void OnInspectorGUI()
        {
            var trans = target as Transform;
            EditorGUIUtility.labelWidth = 15f;

            var pos = Vector3.zero;
            var rot = Vector3.zero;
            var scale = Vector3.zero;

            EditorGUILayout.BeginHorizontal();
            {
                if (DrawButton("P", "Reset Position", IsResetPositionValid(trans), 20f))
                {
                    Undo.RecordObject(trans, "Reset Position");
                    trans.localPosition = Vector3.zero;
                }
                pos = DrawVector3(trans.localPosition);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                if (DrawButton("R", "Reset Rotation", IsResetRotationValid(trans), 20f))
                {
                    Undo.RecordObject(trans, "Reset Rotation");
                    trans.localEulerAngles = Vector3.zero;
                }
                rot = DrawVector3(trans.localEulerAngles);
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            {
                if (DrawButton("S", "Reset Scale", IsResetScaleValid(trans), 20f))
                {
                    Undo.RecordObject(trans, "Reset Scale");
                    trans.localScale = Vector3.one;
                }
                scale = DrawVector3(trans.localScale);
            }
            EditorGUILayout.EndHorizontal();

            if (GUI.changed)
            {
                Undo.RecordObject(trans, "Transform Change");
                trans.localPosition = ValidateVector(pos);
                trans.localEulerAngles = ValidateVector(rot);
                trans.localScale = ValidateVector(scale);
            }
        }

        /// ======================================================================

        private static bool DrawButton(string title, string tooltip, bool enabled, float width)
        {
            if (enabled)
            {
                return GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
            }
            else
            {
                var color = GUI.color;
                GUI.color = new Color(1f, 1f, 1f, 0.25f);
                GUILayout.Button(new GUIContent(title, tooltip), GUILayout.Width(width));
                GUI.color = color;
                return false;
            }
        }
        private static Vector3 DrawVector3(Vector3 value)
        {
            var opt = GUILayout.MinWidth(30f);
            value.x = EditorGUILayout.FloatField("X", value.x, opt);
            value.y = EditorGUILayout.FloatField("Y", value.y, opt);
            value.z = EditorGUILayout.FloatField("Z", value.z, opt);
            return value;
        }

        private static bool IsResetPositionValid(Transform targetTransform)
        {
            var vector = targetTransform.localPosition;
            return (vector.x != 0f || vector.y != 0f || vector.z != 0f);
        }
        private static bool IsResetRotationValid(Transform targetTransform)
        {
            var vector = targetTransform.localEulerAngles;
            return (vector.x != 0f || vector.y != 0f || vector.z != 0f);
        }
        private static bool IsResetScaleValid(Transform targetTransform)
        {
            var vector = targetTransform.localScale;
            return (vector.x != 1f || vector.y != 1f || vector.z != 1f);
        }

        private static Vector3 ValidateVector(Vector3 vector)
        {
            vector.x = float.IsNaN(vector.x) ? 0f : vector.x;
            vector.y = float.IsNaN(vector.y) ? 0f : vector.y;
            vector.z = float.IsNaN(vector.z) ? 0f : vector.z;
            return vector;
        }

        /// ======================================================================
    }
}