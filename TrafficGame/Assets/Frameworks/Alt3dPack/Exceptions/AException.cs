﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AException : Exception
	{
		/// ======================================================================

        public AException(UnityEngine.Object target)
        {
            Debug.LogException(new Exception(), target);
        }
        public AException(string message, UnityEngine.Object target)
        {
            Debug.LogException(new Exception(message), target);
        }

		/// ======================================================================
	}
}