﻿using UnityEngine;

namespace Alt3d.Pack
{
    public class ACameraMouseLook : MonoBehaviour
    {
        /// ======================================================================
       
        public float speed = 20;
        public float sensitivityX = 15;
        public float sensitivityY = 15;

        private float minX = -360;
        private float maxX = 360;
        private float minY = -90;
        private float maxY = 90;

        private float rotX;
        private float rotY;

        private Quaternion originalRotation;

        /// ======================================================================

        private void Start()
        {
            AlignVertical();
        }
        private void LateUpdate()
        {
            if (Input.GetMouseButton(1)) Move();
        }

        /// ======================================================================

        private void AlignVertical()
        {
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            originalRotation = transform.rotation;
        }
        private void Move()
        {
            rotX += Input.GetAxis("Mouse X") * sensitivityX;
            rotY += Input.GetAxis("Mouse Y") * sensitivityY;

            rotX = ClampAngle(rotX, minX, maxX);
            rotY = ClampAngle(rotY, minY, maxY);

            var rotDeltaX = Quaternion.AngleAxis(rotX, Vector3.up);
            var rotDeltaY = Quaternion.AngleAxis(rotY, Vector3.left);
            transform.rotation = originalRotation * rotDeltaX * rotDeltaY;

            var acceleration = 1f;
            if (Input.GetKey(KeyCode.LeftShift)) acceleration = 5f;

            var posDelta = transform.forward * Input.GetAxis("Vertical");
            posDelta += transform.right * Input.GetAxis("Horizontal");
            posDelta *= Time.deltaTime * speed * acceleration;
            transform.position += posDelta;
        }

        private float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360F) angle += 360F;
            if (angle > 360F) angle -= 360F;
            return Mathf.Clamp(angle, min, max);
        }

        /// ======================================================================
    }
}