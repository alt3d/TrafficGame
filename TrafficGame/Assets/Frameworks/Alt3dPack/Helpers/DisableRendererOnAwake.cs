﻿using UnityEngine;

namespace Alt3d.Pack
{
	public class ARendererDeactivator : MonoBehaviour
	{
		/// ======================================================================

        private void Awake()
        {
            var renderer = gameObject.GetComponent<Renderer>();
            if (renderer != null) renderer.enabled = false;
        }

		/// ======================================================================
	}
}