﻿using UnityEngine;

namespace Alt3d.Pack
{
    public class APrefabCleaner : MonoBehaviour
    {
        /// ======================================================================

        [ContextMenu("Clear MonoBehaviours")]
        private void ClearMonoBehaviours()
        {
            var array = GetComponentsInChildren<MonoBehaviour>();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != this) DestroyImmediate(array[i]);
            }
        }

        [ContextMenu("Clear Colliders")]
        private void ClearColliders()
        {
            var array = GetComponentsInChildren<Collider>();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != this) DestroyImmediate(array[i]);
            }
        }

        /// ======================================================================
    }
}
