﻿using UnityEngine;

namespace Alt3d.Pack
{
	public class ATimeScaleController : MonoBehaviour
	{
		/// ======================================================================

        [Range(0, 10f)]
        public float scale = 1f;
        public bool isUseKeys;

		/// ======================================================================

        private void LateUpdate()
        {
            if (isUseKeys)
            {
                if (Input.GetKeyDown(KeyCode.RightArrow)) scale = 1f;
                if (Input.GetKeyDown(KeyCode.LeftArrow)) scale = 0f;

                if (Input.GetKeyDown(KeyCode.UpArrow)) scale *= 2f;
                if (Input.GetKeyDown(KeyCode.DownArrow)) scale *= 0.5f;
            }
            
            Time.timeScale = scale;
        }

		/// ======================================================================
	}
}