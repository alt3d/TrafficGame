﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    [Serializable]
	public class ACurve 
	{
        /// ======================================================================

        public AnimationCurve curve;
        public float factor;

        /// ======================================================================

        public float Evaluate(float value)
        {
            return curve.Evaluate(value) * factor;
        }

        /// ======================================================================
    }
}