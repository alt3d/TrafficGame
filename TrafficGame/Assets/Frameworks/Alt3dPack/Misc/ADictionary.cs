﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public class ADictionary<TKey, TValue>
    {
        /// ======================================================================

        private Dictionary<TKey, TValue> dic = new Dictionary<TKey, TValue>();
        private int count;

        /// ======================================================================

        public int Count
        {
            get { return count; }
        }

        /// ======================================================================

        public event Action<TKey, TValue> Added = delegate { };
        public event Action<TKey, TValue> Removed = delegate { };

        /// ======================================================================

        public void Add(TKey key, TValue value)
        {
            if (value == null) throw new NullReferenceException();
            if (dic.ContainsKey(key)) throw new InvalidOperationException(key.ToString());
            if (dic.ContainsValue(value)) throw new InvalidOperationException(value.ToString());

            dic.Add(key, value);
            count++;
            Added(key, value);
        }
        public void Remove(TKey key, TValue value)
        {
            if (value == null) throw new NullReferenceException();
            if (dic.ContainsKey(key) == false) throw new InvalidOperationException(key.ToString());

            dic.Remove(key);
            count--;
            Removed(key, value);
        }
        public void Clear()
        {
            dic.Clear();
        }

        /// ======================================================================

        public List<TKey> GetAllKeys()
        {
            return new List<TKey>(dic.Keys);
        }
        public List<TValue> GetAllValues()
        {
            return new List<TValue>(dic.Values);
        }
        public TValue Get(TKey key)
        {
            if (dic.ContainsKey(key)) return dic[key];
            else return default(TValue);
        }
        public bool HasKey(TKey key)
        {
            return dic.ContainsKey(key);
        }

        /// ======================================================================
    }
}