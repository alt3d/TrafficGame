﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    [Serializable]
    public class ADistanceMeasure 
    {
        /// ======================================================================

        public Transform pivot;
        public float maxDistance;
        private Vector3 prevPos;

        /// ======================================================================

        public event Action<float> MaxDistanceReached = delegate { }; 

        /// ======================================================================

        public void Update()
        {
            var distance = Vector3.Distance(pivot.position, prevPos);
            if (distance > maxDistance)
            {
                MaxDistanceReached(distance);
                prevPos = pivot.position;
            }
        }
        public void ForceUpdatePrevPosition()
        {
            prevPos = pivot.position;
        }

        /// ======================================================================
    }
}