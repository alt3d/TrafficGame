﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AList<T> 
	{
        /// ======================================================================

        private List<T> _list = new List<T>();
        private int _count;

        /// ======================================================================

        protected List<T> list
        {
            get { return _list; }
        }
        public int count
        {
            get { return _count; }
            private set { _count = value; }
        }

        /// ======================================================================

        public event Action<T> Added = delegate { };
        public event Action<T> Removed = delegate { };

        /// ======================================================================

        public void Add(T entity)
        {
            if (entity == null) throw new NullReferenceException();
            if (list.Contains(entity)) throw new InvalidOperationException(entity.ToString());

            list.Add(entity);
            count++;
            Added(entity);
        }
        public void Remove(T entity)
        {
            if (entity == null) throw new NullReferenceException();
            if (list.Contains(entity) == false) throw new InvalidOperationException();

            list.Remove(entity);
            count--;
            Removed(entity);
        }
        public void Clear()
        {
            count = 0;
            list.Clear();
        }

        /// ======================================================================

        public List<T> GetAll()
        {
            return new List<T>(list);
        }

        /// ======================================================================
    }
}