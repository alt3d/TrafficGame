﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public abstract class AMonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
        /// ======================================================================

        private static T _instance;
        public static T instance
        {
            get
            {
                if (_instance == null) _instance = UGameObject.FindSingleton<T>();
                return _instance;
            }
        }

        /// ======================================================================
    }
}