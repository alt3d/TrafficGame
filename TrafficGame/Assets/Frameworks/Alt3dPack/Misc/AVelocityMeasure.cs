﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    [Serializable]
    public class AVelocityMeasure
    {
        /// ======================================================================

        public Transform pivot;
        public Transform parent;

        [SerializeField] private AData data = new AData();
        private Vector3 dir;
        private Vector3 lastPos;
        private float dot;

        /// ======================================================================

        public Vector3 velocity
        {
            get { return data.velocity; }
        }
        public float speed
        {
            get { return data.speed; }
        }
        
        /// ======================================================================

        public void Update(float deltaTime)
        {
            dir = pivot.position - lastPos;
            data.velocity = dir / deltaTime;
            data.speed = data.velocity.magnitude;

            if (parent != null)
            {
                dot = Vector3.Dot(data.velocity.normalized, parent.forward);
                data.velocity *= dot;
                data.speed *= dot;
            }

            lastPos = pivot.position;
        }

        /// ======================================================================

        [Serializable]
        public class AData
        {
            public Vector3 velocity;
            public float speed;
        }

        /// ======================================================================
    }
}