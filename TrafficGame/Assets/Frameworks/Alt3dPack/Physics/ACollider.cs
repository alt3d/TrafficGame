﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class ACollider : MonoBehaviour
	{
        /// ======================================================================

        private Collider _cldr;
        public Collider cldr
        {
            get
            {
                if (_cldr == null) _cldr = GetComponent<Collider>();
                return _cldr;
            }
        }

        /// ======================================================================

        public event Action<ACollider, Collision> CollisionEnter = delegate { };
        public event Action<ACollider, Collision> CollisionExit = delegate { };

        /// ======================================================================

        private void OnCollisionEnter(Collision collision)
        {
            CollisionEnter(this, collision);
        }
        private void OnCollisionExit(Collision collision)
        {
            CollisionExit(this, collision);
        }

        /// ======================================================================
    }
}