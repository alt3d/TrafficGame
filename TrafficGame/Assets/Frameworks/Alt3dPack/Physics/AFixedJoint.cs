﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AFixedJoint : MonoBehaviour
	{
        /// ======================================================================

        private Rigidbody _body = null;
        private FixedJoint _joint;

        private Vector3 pos;
        private Vector3 angles;

        /// ======================================================================

        private Rigidbody body
        {
            get
            {
                if (_body == null) _body = GetComponent<Rigidbody>();
                return _body;
            }
        }
        private FixedJoint joint
        {
            get
            {
                if (_joint == null) _joint = gameObject.AddComponent<FixedJoint>();
                return _joint;
            }
        }
        public bool isBroken { get; private set; }

        /// ======================================================================

        public event Action<AFixedJoint> Break = delegate { };

        /// ======================================================================

        private void OnJointBreak(float breakForce)
        {
            isBroken = true;
            Break(this);
        }
        
        public void Setup()
        {
            SaveTransform();
        }
        public void Restore(float force)
        {
            isBroken = false;

            MakeKinematic();
            LoadTransform();

            AddJoint(force);
            MakePhysic();
        }

        public void AddJoint(float force)
        {
            joint.breakForce = force;
            joint.breakTorque = force;
        }
        public void AddJoint(float force, float torque)
        {
            joint.breakForce = force;
            joint.breakTorque = torque;
        }

        public void Attach(Rigidbody body)
        {
            joint.connectedBody = body;
        }
        public void Attach(AFixedJoint joint)
        {
            this.joint.connectedBody = joint.body;
        }

        public void MakeKinematic()
        {
            body.isKinematic = true;
            ResetVelocity();
        }
        public void MakePhysic()
        {
            body.isKinematic = false;
            ResetVelocity();
        }
        private void ResetVelocity()
        {
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
            body.Sleep();
        }

        public void SaveTransform()
        {
            pos = transform.localPosition;
            angles = transform.localEulerAngles;
        }
        public void LoadTransform()
        {
            transform.localPosition = pos;
            transform.localEulerAngles = angles;
        }

        /// ======================================================================
    }
}