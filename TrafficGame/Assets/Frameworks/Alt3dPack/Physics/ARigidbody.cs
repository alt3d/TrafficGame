﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class ARigidbody : MonoBehaviour
	{
        /// ======================================================================

        private Rigidbody _body;
        public Rigidbody body
        {
            get
            {
                if (_body == null) _body = GetComponent<Rigidbody>();
                return _body;
            }
        }

        /// ======================================================================

        public event Action<ARigidbody, Collision> CollisionEnter = delegate { };
        public event Action<ARigidbody, Collision> CollisionExit = delegate { };

		/// ======================================================================

        private void OnCollisionEnter(Collision collision)
        {
            CollisionEnter(this, collision);
        }
        private void OnCollisionExit(Collision collision)
        {
            CollisionExit(this, collision);
        }

		/// ======================================================================
	}
}