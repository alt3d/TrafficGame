﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public class ATrigger : MonoBehaviour
	{
        /// ======================================================================

        private Collider _cldr;
        public Collider cldr
        {
            get
            {
                if (_cldr == null) _cldr = GetComponent<Collider>();
                return _cldr;
            }
        }

        /// ======================================================================

        public event Action<ATrigger, Collider> TriggerEnter = delegate { };
        public event Action<ATrigger, Collider> TriggerExit = delegate { };

        /// ======================================================================

        private void OnTriggerEnter(Collider other)
        {
            TriggerEnter(this, other);
        }
        private void OnTriggerExit(Collider other)
        {
            TriggerExit(this, other);
        }

        /// ======================================================================
    }
}