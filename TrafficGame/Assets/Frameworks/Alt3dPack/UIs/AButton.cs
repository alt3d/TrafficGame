﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
    public class AButton : MonoBehaviour
    {
        /// ======================================================================

        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null) _button = GetComponent<Button>();
                return _button;
            }
        }

        /// ======================================================================

        public event Action<AButton> Clicked = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            button.onClick.AddListener(OnClicked);
        }
        private void OnDisable()
        {
            button.onClick.RemoveListener(OnClicked);
        }

        private void OnClicked()
        {
            Clicked(this);
        }

        /// ======================================================================
    }
}