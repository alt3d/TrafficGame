﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
	public class AButtonWithText : AButton
	{
        /// ======================================================================

        [SerializeField] protected Text _text;

        /// ======================================================================

        public Text text
        {
            get { return _text; }
        }

        /// ======================================================================
    }
}