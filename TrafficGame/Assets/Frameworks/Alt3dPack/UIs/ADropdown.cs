﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
	public class ADropdown : MonoBehaviour
	{
        /// ======================================================================

        private Dropdown _dropdown;
        public Dropdown dropdown
        {
            get
            {
                if (_dropdown == null) _dropdown = GetComponent<Dropdown>();
                return _dropdown;
            }
        }

        /// ======================================================================

        public event Action<ADropdown, int> ValueChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            dropdown.onValueChanged.AddListener(OnValueChanged);
        }
        private void OnDisable()
        {
            dropdown.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void OnValueChanged(int value)
        {
            ValueChanged(this, value);
        }
        
		/// ======================================================================
	}
}