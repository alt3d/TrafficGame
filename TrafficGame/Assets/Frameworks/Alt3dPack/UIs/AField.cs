﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
    public class AField : MonoBehaviour
    {
        /// ======================================================================

        private InputField _field;
        public InputField field
        {
            get
            {
                if (_field == null) _field = GetComponent<InputField>();
                return _field;
            }
        }

        /// ======================================================================

        public event Action<AField, string> ValueChanged = delegate { };
        public event Action<AField, string> EndEdit = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            field.onValueChanged.AddListener(OnValueChanged);
            field.onEndEdit.AddListener(OnEndEdit);
        }
        private void OnDisable()
        {
            field.onValueChanged.RemoveListener(OnValueChanged);
            field.onEndEdit.RemoveListener(OnEndEdit);
        }

        private void OnValueChanged(string value)
        {
            ValueChanged(this, value);
        }
        private void OnEndEdit(string value)
        {
            EndEdit(this, value);
        }

        /// ======================================================================
    }
}