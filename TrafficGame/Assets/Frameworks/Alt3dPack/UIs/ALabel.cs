﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
    public class ALabel : MonoBehaviour
    {
        /// ======================================================================

        private Text _text;
        public Text text
        {
            get
            {
                if (_text == null) _text = GetComponent<Text>();
                return _text;
            }
        }

        /// ======================================================================
    }
}