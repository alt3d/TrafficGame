﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
    public class ASlider : MonoBehaviour
    {
        /// ======================================================================

        private Slider _slider;
        public Slider slider
        {
            get
            {
                if (_slider == null) _slider = GetComponent<Slider>();
                return _slider;
            }
        }

        /// ======================================================================

        public event Action<ASlider, float> ValueChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            slider.onValueChanged.AddListener(OnValueChanged);
        }
        private void OnDisable()
        {
            slider.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void OnValueChanged(float value)
        {
            ValueChanged(this, value);
        }

        /// ======================================================================
    }
}