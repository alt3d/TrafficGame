﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
	public class AToggle : MonoBehaviour
	{
        /// ======================================================================

        private Toggle _toggle;
        public Toggle toggle
        {
            get
            {
                if (_toggle == null) _toggle = GetComponent<Toggle>();
                return _toggle;
            }
        }

        /// ======================================================================

        public event Action<AToggle, bool> ValueChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            toggle.onValueChanged.AddListener(OnValueChanged);
        }
        private void OnDisable()
        {
            toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        /// ======================================================================

        private void OnValueChanged(bool value)
        {
            ValueChanged(this, value);
        }

		/// ======================================================================
	}
}