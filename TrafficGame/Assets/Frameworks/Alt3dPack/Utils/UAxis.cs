﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public static class UAxis
    {
        /// ======================================================================

        public static Vector3 GetDirection(EAxis axis)
        {
            switch (axis)
            {
                case EAxis.Left: return Vector3.left;
                case EAxis.Right: return Vector3.right;
                case EAxis.Up: return Vector3.up;
                case EAxis.Down: return Vector3.down;
                case EAxis.Forward: return Vector3.forward;
                case EAxis.Back: return Vector3.back;
                default: throw new Exception(axis.ToString());
            }
        }

        public static EAxis GetOpposite(EAxis axis)
        {
            switch (axis)
            {
                case EAxis.Left: return EAxis.Right;
                case EAxis.Right: return EAxis.Left;
                case EAxis.Up: return EAxis.Down;
                case EAxis.Down: return EAxis.Up;
                case EAxis.Forward: return EAxis.Back;
                case EAxis.Back: return EAxis.Forward;
                default: throw new Exception(axis.ToString());
            }
        }

		/// ======================================================================
	}

    /// ======================================================================

    public enum EAxis
    {
        Up,
        Down,
        Left,
        Right,
        Forward,
        Back
    }

    /// ======================================================================
}