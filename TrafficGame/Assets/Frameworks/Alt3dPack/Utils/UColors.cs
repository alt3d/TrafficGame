﻿using UnityEngine;

namespace Alt3d
{
    public static class UColors
    {
        /// ======================================================================

        public static readonly Color Red = Color.red;
        public static readonly Color Green = Color.green;
        public static readonly Color Blue = Color.blue;
        public static readonly Color Black = Color.black;
        public static readonly Color White = Color.white;
        public static readonly Color Gray = Color.gray;
        public static readonly Color Magenta = Color.magenta;
        public static readonly Color Cyan = Color.cyan;
        public static readonly Color Yellow = new Color(1, 1, 0);
        public static readonly Color Orange = new Color(1, 165f / 255f, 0);
        public static readonly Color Brown = new Color(150f / 256f, 75f / 256f, 0);
        public static readonly Color Pink = new Color(253f / 256f, 123f / 256f, 124f / 256f);
        public static readonly Color Purple = new Color(139f / 256f, 0, 1);

        /// ======================================================================

        public static Color GetRandom()
        {
            var random = Random.Range(0, 9);
            switch (random)
            {
                case 0: return Red;
                case 1: return Green;
                case 2: return Blue;
                case 3: return Magenta;
                case 4: return Cyan;
                case 5: return Yellow;
                case 6: return Orange;
                case 7: return Brown;
                case 8: return Purple;
                default: return Red;
            }
        }
        public static Color Invert(Color color)
        {
            var result = new Color();
            result.r = 1f - color.r;
            result.g = 1f - color.g;
            result.b = 1f - color.b;
            result.a = color.a;
            return result;
        }

        /// ======================================================================
    }
}