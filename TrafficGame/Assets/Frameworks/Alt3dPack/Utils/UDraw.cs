﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Alt3d
{
    public static class UDraw
    {
        /// ======================================================================

        public static void Line(Vector3 start, Vector3 end, Color color) { DrawLine(start, end, color); }

        /// ======================================================================
        // Rays

        public static void Ray(Vector3 origin, Vector3 dir, float length, Color color) { DrawRay(origin, dir, length, color); }
        public static void Ray(Vector3 origin, Vector3 dir, Color color) { DrawRay(origin, dir, Default.size, color); }

        /// ======================================================================
        // Points

        public static void Point(Vector3 center, Vector3 normal, Vector3 right, float size, Color color) { DrawPoint(center, normal, right, size, color); }
        public static void Point(Vector3 center, Vector3 normal, Vector3 right, Color color) { DrawPoint(center, normal, right, Default.size, color); }

        public static void Point(Vector3 center, float size, Color color) { DrawPoint(center, Vector3.up, Vector3.right, size, color); }
        public static void Point(Vector3 center, Color color) { DrawPoint(center, Vector3.up, Vector3.right, Default.size, color); }

        /// ======================================================================
        // PolyLines and Shapes

        public static void PolyLine(Vector3[] points, Color color) { DrawPolyLine(points, color, false); }
        public static void Shape(Vector3[] points, Color color) { DrawPolyLine(points, color, true); }

        /// ======================================================================
        // Rects and Quads

        public static void Rect(Vector3 center, Vector3 normal, Vector3 right, float height, float width, Color color) { DrawRect(center, normal, right, height, width, color); }
        public static void Rect(Vector3 center, float height, float width, Color color) { DrawRect(center, Vector3.up, Vector3.right, height, width, color); }

        public static void Quad(Vector3 center, Vector3 normal, Vector3 right, float size, Color color) { DrawRect(center, normal, right, size, size, color); }
        public static void Quad(Vector3 center, float size, Color color) { DrawRect(center, Vector3.up, Vector3.right, size, size, color); }
        public static void Quad(Vector3 center, Color color) { DrawRect(center, Vector3.up, Vector3.right, Default.size, Default.size, color); }

        /// ======================================================================
        // Circles

        public static void Circle(Vector3 center, Vector3 normal, Vector3 right, float radius, Color color) { DrawCircle(center, normal, right, radius, color); }
        public static void Circle(Vector3 center, Vector3 normal, Vector3 right, Color color) { DrawCircle(center, normal, right, Default.radius, color); }

        public static void Circle(Vector3 center, float radius, Color color) { DrawCircle(center, Vector3.up, Vector3.right, radius, color); }
        public static void Circle(Vector3 center, Color color) { DrawCircle(center, Vector3.up, Vector3.right, Default.radius, color); }

        /// ======================================================================
        // Spheres

        public static void Sphere(Vector3 center, Vector3 normal, Vector3 right, float radius, Color color) { DrawSphere(center, normal, right, radius, color); }
        public static void Sphere(Vector3 center, Vector3 normal, Vector3 right, Color color) { DrawSphere(center, normal, right, Default.radius, color); }

        public static void Sphere(Vector3 center, float radius, Color color) { DrawSphere(center, Vector3.up, Vector3.right, radius, color); }
        public static void Sphere(Vector3 center, Color color) { DrawSphere(center, Vector3.up, Vector3.right, Default.radius, color); }

        /// ======================================================================
        // Boxes

        public static void Box(Vector3 center, Vector3 normal, Vector3 right, float height, float width, float up, Color color) { DrawBox(center, normal, right, height, width, up, color); }
        public static void Box(Vector3 center, Vector3 normal, Vector3 right, Color color) { DrawBox(center, normal, right, Default.size, Default.size, Default.size, color); }

        public static void Box(Vector3 center, float height, float width, float up, Color color) { DrawBox(center, Vector3.up, Vector3.right, height, width, up, color); }
        public static void Box(Vector3 center, Color color) { DrawBox(center, Vector3.up, Vector3.right, Default.size, Default.size, Default.size, color); }

        /// ======================================================================
        // Components

        public static void Capsule(Vector3 center, Vector3 forward, Vector3 right, float radius, float height, Color color) { DrawCapsule(center, forward, right, radius, height, color); }
        public static void Capsule(Vector3 center, float radius, float height, Color color) { DrawCapsule(center, Vector3.forward, Vector3.right, radius, height, color); }

        /// ======================================================================
        // Components

        public static void PolyLine(Component[] array, Color color)
        {
            var points = new Vector3[array.Length];
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = array[i].transform.position;
            }
            DrawPolyLine(points, color, false);
        }
        public static void Shape(Component[] array, Color color)
        {
            var points = new Vector3[array.Length];
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = array[i].transform.position;
            }
            DrawPolyLine(points, color, true);
        }

        /// ======================================================================
        // Colliders

        public static void Sphere(SphereCollider sphere, Color color)
        {
            var trn = sphere.transform;
            var center = trn.position + trn.TransformVector(sphere.center);
            var normal = trn.up;
            var right = trn.right;
            var radius = sphere.radius;
            DrawSphere(center, normal, right, radius, color);
        }
        public static void Box(BoxCollider box, Color color)
        {
            var trn = box.transform;
            var center = trn.position + trn.TransformVector(box.center);
            var normal = trn.up;
            var right = trn.right;
            var height = box.size.z;
            var width = box.size.x;
            var up = box.size.y;
            DrawBox(center, normal, right, height, width, up, color);
        }
        public static void Capsule(CapsuleCollider capsule, Color color)
        {
            var trn = capsule.transform;
            var center = trn.position + trn.TransformVector(capsule.center);
            var forward = trn.forward;
            var right = trn.right;
            var radius = capsule.radius;
            var height = capsule.height;

            switch (capsule.direction)
            {
                case 0:
                    forward = trn.right;
                    right = -trn.forward;
                    break;
                case 1:
                    forward = trn.up;
                    right = trn.right;
                    break;
                case 2:
                    forward = trn.forward;
                    right = trn.right;
                    break;
                default:
                    throw new ArgumentException(capsule.direction.ToString());
            }

            DrawCapsule(center, forward, right, radius, height, color);
        }

        /// ======================================================================

        private class Default
        {
            public static readonly float size = 1f;
            public static readonly float radius = 0.5f;
            public static readonly int circlePointsCount = 24;
        }

        /// ======================================================================

        private static void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            Debug.DrawLine(start, end, color);
        }
        private static void DrawRay(Vector3 origin, Vector3 dir, float length, Color color)
        {
            var end = origin + dir.normalized * length;
            DrawLine(origin, end, color);
        }
        private static void DrawPoint(Vector3 center, Vector3 normal, Vector3 right, float size, Color color)
        {
            var halfSize = size * 0.5f;
            var halfRight = right.normalized * halfSize;
            var halfUp = normal.normalized * halfSize;
            var halfForward = (Vector3.Cross(right, normal)).normalized * halfSize;

            DrawLine(center - halfRight, center + halfRight, color);
            DrawLine(center - halfUp, center + halfUp, color);
            DrawLine(center - halfForward, center + halfForward, color);
        }
        private static void DrawPolyLine(Vector3[] points, Color color, bool closed)
        {
            for (int i = 0; i < points.Length - 1; i++)
            {
                DrawLine(points[i], points[i + 1], color);
            }
            if (closed)
            {
                DrawLine(points[points.Length - 1], points[0], color);
            }
        }

        private static void DrawRect(Vector3 center, Vector3 normal, Vector3 right, float height, float width, Color color)
        {
            var halfRight = right.normalized * width * 0.5f;
            var halfForward = (Vector3.Cross(right, normal)).normalized * height * 0.5f;

            var points = new Vector3[4];
            points[0] = center - halfRight - halfForward;
            points[1] = center - halfRight + halfForward;
            points[2] = center + halfRight + halfForward;
            points[3] = center + halfRight - halfForward;

            DrawPolyLine(points, color, true);
        }
        private static void DrawCircle(Vector3 center, Vector3 normal, Vector3 right, float radius, Color color)
        {
            var pointsCount = Default.circlePointsCount;
            var angleDelta = 360f / pointsCount;
            var offset = right.normalized * radius;

            var points = new Vector3[pointsCount];
            for (int i = 0; i < pointsCount; i++)
            {
                points[i] = center + (Quaternion.AngleAxis(angleDelta * i, normal)) * offset;
            }

            DrawPolyLine(points, color, true);
        }

        private static void DrawSphere(Vector3 center, Vector3 normal, Vector3 right, float radius, Color color)
        {
            normal.Normalize();
            right.Normalize();
            var forward = (Vector3.Cross(right, normal)).normalized;

            DrawCircle(center, normal, right, radius, color);
            DrawCircle(center, forward, normal, radius, color);
            DrawCircle(center, right, forward, radius, color);
        }
        private static void DrawBox(Vector3 center, Vector3 normal, Vector3 right, float height, float width, float up, Color color)
        {
            var halfUp = normal.normalized * up * 0.5f;
            var halfRight = right.normalized * width * 0.5f;
            var halfForward = (Vector3.Cross(right, normal)).normalized * height * 0.5f;

            {
                var lowCenter = center - halfUp;
                var upCenter = center + halfUp;

                DrawRect(lowCenter, normal, right, height, width, color);
                DrawRect(upCenter, normal, right, height, width, color);
            }

            {
                var leftCenter = center - halfRight;
                var rightCenter = center + halfRight;

                var points = new Vector3[8];
                points[0] = leftCenter - halfUp - halfForward;
                points[1] = leftCenter + halfUp - halfForward;
                points[2] = leftCenter - halfUp + halfForward;
                points[3] = leftCenter + halfUp + halfForward;

                points[4] = rightCenter - halfUp - halfForward;
                points[5] = rightCenter + halfUp - halfForward;
                points[6] = rightCenter - halfUp + halfForward;
                points[7] = rightCenter + halfUp + halfForward;

                DrawLine(points[0], points[1], color);
                DrawLine(points[2], points[3], color);
                DrawLine(points[4], points[5], color);
                DrawLine(points[6], points[7], color);
            }
        }
        private static void DrawCapsule(Vector3 center, Vector3 forward, Vector3 right, float radius, float height, Color color)
        {
            forward.Normalize();
            right.Normalize();
            var up = (Vector3.Cross(forward, right)).normalized;

            var halfForward = forward * height * 0.5f;
            var halfUp = up * radius;
            var halfRight = right * radius;

            var forwardCenter = center + halfForward - forward * radius;
            var backCenter = center - halfForward + forward * radius;

            DrawSphere(forwardCenter, up, right, radius, color);
            DrawSphere(backCenter, up, right, radius, color);

            var points = new Vector3[8];
            points[0] = backCenter - halfUp;
            points[1] = forwardCenter - halfUp;
            points[2] = backCenter + halfUp;
            points[3] = forwardCenter + halfUp;
            points[4] = backCenter - halfRight;
            points[5] = forwardCenter - halfRight;
            points[6] = backCenter + halfRight;
            points[7] = forwardCenter + halfRight;

            DrawLine(points[0], points[1], color);
            DrawLine(points[2], points[3], color);
            DrawLine(points[4], points[5], color);
            DrawLine(points[6], points[7], color);
        }

        /// ======================================================================
    }
}