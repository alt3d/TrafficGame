﻿using System;
using System.Collections.Generic;

namespace Alt3d
{
	public static class UEnums 
	{
		/// ======================================================================

        public static T Parse<T>(string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch
            {
                throw new InvalidCastException(value +  " => " + typeof(T).ToString());
            }
        }
        public static T GetRandom<T>()
        {
            try
            {
                var array = GetAllAsArray<T>();
                var random = UnityEngine.Random.Range(0, array.Length);
                return array[random];
            }
            catch
            {
                throw new InvalidOperationException(typeof(T).ToString());
            }
        }

        public static ICollection<T> GetAllAsCollection<T>()
        {
            try
            {
                var array = Enum.GetValues(typeof(T));
                var result = new List<T>();
                foreach (var elem in array)
                {
                    result.Add((T)elem);
                }
                return result;
            }
            catch
            {
                throw new InvalidOperationException(typeof(T).ToString());
            }
        }
        public static T[] GetAllAsArray<T>()
        {
            try
            {
                var array = Enum.GetValues(typeof(T));
                var result = new T[array.Length];

                int i = 0;
                foreach (var elem in array)
                {
                    result[i] = (T)elem;
                    i++;
                }
                return result;
            }
            catch
            {
                throw new InvalidOperationException(typeof(T).ToString());
            }
        }

        /// ======================================================================
    }
}