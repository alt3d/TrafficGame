﻿using UnityEngine;
using UnityEngine.UI;

namespace Alt3d
{
	public static class UGUI
	{
		/// ======================================================================

        public static void ResizeAndAlignLayoutGroupToLeft(GridLayoutGroup gridLayoutGroup)
        {
            var gridTransform = gridLayoutGroup.GetComponent<RectTransform>();

            var cellsCount = gridTransform.childCount;
            var cellWidth = gridLayoutGroup.cellSize.x;
            var cellSpacing = gridLayoutGroup.spacing.x;

            var oldWidth = gridTransform.sizeDelta.x;
            var newWidth = cellWidth * cellsCount + cellSpacing * (cellsCount - 1);
            var right = newWidth - oldWidth;

            gridTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, right, newWidth);
        }

		/// ======================================================================
	}
}