﻿using System;
using System.Reflection;
using UnityEngine;

namespace Alt3d
{
    public static class UGameObject
    {
        /// ======================================================================

        public static GameObject Instantiate(GameObject prefab)
        {
            if (prefab == null) throw new NullReferenceException();

            var result = GameObject.Instantiate<GameObject>(prefab);
            result.name = prefab.name;
            return result;

        }
        public static GameObject LoadAsset(string path)
        {
            var prefab = Resources.Load<GameObject>(path);
            if (prefab != null) return Instantiate(prefab);
            else throw new NullReferenceException("Object not found at resources: " + path);
        }

        public static Transform[] GetChilds(Transform trn)
        {
            var count = trn.childCount;
            var result = new Transform[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = trn.GetChild(i);
            }
            return result;
        }
        public static void DeleteChilds(Transform trn)
        {
            var childs = GetChilds(trn);
            for (int i = 0; i < childs.Length; i++)
            {
                childs[i].gameObject.SetActive(false);
                GameObject.Destroy(childs[i].gameObject);
            }
        }
        public static void DeleteChildsImmediate(Transform trn)
        {
            var childs = GetChilds(trn);
            for (int i = 0; i < childs.Length; i++)
            {
                childs[i].gameObject.SetActive(false);
                GameObject.DestroyImmediate(childs[i].gameObject);
            }
        }

        /// ======================================================================

        public static T GetComponent<T>(Component cmp) where T : Component
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponent<T>();
            if (result != null) return result;

            throw new NullReferenceException("Component not not found: " + cmp.ToString());
        }
        public static T GetComponentOnSelfOrParent<T>(Component cmp) where T : MonoBehaviour
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponent<T>();
            if (result != null) return result;

            result = cmp.GetComponentInParent<T>();
            if (result != null) return result;

            throw new NullReferenceException("Component not not found on self or parent: " + cmp.name);
        }

        public static T GetComponentInChild<T>(Component cmp) where T : Component
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponentInChildren<T>();
            if (result != null) return result;
            else throw new NullReferenceException("Components in childs not found: " + cmp.name);
        }
        public static T GetComponentInParent<T>(Component cmp) where T : Component
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponentInParent<T>();
            if (result != null) return result;
            else throw new NullReferenceException("Component in parents not found: " + cmp.name);
        }

        public static T[] GetComponentsArrayInChild<T>(Component cmp) where T : Component
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponentsInChildren<T>();
            if (result.Length > 0) return result;
            else throw new NullReferenceException("Components in childs not found: " + cmp.name);
        }
        public static T[] GetComponentsArrayInParent<T>(Component cmp) where T : Component
        {
            if (cmp == null) throw new NullReferenceException();

            var result = cmp.GetComponentsInParent<T>();
            if (result.Length > 0) return result;
            else throw new NullReferenceException("Components in parents not found: " + cmp.name);
        }

        /// ======================================================================

        public static T FindSingleton<T>() where T : Component
        {
            var array = GameObject.FindObjectsOfType<T>();
            if (array.Length == 0) throw new NullReferenceException("Singleton not found: " + typeof(T).ToString());
            if (array.Length > 1) throw new InvalidOperationException("Singleton not one: " + typeof(T).ToString());
            return array[0];
        }
        public static GameObject FindGo(string name)
        {
            var result = GameObject.Find(name);
            if (result != null) return result;
            else throw new NullReferenceException("GameObject not found: " + name);
        }
        public static Transform FindTrn(string name)
        {
            var result = GameObject.Find(name).transform;
            if (result != null) return result;
            else throw new NullReferenceException("Transform not found: " + name);
        }

        public static string GetFullName(GameObject go)
        {
            var path = "/" + go.name;
            while (go.transform.parent != null)
            {
                go = go.transform.parent.gameObject;
                path = "/" + go.name + path;
            }
            return path;
        }
        public static GameObject[] GetAllRoots()
        {
            return UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        }

        /// ======================================================================
    }
}