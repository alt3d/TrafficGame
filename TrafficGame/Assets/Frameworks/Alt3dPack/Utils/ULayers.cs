﻿using UnityEngine;

namespace Alt3d
{
    public static class ULayers
    {
        /// ======================================================================

        public static bool IsMaskContains(LayerMask mask, Component cmp)
        {
            return IsMaskContains(mask, cmp.gameObject.layer);
        }
        public static bool IsMaskContains(int mask, Component cmp)
        {
            return IsMaskContains(mask, cmp.gameObject.layer);
        }

        public static bool IsMaskContains(LayerMask mask, int layer)
        {
            return IsMaskContains(mask.value, layer);
        }
        public static bool IsMaskContains(int mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }

        /// ======================================================================
    }
}