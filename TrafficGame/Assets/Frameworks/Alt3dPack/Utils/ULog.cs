﻿using UnityEngine;

namespace Alt3d
{
    public static class ULog
    {
        /// ======================================================================

        private static readonly EColor normalColor = EColor.Green;
        private static readonly EColor warningColor = EColor.Yellow;
        private static readonly EColor errorColor = EColor.Red;
        private static readonly EColor secondLineColor = EColor.Grey;

        private static readonly string normalColorText = normalColor.ToString().ToLower();
        private static readonly string warningColorText = warningColor.ToString().ToLower();
        private static readonly string errorColorText = errorColor.ToString().ToLower();
        private static readonly string secondLineColorText = secondLineColor.ToString().ToLower();

        private static int tabsCount = 0;
        private static readonly string tabsText = "    ";

        private static bool isList = false;
        private static int listIndex = 0;
        private static readonly string listDelimiter = ". ";

        private static readonly string lineText = "=================================================";
        private static bool isLine = false;

        /// ======================================================================

        public static void Normal(object first, object second = null, Object context = null)
        {
            OutMessageToConsole(EKind.Normal, first, second, context);
        }
        public static void Warning(object first, object second = null, Object context = null)
        {
            OutMessageToConsole(EKind.Warning, first, second, context);
        }
        public static void Error(object first, object second = null, Object context = null)
        {
            OutMessageToConsole(EKind.Error, first, second, context);
        }

        public static void TabsRight()
        {
            tabsCount++;
        }
        public static void TabsLeft()
        {
            tabsCount--;
        }

        public static void StartList()
        {
            isList = true;
            listIndex = 0;
        }
        public static void FinishList()
        {
            isList = false;
        }

        public static void Line()
        {
            // Если уже выведена линия - ничего не делаем
            if (isLine == true) return;

            // Выводим линию в консоль
            OutMessageToConsole(EKind.Normal, lineText, null, null);

            // Выставляем флаг линии
            isLine = true;
        }

        /// ======================================================================
        // Методы

        private static void OutMessageToConsole(EKind kind, object first, object second = null, Object context = null)
        {
            var firstLineText = first.ToString();
            var secondLineText = second != null ? second.ToString() : string.Empty;

            firstLineText = string.Format("<color={0}>{1}{2}{3}</color>", GetColorText(kind), GetTabsText(), GetListText(), firstLineText);
            secondLineText = string.Format("<color={0}>{1}{2}</color>", secondLineColorText, GetTabsText(), secondLineText);
            var resultText = string.Format("{0}\n{1}", firstLineText, secondLineText);

            switch (kind)
            {
                case EKind.Normal:
                    Debug.Log(resultText, context);
                    break;
                case EKind.Warning:
                    Debug.LogWarning(resultText, context);
                    break;
                case EKind.Error:
                    Debug.LogError(resultText, context);
                    break;
            }

            isLine = false;
        }

        private static string GetListText()
        {
            if (isList == false) return string.Empty;

            listIndex++;

            var result = listIndex.ToString();
            if (result.Length == 1) result = "0" + result;

            result += listDelimiter;
            return result;
        }
        private static string GetTabsText()
        {
            if (tabsCount == 0) return string.Empty;

            var result = string.Empty;
            for (int i = 0; i < tabsCount; i++)
            {
                result += tabsText;
            }

            return result;
        }
        private static string GetColorText(EKind kind)
        {
            switch (kind)
            {
                case EKind.Normal: return normalColorText;
                case EKind.Warning: return warningColorText;
                case EKind.Error: return errorColorText;
                default: throw new System.ArgumentException(kind.ToString());
            }
        }

        /// ======================================================================

        private enum EKind
        {
            Normal,
            Warning,
            Error
        }

        public enum EColor
        {
            Red,
            Green,
            Yellow,
            Grey,
            Black,
            White
        }

        /// ======================================================================
    }
}