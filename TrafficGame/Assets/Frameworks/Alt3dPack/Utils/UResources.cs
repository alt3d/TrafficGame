﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public static class UResources
    {
        /// ======================================================================

        public static T[] LoadAll<T>(string path) where T : ScriptableObject
        {
            var result = Resources.LoadAll<T>(path);
            if (result.Length == 0) throw new Exception("No resources in path: " + path + "\n" + typeof(T).ToString());
            return result;
        }
        public static T Load<T>(string path) where T : ScriptableObject
        {
            var result = Resources.Load<T>(path);
            if (result == null) throw new Exception("No resource in path: " + path + "\n" + typeof(T).ToString());
            return result;
        }

        public static UnityEngine.Object Load(string path)
        {
            var result = Resources.Load(path);
            if (result == null) throw new Exception("No resource in path: " + path);
            return result;
        }

		/// ======================================================================
	}
}