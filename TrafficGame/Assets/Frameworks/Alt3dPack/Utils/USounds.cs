﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
    public static class USounds
    {
        /// ======================================================================

        public static float GetRandomTime(AudioClip clip)
        {
            if (clip == null)
                throw new Exception();

            return UnityEngine.Random.Range(0, clip.length);
        }
        public static float GetRandomTime(AudioClip clip, float startOffset, float endOffset)
        {
            if (clip == null)
                throw new Exception();

            var min = startOffset;
            var max = clip.length - endOffset;

            if (min < 0)
                throw new Exception("Min: " + min);

            if (max < 0)
                throw new Exception("Max: " + max + " | Length: " + clip.length +  " | Start offset: " + startOffset);

            return UnityEngine.Random.Range(min, max);
        }

		/// ======================================================================
	}
}