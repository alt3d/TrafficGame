﻿using System;
using UnityEngine;

namespace Alt3d
{
    public static class UText
    {
        /// ======================================================================

        public static string AddLine(object text)
        {
            return AddLine(text, string.Empty);
        }
        public static string AddLine(object text, object line)
        {
            if (text == null) throw new NullReferenceException();
            if (line == null) throw new NullReferenceException();

            if (text.ToString() == string.Empty) return line.ToString();
            else return text.ToString() + "\n" + line.ToString();
        }

        /// ======================================================================

        public static string Format(object value, int count)
        {
            switch (count)
            {
                case 1: return Format1(value);
                case 2: return Format2(value);
                case 3: return Format3(value);
                default: throw new ArgumentException(count.ToString());
            }
        }
        public static string Format1(object value)
        {
            return string.Format("{0:0.0}", value);
        }
        public static string Format2(object value)
        {
            return string.Format("{0:00.00}", value);
        }
        public static string Format3(object value)
        {
            return string.Format("{0:000.000}", value);
        }

        /// ======================================================================

        public static bool ParseBool(string value)
        {
            if (value == "True") return true;
            if (value == "true") return true;
            if (value == "TRUE") return true;
            else return false;
        }
        public static float ParseFloat(string value)
        {
            try
            {
                return float.Parse(value);
            }
            catch
            {
                throw new ArgumentException(value);
            }
        }
        public static int ParseInt(string value)
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                throw new ArgumentException(value);
            }
        }
        public static Vector3 ParseVector3(string value)
        {
            value = value.Replace("(", "");
            value = value.Replace(")", "");

            var temp = value.Split(new[] { ',' });
            if (temp.Length < 3) throw new InvalidOperationException(value);

            var result = Vector3.zero;
            result.x = float.Parse(temp[0]);
            result.y = float.Parse(temp[1]);
            result.z = float.Parse(temp[2]);
            return result;
        }
        public static T ParseEnum<T>(string value)
        {
            return UEnums.Parse<T>(value);
        }

        /// ======================================================================
    }
}