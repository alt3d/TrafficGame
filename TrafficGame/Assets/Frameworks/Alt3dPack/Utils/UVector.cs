﻿using UnityEngine;

namespace Alt3d
{
    public static class UVector
    {
        /// ======================================================================

        public static Vector3 GetRandomDirection()
        {
            var x = Random.Range(-0.5f, 0.5f);
            var y = Random.Range(-0.5f, 0.5f);
            var z = Random.Range(-0.5f, 0.5f);
            return new Vector3(x, y, z).normalized;
        }
        public static Vector3 GetRandomDirection2D()
        {
            var x = Random.Range(-0.5f, 0.5f);
            var y = Random.Range(-0.5f, 0.5f);
            return new Vector3(x, y, 0).normalized;
        }

        /// ======================================================================
    }
}