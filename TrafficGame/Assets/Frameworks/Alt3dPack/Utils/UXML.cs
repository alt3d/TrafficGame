﻿using System;
using System.Xml;
using UnityEngine;

namespace Alt3d
{
    public static class UXML
    {
        /// ======================================================================

        public static XmlDocument LoadFromResources(string path)
        {
            var textAsset = Resources.Load<TextAsset>(path);
            if (textAsset != null)
            {
                var result = new XmlDocument();
                result.LoadXml(textAsset.text);
                return result;
            }
            else
            {
                throw new ArgumentException(path);
            }
        }

        /// ======================================================================

        public static XmlNode AddNode(string name, XmlNode parent)
        {
            if (name == "" || name == string.Empty) throw new ArgumentException();
            if (parent == null) throw new ArgumentException();

            var result = parent.OwnerDocument.CreateElement(name);
            parent.AppendChild(result);
            return result;
        }
        public static XmlNode AddNode(string name, XmlNode parent, object value)
        {
            var node = AddNode(name, parent);
            SetValue(node, value);
            return node;
        }
        private static void SetValue(XmlNode node, object value)
        {
            if (node == null) throw new ArgumentException();
            if (value == null) throw new ArgumentException();
            node.InnerText = value.ToString();
        }

        /// ======================================================================

        public static string GetStringInChild(string name, XmlNode parent)
        {
            return FindInnerInChild(name, parent);
        }
        public static bool GetBoolInChild(string name, XmlNode parent)
        {
            var value = FindInnerInChild(name, parent);
            return UText.ParseBool(value);
        }
        public static int GetIntInChild(string name, XmlNode parent)
        {
            var value = FindInnerInChild(name, parent);
            return UText.ParseInt(value);
        }
        public static float GetFloatInChild(string name, XmlNode parent)
        {
            var value = FindInnerInChild(name, parent);
            return UText.ParseFloat(value);
        }
        public static Vector3 GetVector3InChild(string name, XmlNode parent)
        {
            var value = FindInnerInChild(name, parent);
            return UText.ParseVector3(value);
        }
        public static T GetEnumInChild<T>(string name, XmlNode parent)
        {
            var value = FindInnerInChild(name, parent);
            return UText.ParseEnum<T>(value);
        }

        private static string FindInnerInChild(string name, XmlNode parent)
        {
            if (parent == null) throw new NullReferenceException(parent.ToString());
            if (name == "" || name == string.Empty) throw new ArgumentException();
            return parent.SelectSingleNode(name).InnerText;
        }

        /// ======================================================================
    }
}