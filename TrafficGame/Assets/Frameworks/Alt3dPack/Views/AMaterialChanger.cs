﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AMaterialChanger : MonoBehaviour
	{
        /// ======================================================================

        public Renderer[] renderers;
        public Material[] materials;

        /// ======================================================================

        private void OnEnable()
        {
            var random = UnityEngine.Random.Range(0, materials.Length);
            foreach (var rnd in renderers)
            {
                rnd.sharedMaterial = materials[random];
            }
        }

        /// ======================================================================
    }
}