﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d
{
	public class AMaterialRandomizer : MonoBehaviour
	{
        /// ======================================================================

        public Renderer[] renderers;
        public Material[] materials;

        /// ======================================================================

        private void OnEnable()
        {
            foreach (var rnd in renderers)
            {
                var random = Random.Range(0, materials.Length);
                rnd.sharedMaterial = materials[random];
            }
        }

        /// ======================================================================
    }
}