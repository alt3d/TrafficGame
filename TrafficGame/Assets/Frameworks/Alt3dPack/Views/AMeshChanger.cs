﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d
{
	public class AMeshChanger : MonoBehaviour
	{
        /// ======================================================================

        public MeshFilter filter;
        public Mesh[] meshes;

        /// ======================================================================

        private void OnEnable()
        {
            var random = UnityEngine.Random.Range(0, meshes.Length);
            filter.sharedMesh = meshes[random];
        }

        /// ======================================================================
    }
}