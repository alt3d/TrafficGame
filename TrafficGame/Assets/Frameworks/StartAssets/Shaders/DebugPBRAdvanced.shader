﻿Shader "Debug/PBR Advanced" 
{
	Properties 
	{
		_Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex("Albedo", 2D) = "white" {}

		_Metallic("Metallic", Range(0.0, 2.0)) = 0.0
		_MetallicMap("Metallic (R)", 2D) = "white" {}

		_Glossiness("Smoothness", Range(0.0, 2.0)) = 0.5
		_GlossMap("Smoothness (B)", 2D) = "white" {}

		_BumpScale("Normal Scale", Range(0.0, 2.0)) = 1.0
		[Normal]_BumpMap("Normal Map", 2D) = "bump" {}

		_OcclusionScale("Occlusion Scale", Range(0.0, 2.0)) = 1.0
		_OcclusionMap("Occlusion (G)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		// ======================================================================

		float4 _Color;
		sampler2D _MainTex;

		float _Metallic;
		sampler2D _MetallicMap;

		float _Glossiness;
		sampler2D _GlossMap;

		float _BumpScale;
		sampler2D _BumpMap;

		float _OcclusionScale;
		sampler2D _OcclusionMap;

		// ======================================================================
		
		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_MetallicMap;
			float2 uv_GlossMap;
			float2 uv_BumpMap;
			float2 uv_OcclusionMap;
		};

		// ======================================================================

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			float4 diffTex = tex2D(_MainTex, IN.uv_MainTex);
			float4 metallTex = tex2D(_MetallicMap, IN.uv_MetallicMap);
			float4 smoothTex = tex2D(_GlossMap, IN.uv_GlossMap);
			float4 nrmTex = tex2D(_BumpMap, IN.uv_BumpMap);
			float4 aoTex = tex2D(_OcclusionMap, IN.uv_OcclusionMap);

			o.Albedo = (diffTex * _Color).rgb;
			o.Metallic = metallTex.r * _Metallic;
			o.Smoothness = smoothTex.b * _Glossiness;
			o.Normal = UnpackScaleNormal(nrmTex, _BumpScale);
			o.Occlusion = lerp(2, aoTex.g, _OcclusionScale);
		}

		// ======================================================================

		ENDCG
	} 

	FallBack "Diffuse"
}
