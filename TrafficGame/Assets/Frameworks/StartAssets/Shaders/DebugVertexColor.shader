﻿Shader "Debug/Vertex Color" 
{
	Properties
	{
		_Color("Color", Color) = (1, 1, 1, 1)
	}

	SubShader 
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 400
		
		CGPROGRAM
		#pragma surface surf Standard vertex:vert fullforwardshadows
		#pragma target 3.0

		// ======================================================================

		float4 _Color;

		// ======================================================================

		struct Input 
		{
			float4 vertexColor;
		};

		// ======================================================================

		void vert(inout appdata_full v, out Input o)
		{
			o.vertexColor = v.color;
		}

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = IN.vertexColor * _Color;
		}

		// ======================================================================

		ENDCG
	} 

	FallBack "Diffuse"
}
