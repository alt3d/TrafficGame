﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Achivs.Achivs;

namespace Alt3d.TrafficGame.Achivs
{
	public abstract class AAchiv : IAchiv
	{
        /// ======================================================================

        public IAchivDesc desc { get; set; }
        public bool isUnlocked { get; set; }

        /// ======================================================================

        public event Action<IAchiv> Unlocked = delegate { };

        /// ======================================================================

        public abstract void Awake();
        public abstract void Enable();
        public abstract void Disable();

        protected void Unlock()
        {
            if (isUnlocked)
                return;

            isUnlocked = true;
            Unlocked(this);
            Disable();
        }

        /// ======================================================================

        public static AAchiv Create(EAchivID id)
        {
            switch (id)
            {
                case EAchivID.AmbulanceCrasher: return new AAmbulanceCrasher();
                case EAchivID.PoliceCrasher: return new APoliceCrasher();
                case EAchivID.FireTruckCrasher: return new AFireTruckCrasher();
                // ------
                case EAchivID.HydrantsDestroyer: return new AHydrantsDestroyer();
                case EAchivID.BilboardADestroyer: return new ABilboardADestroyer();
                case EAchivID.BilboardBDestroyer: return new ABilboardBDestroyer();
                case EAchivID.CafeDestroyer: return new ACafeDestroyer();
                case EAchivID.HotelSignDestroyer: return new AHotelSignDestroyer();
                case EAchivID.IcecreamSignDestroyer: return new AIcecreamSignDestroyer();
                // ------
                case EAchivID.RanchSignDestroyer: return new ARanchSignDestroyer();
                case EAchivID.WindmillDestroyer: return new AWindmillDestroyer();
                case EAchivID.OuthouseDestroyer: return new AOuthouseDestroyer();
                case EAchivID.WaterTowerDestroyer: return new AWaterTowerDestroyer();
                case EAchivID.ScareCrowDestroyer: return new AScareCrowDestroyer();
                // ------
                case EAchivID.CarPassA: return new APassCarsA();
                case EAchivID.CarPassB: return new APassCarsB();
                case EAchivID.CarPassC: return new APassCarsC();
                // ------
                case EAchivID.OneSignalCarsPassA: return new AOneSignalCarsPassA();
                case EAchivID.PassAmbulance: return new APassAmbulance();
                case EAchivID.PassPolice: return new APassPolice();
                case EAchivID.PassFireTruck: return new APassFireTruck();
                // ------
                default: throw new Exception(id.ToString());
            }
        }
        protected AAchiv()
        {

        }

        /// ======================================================================
    }
}