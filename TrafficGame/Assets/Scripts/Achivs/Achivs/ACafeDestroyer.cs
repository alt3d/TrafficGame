﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class ACafeDestroyer : AAchiv
    {
        /// ======================================================================

        private IPropsEvents events;
        private ILevel level;

        private int currentCount;
        private const int targetCount = 3;

        /// ======================================================================

        public override void Awake()
        {
            events = AServices.Get<IPropsEvents>();
            level = AServices.Get<ILevel>();
        }
        public override void Enable()
        {
            events.PropsBroken += OnPropsBroken;
            level.StateChanged += OnLevelStateChanged;
        }
        public override void Disable()
        {
            events.PropsBroken -= OnPropsBroken;
            level.StateChanged -= OnLevelStateChanged;
        }

        private void OnPropsBroken(EPropsID id)
        {
            if (isUnlocked)
                return;

            if (currentCount >= targetCount)
                return;

            if (id == EPropsID.CafeTable)
                currentCount++;

            if (currentCount == targetCount)
                Unlock();
        }
        private void OnLevelStateChanged(ELevelState state)
        {
            if (state == ELevelState.Play)
                currentCount = 0;
        }

        /// ======================================================================
    }
}