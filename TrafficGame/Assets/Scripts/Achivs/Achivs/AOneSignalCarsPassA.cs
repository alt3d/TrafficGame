﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class AOneSignalCarsPassA : AAchiv
	{
        /// ======================================================================

        public IStats stats;
        private const int targetCount = 8;

        /// ======================================================================

        public override void Awake()
        {
            stats = AServices.Get<IStats>();
        }
        public override void Enable()
        {
            stats.OneSignalCarPassedChanged += OnOneSignalCarPassedChanged;
        }
        public override void Disable()
        {
            stats.OneSignalCarPassedChanged += OnOneSignalCarPassedChanged;
        }

        private void OnOneSignalCarPassedChanged(int count)
        {
            if (isUnlocked == false)
                if (count >= targetCount)
                    Unlock();
        }

        /// ======================================================================
    }
}