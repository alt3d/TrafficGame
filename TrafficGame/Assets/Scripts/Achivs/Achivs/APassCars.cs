﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public abstract class APassCars : AAchiv
	{
        /// ======================================================================

        protected IStats stats;
        protected abstract int targetCount { get; }

        /// ======================================================================

        public override void Awake()
        {
            stats = AServices.Get<IStats>();
        }
        public override void Enable()
        {
            stats.PassedCarsCountChanged += OnPassedCarsCountChanged;
        }
        public override void Disable()
        {
            stats.PassedCarsCountChanged -= OnPassedCarsCountChanged;
        }

        private void OnPassedCarsCountChanged(int count)
        {
            if (isUnlocked == false)
                if (count >= targetCount)
                    Unlock();
        }

        /// ======================================================================
    }
}