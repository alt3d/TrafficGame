﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class APassCarsA : APassCars
    {
        /// ======================================================================

        private const int _targetCount = 100;

        /// ======================================================================

        protected override int targetCount
        {
            get
            {
                return _targetCount;
            }
        }
        public override void Enable()
        {
            base.Enable();
        }
        public override void Disable()
        {
            base.Disable();
        }

        /// ======================================================================
    }
}