﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class APassCarsC : APassCars
    {
        /// ======================================================================

        private const int _targetCount = 500;

        /// ======================================================================

        protected override int targetCount
        {
            get
            {
                return _targetCount;
            }
        }

        /// ======================================================================
    }
}