﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class APassPolice : AAchiv
	{
        /// ======================================================================

        private ICarsEvents carsEvents;

        /// ======================================================================

        public override void Awake()
        {
            carsEvents = AServices.Get<ICarsEvents>();
        }
        public override void Enable()
        {
            carsEvents.CarPassed += OnCarPassed;
        }
        public override void Disable()
        {
            carsEvents.CarPassed -= OnCarPassed;
        }

        private void OnCarPassed(ICar car)
        {
            if (isUnlocked == false)
                if (car.id == ECarID.PoliceA)
                    if (car.IsWasStop() == false)
                        Unlock();
        }

        /// ======================================================================
    }
}