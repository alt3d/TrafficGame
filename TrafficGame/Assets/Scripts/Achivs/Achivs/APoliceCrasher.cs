﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class APoliceCrasher : AAchiv
	{
        /// ======================================================================

        private ICarsEvents events;
        private IStats stats;

        /// ======================================================================

        public override void Awake()
        {
            events = AServices.Get<ICarsEvents>();
            stats = AServices.Get<IStats>();
        }
        public override void Enable()
        {
            events.CarCrashed += OnCarCrashed;
        }
        public override void Disable()
        {
            events.CarCrashed -= OnCarCrashed;
        }

        private void OnCarCrashed(ICar car)
        {
            if (isUnlocked == false)
                if (stats.crashedCarsCount <= 2)
                    if (car.id == ECarID.PoliceA)
                        Unlock();
        }

        /// ======================================================================
    }
}