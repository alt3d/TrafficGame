﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs.Achivs
{
	public class AWaterTowerDestroyer : AAchiv
    {
        /// ======================================================================

        private IPropsEvents events;

        /// ======================================================================

        public override void Awake()
        {
            events = AServices.Get<IPropsEvents>();
        }
        public override void Enable()
        {
            events.PropsBroken += OnPropsBroken;
        }
        public override void Disable()
        {
            events.PropsBroken -= OnPropsBroken;
        }

        private void OnPropsBroken(EPropsID id)
        {
            if (isUnlocked == false)
                if (id == EPropsID.WaterTower)
                    Unlock();
        }

        /// ======================================================================
    }
}