﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs
{
	public class AAchivDesc : IAchivDesc
    {
		/// ======================================================================

        public EAchivID id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int unlockReward { get; set; }

        /// ======================================================================
    }
}