﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs
{
	public class AAchivDescAsset : ScriptableObject
	{
        /// ======================================================================

        [SerializeField] protected string _title;
        [SerializeField] protected string _description;
        [SerializeField] protected int _unlockReward = 1000;

        /// ======================================================================

        public string title
        {
            get { return _title; }
        }
        public string description
        {
            get { return _description; }
        }
        public int unlockReward
        {
            get { return _unlockReward; }
        }

        /// ======================================================================
    }
}