﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IAchiv 
	{
		/// ======================================================================

        IAchivDesc desc { get; }
        bool isUnlocked { get; }

        /// ======================================================================

        event Action<IAchiv> Unlocked;

        /// ======================================================================
    }
}