﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IAchivDesc 
	{
		/// ======================================================================

        EAchivID id { get; }
        string title { get; }
        string description { get; }
        int unlockReward { get; }

        /// ======================================================================
    }
}