﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IAchivDescsRepository 
	{
        /// ======================================================================

        //event Action<IAchivDesc> Added;
        //event Action<IAchivDesc> Removed;

        /// ======================================================================

        IAchivDesc Get(EAchivID id);

        /// ======================================================================
    }
}