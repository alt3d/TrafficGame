﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IAchivsRepository 
	{
        /// ======================================================================

        event Action<IAchiv> Added;
        event Action<IAchiv> Removed;

        /// ======================================================================

        IAchiv Get(EAchivID id);
        //List<IAchiv> GetAll();

		/// ======================================================================
	}
}