﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs
{
	public class AAchivDescsRepository : MonoBehaviour, IAchivDescsRepository
	{
        /// ======================================================================

        private readonly ADictionary<EAchivID, AAchivDesc> dic = new ADictionary<EAchivID, AAchivDesc>();
        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        public event Action<IAchivDesc> Added = delegate { };
        public event Action<IAchivDesc> Removed = delegate { };

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            dic.Added += OnDescAdded;
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            dic.Removed += OnDescRemoved;
            appActions.Remove(appLoadAction);
        }

        public IAchivDesc Get(EAchivID id)
        {
            return dic.Get(id);
        }

        private IEnumerator DoLoadApp()
        {
            LoadDescs();
            yield return null;
        }
        private void LoadDescs()
        {
            var path = ZAchivs.descsPath;
            var assets = UResources.LoadAll<AAchivDescAsset>(path);
            foreach (var asset in assets)
            {
                var id = UEnums.Parse<EAchivID>(asset.name);
                var desc = new AAchivDesc();
                desc.id = id;
                desc.title = asset.title;
                desc.description = asset.description;
                desc.unlockReward = asset.unlockReward;
                dic.Add(desc.id, desc);
            }
        }

        /// ======================================================================

        private void OnDescAdded(EAchivID id, AAchivDesc desc)
        {
            Added(desc);
        }
        private void OnDescRemoved(EAchivID id, AAchivDesc desc)
        {
            Removed(desc);
        }

        /// ======================================================================
    }
}