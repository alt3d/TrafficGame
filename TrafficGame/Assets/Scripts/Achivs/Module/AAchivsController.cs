﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs
{
	public class AAchivsController : MonoBehaviour
	{
        /// ======================================================================

        private AAchivsRepository achivs;
        private AAchivDescsRepository descs;

        private IStream stream;

        private IGameActionsRepository gameActions;
        private DGameLoadAction gameLoadAction;
        private DGameExitAction gameExitAction;

        private const string prefix = "achivs_";

        /// ======================================================================

        private void Awake()
        {
            achivs = AServices.Get<AAchivsRepository>();
            descs = AServices.Get<AAchivDescsRepository>();
            stream = AServices.Get<IStream>(); 

            gameActions = AServices.Get<IGameActionsRepository>();
            gameLoadAction = new DGameLoadAction(DoLoadGame);
            gameExitAction = new DGameExitAction(DoExitGame);
        }
        private void OnEnable()
        {
            descs.Added += OnDescAdded;
            achivs.Added += OnAchivAdded;
            achivs.Removed += OnAchivRemoved;

            gameActions.Add(gameLoadAction);
            gameActions.Add(gameExitAction);
        }
        private void OnDisable()
        {
            descs.Added -= OnDescAdded;
            achivs.Added -= OnAchivAdded;
            achivs.Removed -= OnAchivRemoved;

            gameActions.Remove(gameLoadAction);
            gameActions.Remove(gameExitAction);
        }

        private void OnDescAdded(IAchivDesc desc)
        {
            if (ZAchivs.IsExists(desc.id))
                CreateAchiv(desc.id);
        }
        private void CreateAchiv(EAchivID id)
        {
            var achiv = AAchiv.Create(id);
            var desc = descs.Get(id);
            achiv.desc = desc;
            achiv.Awake();
            achivs.Add(achiv);
        }

        private IEnumerator DoLoadGame()
        {
            SetupAchivs();
            EnableAchivs();
            yield return null;
        }
        private void DoExitGame()
        {
            DisableAchivs();
        }

        private void SetupAchivs()
        {
            foreach (var achiv in achivs.GetAll())
            {
                var key = prefix + achiv.desc.id;
                (achiv as AAchiv).isUnlocked = stream.HasKey(key);
            }
        }
        private void EnableAchivs()
        {
            foreach (var achiv in achivs.GetAll())
            {
                if (achiv.isUnlocked == false)
                {
                    (achiv as AAchiv).Enable();
                }
            }
        }
        private void DisableAchivs()
        {
            foreach (var achiv in achivs.GetAll())
            {
                if (achiv.isUnlocked == false)
                {
                    (achiv as AAchiv).Disable();
                }
            }
        }

        private void OnAchivAdded(IAchiv achiv)
        {
            achiv.Unlocked += OnAchivUnlocked;
        }
        private void OnAchivRemoved(IAchiv achiv)
        {
            achiv.Unlocked -= OnAchivUnlocked;
        }
        private void OnAchivUnlocked(IAchiv achiv)
        {
            var key = prefix + achiv.desc.id;
            stream.SaveBool(key, true);
            Debug.Log("SAVE: " + key);
        }

        /// ======================================================================
    }
}