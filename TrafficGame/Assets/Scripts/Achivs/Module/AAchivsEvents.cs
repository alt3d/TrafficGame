﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public class AAchivsEvents : MonoBehaviour, IAchivsEvents
	{
        /// ======================================================================

        private IAchivsRepository achivs;

        /// ======================================================================

        public event Action<IAchiv> AchivUnlocked = delegate { };

        /// ======================================================================

        private void Awake()
        {
            achivs = AServices.Get<IAchivsRepository>();    
        }
        private void OnEnable()
        {
            achivs.Added += OnAchivAdded;
            achivs.Removed += OnAchivRemoved;
        }
        private void OnDisable()
        {
            achivs.Added -= OnAchivAdded;
            achivs.Removed -= OnAchivRemoved;
        }

        private void OnAchivAdded(IAchiv achiv)
        {
            achiv.Unlocked += OnAchivUnlocked;
        }
        private void OnAchivRemoved(IAchiv achiv)
        {
            achiv.Unlocked -= OnAchivUnlocked;
        }
        private void OnAchivUnlocked(IAchiv achiv)
        {
            AchivUnlocked(achiv);
        } 

		/// ======================================================================
	}
}