﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Achivs
{
	public class AAchivsRepository : MonoBehaviour, IAchivsRepository
	{
        /// ======================================================================

        private readonly AList<AAchiv> gameAchivs = new AList<AAchiv>();

        /// ======================================================================

        public event Action<IAchiv> Added = delegate { };
        public event Action<IAchiv> Removed = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            gameAchivs.Added += OnAchivAdded;
            gameAchivs.Removed += OnAchivRemoved;
        }
        private void OnDisable()
        {
            gameAchivs.Added -= OnAchivAdded;
            gameAchivs.Removed -= OnAchivRemoved;
        }

        public void Add(AAchiv achiv)
        {
            gameAchivs.Add(achiv);
        }
        public void Remove(AAchiv achiv)
        {
            gameAchivs.Remove(achiv);
        }

        public IAchiv Get(EAchivID id)
        {
            foreach (var achiv in gameAchivs.GetAll())
            {
                if (achiv.desc.id == id) return achiv;
            }
            throw new Exception(id.ToString());
        }
        public List<IAchiv> GetAll()
        {
            var result = new List<IAchiv>();
            foreach (var achiv in gameAchivs.GetAll())
            {
                result.Add(achiv);
            }
            return result;
        }

        private void OnAchivAdded(AAchiv achiv)
        {
            Added(achiv);
        }
        private void OnAchivRemoved(AAchiv achiv)
        {
            Removed(achiv);
        }
        
        /// ======================================================================
    }
}