﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Achivs;

namespace Alt3d.TrafficGame
{
    public static class ZAchivs
    {
        /// ======================================================================

        public const string descsPath = "Achivs";

        public static List<EAchivID> GetAppAchivs()
        {
            return new List<EAchivID>()
            {
                EAchivID.AmbulanceCrasher,
                EAchivID.PoliceCrasher,
                EAchivID.FireTruckCrasher,
                // --------------------
                EAchivID.HydrantsDestroyer,
                //EAchivID.BilboardADestroyer,
                //EAchivID.BilboardBDestroyer,
                EAchivID.CafeDestroyer,
                EAchivID.HotelSignDestroyer,
                EAchivID.IcecreamSignDestroyer,
                //// --------------------
                //EAchivID.RanchSignDestroyer,
                //EAchivID.WindmillDestroyer,
                EAchivID.OuthouseDestroyer,
                //EAchivID.WaterTowerDestroyer,
                EAchivID.ScareCrowDestroyer,
                //// --------------------
                EAchivID.CarPassA,
                //EAchivID.CarPassB,
                //EAchivID.CarPassC,
                //// --------------------
                EAchivID.OneSignalCarsPassA,
                EAchivID.PassAmbulance,
                EAchivID.PassPolice,
                EAchivID.PassFireTruck
            };
        }
        public static bool IsExists(EAchivID id)
        {
            foreach (var item in GetAppAchivs())
            {
                if (item == id) return true;
            }
            return false;
        }

        public static void Init(IContainer container)
        {
            var achivsRepo = container.AddComponent<AAchivsRepository>();
            container.Register<AAchivsRepository>(achivsRepo);
            container.Register<IAchivsRepository>(achivsRepo);

            var descsRepo = container.AddComponent<AAchivDescsRepository>();
            container.Register<AAchivDescsRepository>(descsRepo);
            container.Register<IAchivDescsRepository>(descsRepo);

            var events = container.AddComponent<AAchivsEvents>();
            container.Register<IAchivsEvents>(events);

            container.AddComponent<AAchivsController>();
        }

        /// ======================================================================
    }
}

namespace Alt3d.TrafficGame
{
    public enum EAchivID
    {
        AmbulanceCrasher,
        PoliceCrasher,
        FireTruckCrasher,
        // ---
        HydrantsDestroyer,
        BilboardADestroyer,
        BilboardBDestroyer,
        CafeDestroyer,
        HotelSignDestroyer,
        IcecreamSignDestroyer,
        // ---
        RanchSignDestroyer,
        WindmillDestroyer,
        OuthouseDestroyer,
        WaterTowerDestroyer,
        ScareCrowDestroyer,
        // ---
        CarPassA, // TODO Переименовать
        CarPassB,
        CarPassC,
        // ---
        OneSignalCarsPassA,
        PassAmbulance,
        PassPolice,
        PassFireTruck
    }
}