﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Actions
{
	public class AAppActionsRepository : MonoBehaviour, IAppActionsRepository
    {
        /// ======================================================================

        private AList<DAppLoadAction> load = new AList<DAppLoadAction>();
        private AList<DAppExitAction> exit = new AList<DAppExitAction>();

        /// ======================================================================

        public void Add(DAppLoadAction action)
        {
            load.Add(action);
        }
        public void Remove(DAppLoadAction action)
        {
            load.Remove(action);
        }
        public List<DAppLoadAction> GetLoad()
        {
            return new List<DAppLoadAction>(load.GetAll());
        }

        public void Add(DAppExitAction action)
        {
            exit.Add(action);
        }
        public void Remove(DAppExitAction action)
        {
            exit.Remove(action);
        }
        public List<DAppExitAction> GetExit()
        {
            return new List<DAppExitAction>(exit.GetAll());
        }

        /// ======================================================================
    }
}