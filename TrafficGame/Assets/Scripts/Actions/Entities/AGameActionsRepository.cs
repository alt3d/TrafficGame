﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Actions
{
	public class AGameActionsRepository : MonoBehaviour, IGameActionsRepository
    {
        /// ======================================================================

        private AList<DGameLoadAction> load = new AList<DGameLoadAction>();
        private AList<DGameExitAction> exit = new AList<DGameExitAction>();

        /// ======================================================================

        public void Add(DGameLoadAction action)
        {
            load.Add(action);
        }
        public void Remove(DGameLoadAction action)
        {
            load.Remove(action);
        }
        public List<DGameLoadAction> GetLoad()
        {
            return new List<DGameLoadAction>(load.GetAll());
        }

        public void Add(DGameExitAction action)
        {
            exit.Add(action);
        }
        public void Remove(DGameExitAction action)
        {
            exit.Remove(action);
        }
        public List<DGameExitAction> GetExit()
        {
            return new List<DGameExitAction>(exit.GetAll());
        }

        /// ======================================================================
    }
}