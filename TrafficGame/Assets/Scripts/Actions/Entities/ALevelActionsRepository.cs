﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Actions
{
	public class ALevelActionsRepository : MonoBehaviour, ILevelActionsRepository
    {
        /// ======================================================================

        private AList<DLevelLoadAction> load = new AList<DLevelLoadAction>();
        private AList<DLevelExitAction> exit = new AList<DLevelExitAction>();

        /// ======================================================================

        public void Add(DLevelLoadAction action)
        {
            load.Add(action);
        }
        public void Remove(DLevelLoadAction action)
        {
            load.Remove(action);
        }
        public List<DLevelLoadAction> GetLoad()
        {
            return new List<DLevelLoadAction>(load.GetAll());
        }

        public void Add(DLevelExitAction action)
        {
            exit.Add(action);
        }
        public void Remove(DLevelExitAction action)
        {
            exit.Remove(action);
        }
        public List<DLevelExitAction> GetExit()
        {
            return new List<DLevelExitAction>(exit.GetAll());
        }

        /// ======================================================================
    }
}