﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IAppActionsRepository 
	{
        /// ======================================================================

        void Add(DAppLoadAction action);
        void Remove(DAppLoadAction action);
        List<DAppLoadAction> GetLoad();

        void Add(DAppExitAction action);
        void Remove(DAppExitAction action);
        List<DAppExitAction> GetExit();
   
        /// ======================================================================
    }
}