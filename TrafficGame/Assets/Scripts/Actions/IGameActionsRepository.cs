﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IGameActionsRepository
	{
        /// ======================================================================

        void Add(DGameLoadAction action);
        void Remove(DGameLoadAction action);
        List<DGameLoadAction> GetLoad();

        void Add(DGameExitAction action);
        void Remove(DGameExitAction action);
        List<DGameExitAction> GetExit();
   
        /// ======================================================================
    }
}