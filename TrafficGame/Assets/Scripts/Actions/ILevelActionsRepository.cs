﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ILevelActionsRepository 
	{
        /// ======================================================================

        void Add(DLevelLoadAction action);
        void Remove(DLevelLoadAction action);
        List<DLevelLoadAction> GetLoad();

        void Add(DLevelExitAction action);
        void Remove(DLevelExitAction action);
        List<DLevelExitAction> GetExit();

        /// ======================================================================
    }
}