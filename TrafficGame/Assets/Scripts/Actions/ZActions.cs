﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Actions;

namespace Alt3d.TrafficGame
{
	public static class ZActions 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var appActions = container.AddComponent<AAppActionsRepository>();
            container.Register<IAppActionsRepository>(appActions);

            var gameActions = container.AddComponent<AGameActionsRepository>();
            container.Register<IGameActionsRepository>(gameActions);

            var levelActions = container.AddComponent<ALevelActionsRepository>();
            container.Register<ILevelActionsRepository>(levelActions);
        }

		/// ======================================================================
	}
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public delegate IEnumerator DAppLoadAction();
    public delegate void DAppExitAction();

    public delegate IEnumerator DGameLoadAction();
    public delegate void DGameExitAction();

    public delegate IEnumerator DLevelLoadAction();
    public delegate IEnumerator DLevelExitAction();

    /// ======================================================================
}