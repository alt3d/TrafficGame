﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Alt3d.TrafficGame.Adverts
{
	public class AInterstitialAdvert : MonoBehaviour, IInterstitialAdvert
    {
        /// ======================================================================

        private ILevel level;
        private IGameSettings settings;

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        private int playedLevelsCount;
        private bool isShow;

        private const string placementId = "video";

        /// ======================================================================

        public bool isNeedAds { get; set; }

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
            settings = AServices.Get<IGameSettings>();

            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            level.StateChanged += OnLevelStateChanged;
            appActions.Remove(appLoadAction);
        }

        public bool IsNeedAds()
        {
            if (Advertisement.isSupported == false)
                return false;

            if (settings.isEnableInterstitialAdvert == false)
                return false;

            return playedLevelsCount > settings.levelsBetweenInterstitialAdvert;
        }
        public IEnumerator DoShowAds()
        {
            if (Advertisement.isSupported == false)
                yield break;

            if (Advertisement.isInitialized == false)
                Advertisement.Initialize(AProduct.adsID);

            ShowOptions options = new ShowOptions();
            options.resultCallback = OnAdsFinished;
            Advertisement.Show(placementId, options);

            isShow = true;
            while (isShow)
            {
                yield return null;
            }
        }

        private void OnAdsFinished(ShowResult result)
        {
            isNeedAds = false;
            playedLevelsCount = 0;
            isShow = false;
        }
        private void OnLevelStateChanged(ELevelState state)
        {
            if (state == ELevelState.Stop)
                playedLevelsCount++;
        }

        private IEnumerator DoLoadApp()
        {
            InitAds();
            yield return null;
        }
        private void InitAds()
        {
            if (Advertisement.isInitialized == false)
                if (Advertisement.isSupported)
                    Advertisement.Initialize(AProduct.adsID);
        }

        /// ======================================================================
    }
}