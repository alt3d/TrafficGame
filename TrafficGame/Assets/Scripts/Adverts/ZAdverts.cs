﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using Alt3d.TrafficGame.Adverts;

namespace Alt3d.TrafficGame
{
	public static class ZAdverts 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var advert = container.AddComponent<AInterstitialAdvert>();
            container.Register<IInterstitialAdvert>(advert);
        }

		/// ======================================================================
	}
}