﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.App
{
	public class AApp : MonoBehaviour, IApp
	{
        /// ======================================================================

        private EAppState _state = EAppState.Stop;
        private bool _focus = false;

        /// ======================================================================

        public EAppState state
        {
            get { return _state; }
            set
            {
                _state = value;
                StateChanged(_state);
            }
        }
        public bool focus
        {
            get { return _focus; }
            set
            {
                _focus = value;
                FocusChanged(_focus);
            }
        }

        /// ======================================================================

        public event Action<EAppState> StateChanged = delegate { };
        public event Action<bool> FocusChanged = delegate { };

        /// ======================================================================
    }
}