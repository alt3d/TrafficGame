﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IApp 
	{
        /// ======================================================================

        EAppState state { get; }
        bool focus { get; }

        /// ======================================================================

        event Action<EAppState> StateChanged;
        event Action<bool> FocusChanged;

        /// ======================================================================
    }
}