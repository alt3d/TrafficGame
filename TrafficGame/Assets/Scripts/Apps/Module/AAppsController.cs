﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.App
{
	public class AAppsController : MonoBehaviour
	{
        /// ======================================================================

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
        }

        private IEnumerator DoLoadApp()
        {
            SetupTargetFps();
            yield return null;
        }
        private void SetupTargetFps()
        {
            if (Application.isEditor)
                Application.targetFrameRate = 999;

            if (Application.isMobilePlatform)
                Application.targetFrameRate = 60;
        }

        /// ======================================================================
    }
}