﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.App;

namespace Alt3d.TrafficGame
{
    public static class ZApps
    {
        /// ======================================================================

        public static void Init(IContainer container)
        {
            var app = container.AddComponent<App.AApp>();
            container.Register<AApp>(app);
            container.Register<IApp>(app);

            container.AddComponent<AAppsController>();
        }

        /// ======================================================================
    }
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum EAppState
    {
        Stop,
        Launch,
        Game,
        Quit
    }

    /// ======================================================================
}