﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Cams.Cams;

namespace Alt3d.TrafficGame.Cams
{
	public class ACam : MonoBehaviour
	{
        /// ======================================================================

        public Transform playPoint;
        public Transform crashPoint;
        public float moveSpeed;

        [Space]
        public Camera cam;
        public float playFov;
        public float crashFov;
        public float fovSpeed;

        private ITown town;

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();
        }
        private void OnEnable()
        {
            town.StateChanged += OnLevelStateChanged;
            SetPlay();
        }
        private void OnDisable()
        {
            town.StateChanged -= OnLevelStateChanged;
            StopAllCoroutines();
        }

        /// =====================================================================

        private void OnLevelStateChanged(ETownState state)
        {
            switch (state)
            {
                case ETownState.Play:
                    SetPlay();
                    break;
                case ETownState.Crash:
                    SetCrash();
                    break;
            }
        }

        /// ======================================================================

        private void SetPlay()
        {
            StopAllCoroutines();

            transform.position = playPoint.position;
            transform.rotation = playPoint.rotation;

            cam.fieldOfView = playFov;
        }
        private void SetCrash()
        {
            StopAllCoroutines();
            StartCoroutine(DoSetCrash());
        }
        private IEnumerator DoSetCrash()
        {
            while (true)
            {
                var lerp = Time.deltaTime * moveSpeed;
                transform.position = Vector3.Lerp(transform.position, crashPoint.position, lerp);
                transform.rotation = Quaternion.Slerp(transform.rotation, crashPoint.rotation, lerp);

                lerp = Time.deltaTime * fovSpeed;
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, crashFov, lerp);

                yield return new WaitForEndOfFrame();
            }
        }

        /// ======================================================================
    }
}