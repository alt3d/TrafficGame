﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cams.Cams
{
	public class AMainCamPoint : MonoBehaviour
	{
        /// ======================================================================

        public AMainCamTarget target;
        public float moveSpeed;
        public float distanceFactor;

        private Vector3 startPos;

        /// ======================================================================

        private void Awake()
        {
            startPos = transform.position;
        }
        private void Update()
        {
            var dir = startPos - target.transform.position;
            var pos = target.transform.position + dir * distanceFactor;
            var lerp = Time.deltaTime * moveSpeed;

            transform.position = Vector3.Lerp(transform.position, pos, lerp);
            transform.LookAt(target.transform.position);
        }
        private void OnDrawGizmos()
        {
            UDraw.Line(transform.position, target.transform.position, Color.green);
            UDraw.Sphere(transform.position, 1f, Color.green);
        }

        /// ======================================================================
    }
}