﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cams.Cams
{
	public class AMainCamTarget : MonoBehaviour
	{
        /// ======================================================================

        public ACarDetectionArea area;
        public float moveSpeed;

        private ILevel level;
        private readonly List<ICar> cars = new List<ICar>();
        private Vector3 sum = Vector3.zero;

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
            area.CarEntered += OnCarEnteredToArea;
            area.CarExited += OnCarExitedFromArea;
        }
        private void OnDisable()
        {
            level.StateChanged -= OnLevelStateChanged;
            area.CarEntered -= OnCarEnteredToArea;
            area.CarExited -= OnCarExitedFromArea;
        }

        private void OnDrawGizmos()
        {
            foreach (var car in cars)
            {
                UDraw.Line(car.position, transform.position, Color.yellow);
                UDraw.Sphere(car.position, 1f, Color.yellow);
            }

            UDraw.Sphere(transform.position, 1f, Color.red);
        }
        private void Update()
        {
            if (cars.Count == 0)
                return;

            sum = Vector3.zero;
            foreach (var car in cars)
            {
                sum.x += car.position.x;
                sum.y += car.position.y;
                sum.z += car.position.z;
            }

            var pos = sum / cars.Count;
            var lerp = moveSpeed * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, pos, lerp);
        }

        private void OnLevelStateChanged(ELevelState state)
        {
            switch (state)
            {
                case ELevelState.Stop:
                    transform.position = Vector3.zero;
                    cars.Clear();
                    break;
            }
        }
        private void OnCarEnteredToArea(ICar car)
        {
            if (cars.Contains(car))
                return;

            cars.Add(car);
            car.StateChanged += OnCarStateChanged;
        }
        private void OnCarExitedFromArea(ICar car)
        {
            if (cars.Contains(car) == false)
                return;

            cars.Remove(car);
            car.StateChanged -= OnCarStateChanged;
        }
        private void OnCarStateChanged(ICar car, ECarState state)
        {
            if (state != ECarState.Idle) return;
            if (cars.Contains(car) == false) return;

            cars.Remove(car);
            car.StateChanged -= OnCarStateChanged;
        }

        /// ======================================================================
    }
}