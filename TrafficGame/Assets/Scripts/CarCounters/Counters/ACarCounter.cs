﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarCounters.Counters;

namespace Alt3d.TrafficGame.CarCounters
{
    public class ACarCounter : MonoBehaviour, ICarCounter
    {
        /// ======================================================================

        public ACarCounterData data;
        public ACarDetectionArea area;

        /// ======================================================================

        public event Action<ICarCounter, ICar> CarEntered = delegate { };
        public event Action<ICarCounter, ICar> CarExited = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            area.CarEntered += OnCarEnteredToArea;
            area.CarExited += OnCarExitedFromArea;
        }
        private void OnDisable()
        {
            area.CarEntered -= OnCarEnteredToArea;
            area.CarExited -= OnCarExitedFromArea;
        }

        private void OnCarEnteredToArea(ICar car)
        {
            CarEntered(this, car);
        }
        private void OnCarExitedFromArea(ICar car)
        {
            CarExited(this, car);
        }

        /// ======================================================================
    }
}