﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.CarCounters.Counters
{
	public class ACarCounterData : MonoBehaviour
	{
		/// ======================================================================

        public ETownID town { get; set; }
        public EDirection direction { get; set; }

		/// ======================================================================
	}
}