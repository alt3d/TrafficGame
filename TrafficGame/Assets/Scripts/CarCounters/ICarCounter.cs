﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarCounter 
	{
        /// ======================================================================

        event Action<ICarCounter, ICar> CarEntered;
        event Action<ICarCounter, ICar> CarExited;

        /// ======================================================================
    }
}