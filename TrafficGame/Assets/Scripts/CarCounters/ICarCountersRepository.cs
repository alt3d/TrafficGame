﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarCountersRepository 
	{
        /// ======================================================================

        event Action<ICarCounter> GameCounterAdded;
        event Action<ICarCounter> GameCounterRemoved;

        event Action<ICarCounter> LevelCounterAdded;
        event Action<ICarCounter> LevelCounterRemoved;

        /// ======================================================================

        List<ICarCounter> GetAllLevelCounters();

        /// ======================================================================
    }
}