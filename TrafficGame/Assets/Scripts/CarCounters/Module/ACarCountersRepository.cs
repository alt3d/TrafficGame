﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.CarCounters
{
	public class ACarCountersRepository : MonoBehaviour, ICarCountersRepository
	{
        /// ======================================================================

        private AList<ACarCounter> gameCounters = new AList<ACarCounter>();
        private AList<ACarCounter> levelCounters = new AList<ACarCounter>();

        private ITown town;

        private IAppActionsRepository appActions;
        private ILevelActionsRepository levelActions;
        private DAppLoadAction appLoadAction;
        private DLevelLoadAction levelLoadAction;
        private DLevelExitAction levelExitAction;

        /// ======================================================================

        public event Action<ICarCounter> GameCounterAdded = delegate { };
        public event Action<ICarCounter> GameCounterRemoved = delegate { };

        public event Action<ICarCounter> LevelCounterAdded = delegate { };
        public event Action<ICarCounter> LevelCounterRemoved = delegate { };

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();

            appActions = AServices.Get<IAppActionsRepository>();
            levelActions = AServices.Get<ILevelActionsRepository>();

            appLoadAction = new DAppLoadAction(DoLoadApp);
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
            levelExitAction = new DLevelExitAction(DoExitLevel);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
            levelActions.Add(levelLoadAction);
            levelActions.Add(levelExitAction);

            gameCounters.Added += OnGameCounterAdded;
            gameCounters.Removed += OnGameCounterRemoved;

            levelCounters.Added += OnLevelCounterAdded;
            levelCounters.Removed += OnLevelCounterRemoved;
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
            levelActions.Remove(levelLoadAction);
            levelActions.Remove(levelExitAction);

            gameCounters.Added -= OnGameCounterAdded;
            gameCounters.Removed -= OnGameCounterRemoved;

            levelCounters.Added -= OnLevelCounterAdded;
            levelCounters.Removed -= OnLevelCounterRemoved;
        }

        public List<ICarCounter> GetAllLevelCounters()
        {
            var result = new List<ICarCounter>();
            foreach (var counter in levelCounters.GetAll())
            {
                result.Add(counter);
            }
            return result;
        }

        private IEnumerator DoLoadLevel()
        {
            FindLevelCounters();
            yield return null;
        }
        private void FindLevelCounters()
        {
            foreach (var counter in gameCounters.GetAll())
            {
                if (counter.data.town == town.town)
                    levelCounters.Add(counter);
            }
        }

        private IEnumerator DoExitLevel()
        {
            ClearLevelCounters();
            yield return null;
        }
        public void ClearLevelCounters()
        {
            foreach (var counter in levelCounters.GetAll())
            {
                levelCounters.Remove(counter);
            }
        }

        private IEnumerator DoLoadApp()
        {
            FindGameCounters();
            SetupGameCounters();
            yield return null;
        }
        private void FindGameCounters()
        {
            var parents = AScene.GetCarCounterParents();
            foreach (var parent in parents)
            {
                var childs = UGameObject.GetChilds(parent);
                foreach (var child in childs)
                {
                    var counter = child.gameObject.GetComponent<ACarCounter>();
                    gameCounters.Add(counter);
                }
            }
        }
        private void SetupGameCounters()
        {
            foreach (var counter in gameCounters.GetAll())
            {
                var text = counter.transform.parent.parent.name;
                counter.data.town = UEnums.Parse<ETownID>(text);

                text = counter.gameObject.name;
                counter.data.direction = UEnums.Parse<EDirection>(text);
            }
        }

        /// ======================================================================

        private void OnGameCounterAdded(ACarCounter counter)
        {
            GameCounterAdded(counter);
        }
        private void OnGameCounterRemoved(ACarCounter counter)
        {
            GameCounterRemoved(counter);
        }

        private void OnLevelCounterAdded(ACarCounter counter)
        {
            LevelCounterAdded(counter);
        }
        private void OnLevelCounterRemoved(ACarCounter counter)
        {
            LevelCounterRemoved(counter);
        }

        /// ======================================================================
    }
}