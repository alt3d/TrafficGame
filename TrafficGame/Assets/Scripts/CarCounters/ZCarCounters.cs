﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarCounters;

namespace Alt3d.TrafficGame
{
	public static class ZCarCounters 
	{
        /// ======================================================================

        public static void Init(IContainer container)
        {
            var repo = container.AddComponent<ACarCountersRepository>();
            container.Register<ACarCountersRepository>(repo);
            container.Register<ICarCountersRepository>(repo);
        }

        /// ======================================================================
    }
}