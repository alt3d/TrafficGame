﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarRoute 
	{
        /// ======================================================================

        ETownID town { get; }
        EDirection direction { get; }

        bool isStartBusy { get; }
        Vector3 startPosition { get; }
        Quaternion startRotation { get; }

        /// ======================================================================

        event Action<ICarRoute, ICar> CarEnteredToStartArea;
        event Action<ICarRoute, ICar> CarExitedFromStartArea;

        event Action<ICarRoute, ICar> CarEnteredToFinishArea;
        event Action<ICarRoute, ICar> CarExitedFromFinishArea;

        /// ======================================================================

        bool CastStartSpace(Vector3 size);

        /// ======================================================================
    }
}