﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarRoutesRepository 
	{
        /// ======================================================================

        event Action<ICarRoute> GameRouteAdded;
        event Action<ICarRoute> GameRouteRemoved;

        event Action<ICarRoute> LevelRouteAdded;
        event Action<ICarRoute> LevelRouteRemoved;

        /// ======================================================================

        List<ICarRoute> GetAllLevelRoutes();

        /// ======================================================================
    }
}