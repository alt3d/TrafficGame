﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.CarRoutes
{
	public class ACarRoutesRepository : MonoBehaviour, ICarRoutesRepository
    {
        /// ======================================================================

        private readonly AList<ACarRoute> gameRoutes = new AList<ACarRoute>();
        private readonly AList<ACarRoute> levelRoutes = new AList<ACarRoute>();

        private ITown town;

        private IAppActionsRepository appActions;
        private ILevelActionsRepository levelActions;

        private DAppLoadAction appLoadAction;
        private DLevelLoadAction levelLoadAction;
        private DLevelExitAction levelExitAction;

        /// ======================================================================

        public event Action<ICarRoute> GameRouteAdded = delegate { };
        public event Action<ICarRoute> GameRouteRemoved = delegate { };

        public event Action<ICarRoute> LevelRouteAdded = delegate { };
        public event Action<ICarRoute> LevelRouteRemoved = delegate { };

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();

            appActions = AServices.Get<IAppActionsRepository>();
            levelActions = AServices.Get<ILevelActionsRepository>();

            appLoadAction = new DAppLoadAction(DoLoadApp);
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
            levelExitAction = new DLevelExitAction(DoExitLevel);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
            levelActions.Add(levelLoadAction);
            levelActions.Add(levelExitAction);

            gameRoutes.Added += OnGameRouteAdded;
            gameRoutes.Removed += OnGameRouteRemoved;

            levelRoutes.Added += OnLevelRouteAdded;
            levelRoutes.Removed += OnLevelRouteRemoved;
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
            levelActions.Remove(levelLoadAction);
            levelActions.Remove(levelExitAction);

            gameRoutes.Added -= OnGameRouteAdded;
            gameRoutes.Removed -= OnGameRouteRemoved;

            levelRoutes.Added -= OnLevelRouteAdded;
            levelRoutes.Removed -= OnLevelRouteRemoved;
        }

        public List<ICarRoute> GetAllLevelRoutes()
        {
            var result = new List<ICarRoute>();
            foreach (var route in levelRoutes.GetAll())
            {
                result.Add(route);
            }
            return result;
        }
   
        private IEnumerator DoLoadLevel()
        {
            FindLevelRoutes();
            yield return null;
        }
        private void FindLevelRoutes()
        {
            var townID = town.town;
            foreach (var route in gameRoutes.GetAll())
            {
                if (route.data.town == townID)
                    levelRoutes.Add(route);
            }
        }

        private IEnumerator DoExitLevel()
        {
            ClearLevelRoutes();
            yield return null;
        }
        private void ClearLevelRoutes()
        {
            foreach (var route in levelRoutes.GetAll())
            {
                levelRoutes.Remove(route);
            }
        }

        private IEnumerator DoLoadApp()
        {
            FindGameRoutes();
            SetupGameRoutes();
            yield return null;
        }
        private void FindGameRoutes()
        {
            var parents = AScene.GetCarRoutesParents();
            foreach (var parent in parents)
            {
                var childs = UGameObject.GetChilds(parent);
                foreach (var child in childs)
                {
                    var route = child.gameObject.GetComponent<ACarRoute>();
                    gameRoutes.Add(route);
                }
            }
        }
        private void SetupGameRoutes()
        {
            foreach (var route in gameRoutes.GetAll())
            {
                var text = route.transform.parent.parent.name;
                route.data.town = UEnums.Parse<ETownID>(text);

                text = route.gameObject.name;
                route.data.direction = UEnums.Parse<EDirection>(text);
            }
        }

        /// ======================================================================

        private void OnGameRouteAdded(ACarRoute route)
        {
            GameRouteAdded(route);
        }
        private void OnGameRouteRemoved(ACarRoute route)
        {
            GameRouteRemoved(route);
        }

        private void OnLevelRouteAdded(ACarRoute route)
        {
            LevelRouteAdded(route);
        }
        private void OnLevelRouteRemoved(ACarRoute route)
        {
            LevelRouteRemoved(route);
        }

        /// ======================================================================
    }
}