﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarRoutes.Routes;

namespace Alt3d.TrafficGame.CarRoutes
{
	public class ACarRoute : MonoBehaviour, ICarRoute
	{
        /// ======================================================================

        public ACarRouteData data;
        public ACarDetectionArea startArea;
        public ACarDetectionArea finishArea;

        /// ======================================================================

        public ETownID town
        {
            get { return data.town; }
        }
        public EDirection direction
        {
            get { return data.direction; }
        }
        
        public bool isStartBusy
        {
            get { return data.isStartBusy; }
        }
        public Vector3 startPosition
        {
            get { return startArea.transform.position; }
        }
        public Quaternion startRotation
        {
            get { return startArea.transform.rotation; }
        }

        /// ======================================================================

        public event Action<ICarRoute, ICar> CarEnteredToStartArea = delegate { };
        public event Action<ICarRoute, ICar> CarExitedFromStartArea = delegate { };

        public event Action<ICarRoute, ICar> CarEnteredToFinishArea = delegate { };
        public event Action<ICarRoute, ICar> CarExitedFromFinishArea = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            startArea.CarEntered += OnCarEnteredToStartArea;
            startArea.CarExited += OnCarExitedFromStartArea;

            finishArea.CarEntered += OnCarEnteredToFinishArea;
            finishArea.CarExited += OnCarExitedFromFinishArea;
        }
        private void OnDisable()
        {
            startArea.CarEntered -= OnCarEnteredToStartArea;
            startArea.CarExited -= OnCarExitedFromStartArea;

            finishArea.CarEntered -= OnCarEnteredToFinishArea;
            finishArea.CarExited -= OnCarExitedFromFinishArea;
        }

        public bool CastStartSpace(Vector3 size)
        {
            var center = startArea.transform.position;
            center += Vector3.up * size.y * 0.5f;

            var extents = size * 0.6f; // 0.6 - с запасом
            var orientation = startArea.transform.rotation;
            var mask = 1 << ALayers.carAreas;

            var colliders = Physics.OverlapBox(center, extents, orientation, mask);
            return colliders.Length == 0;
        }

        /// ======================================================================

        private void OnCarEnteredToStartArea(ICar car)
        {
            CarEnteredToStartArea(this, car);
        }
        private void OnCarExitedFromStartArea(ICar car)
        {
            CarExitedFromStartArea(this, car);
        }
        
        private void OnCarEnteredToFinishArea(ICar car)
        {
            CarEnteredToFinishArea(this, car);
        }
        private void OnCarExitedFromFinishArea(ICar car)
        {
            CarExitedFromFinishArea(this, car);
        }

        /// ======================================================================
    }
}