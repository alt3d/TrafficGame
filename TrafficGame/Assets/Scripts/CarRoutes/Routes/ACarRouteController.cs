﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.CarRoutes.Routes
{
	public class ACarRouteController : MonoBehaviour
	{
        /// ======================================================================

        public ACarRouteData data;
        public ACarDetectionArea startArea;

        private readonly AList<ICar> startCars = new AList<ICar>();

        private ILevel level;
        private ITown town;

        private ILevelActionsRepository levelActions;
        private DLevelLoadAction levelLoadAction;
        
        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
            town = AServices.Get<ITown>();

            levelActions = AServices.Get<ILevelActionsRepository>();
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChange;
            startArea.CarEntered += OnCarEnteredToStartArea;
            startArea.CarExited += OnCarExitedFromStartArea;

            levelActions.Add(levelLoadAction);
        }
        private void OnDisable()
        {
            level.StateChanged += OnLevelStateChange;
            startArea.CarEntered -= OnCarEnteredToStartArea;
            startArea.CarExited -= OnCarExitedFromStartArea;

            levelActions.Remove(levelLoadAction);
        }

        private void OnCarEnteredToStartArea(ICar car)
        {
            if (level.state != ELevelState.Play) return;
            if (town.state != ETownState.Play) return;

            startCars.Add(car);
            UpdateStartBusyState();
            UpdateStartAreaSize(car.areaSize);
        }
        private void OnCarExitedFromStartArea(ICar car)
        {
            if (level.state != ELevelState.Play) return;
            if (town.state != ETownState.Play) return;

            startCars.Remove(car);
            data.isStartBusy = false;
        }

        private void UpdateStartBusyState()
        {
            data.isStartBusy = startCars.count > 0;
        }
        private void UpdateStartAreaSize(Vector3 carSize)
        {
            var size = carSize + ZCarRoutes.startAreaSizeDelta;
            startArea.volume.size = size;

            var center = Vector3.zero;
            center.y = size.y * 0.5f;
            startArea.volume.center = center; 
        }

        private void OnLevelStateChange(ELevelState state)
        {
            if (state == ELevelState.Play)
            {
                startCars.Clear();
                data.isStartBusy = false;

                var size = ZCarRoutes.startAreaSize;
                UpdateStartAreaSize(size);
            }
        }

        private IEnumerator DoLoadLevel()
        {
            ResetData();
            yield return null;
        }
        private void ResetData()
        {
            startCars.Clear();
        }

        /// ======================================================================
    }
}