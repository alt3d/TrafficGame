﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.CarRoutes.Routes
{
	public class ACarRouteData : MonoBehaviour
	{
        /// ======================================================================

        private ETownID _town = ETownID.City;
        private EDirection _direction = EDirection.Down;
        private bool _isStartBusy = false;

        private ILevelActionsRepository levelActions;
        private DLevelLoadAction levelLoadAction;

        /// ======================================================================

        public ETownID town
        {
            get { return _town; }
            set
            {
                _town = value;
            }
        }
        public EDirection direction
        {
            get { return _direction; }
            set
            {
                _direction = value;
            }
        }
        public bool isStartBusy
        {
            get { return _isStartBusy; }
            set
            {
                _isStartBusy = value;
            }
        }

        /// ======================================================================

        private void Awake()
        {
            levelActions = AServices.Get<ILevelActionsRepository>();
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
        }
        private void OnEnable()
        {
            levelActions.Add(levelLoadAction);
        }
        private void OnDisable()
        {
            levelActions.Remove(levelLoadAction);
        }

        private IEnumerator DoLoadLevel()
        {
            ResetData();
            yield return null;
        }
        private void ResetData()
        {
            isStartBusy = false;
        }

        /// ======================================================================
    }
}