﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarRoutes;

namespace Alt3d.TrafficGame
{
    public static class ZCarRoutes
    {
        /// ======================================================================

        public static readonly Vector3 startAreaSize = new Vector3(5, 5, 5);
        public static readonly Vector3 startAreaSizeDelta = new Vector3(4.0f, 4.0f, 4.0f);

        /// ======================================================================

        public static bool IsPartOfRoute(Collider collider)
        {
            var layer = collider.gameObject.layer;

            if (layer == ALayers.carsRouteStartAreas) return true;
            if (layer == ALayers.carsRouteFinishAreas) return true;
            return false;
        }
        public static bool IsStartArea(Collider collider)
        {
            var layer = collider.gameObject.layer;
            return layer == ALayers.carsRouteStartAreas;
        }
        public static bool IsFinishArea(Collider collider)
        {
            var layer = collider.gameObject.layer;
            return layer == ALayers.carsRouteFinishAreas;
        }

        public static ICarRoute GetRoute(Collider collider)
        {
            var layer = collider.gameObject.layer;

            if (layer == ALayers.carsRouteStartAreas || layer == ALayers.carsRouteFinishAreas)
            {
                var parent = collider.transform.parent;
                return parent.GetComponent<ACarRoute>();
            }

            throw new AException(collider);
        }

        /// ======================================================================

        public static void Init(IContainer container)
        {
            var repo = container.AddComponent<ACarRoutesRepository>();
            container.Register<ACarRoutesRepository>(repo);
            container.Register<ICarRoutesRepository>(repo);
        }

        /// ======================================================================
    }
}