﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Cars.Cars;

namespace Alt3d.TrafficGame.Cars
{
    public class ACar : MonoBehaviour, ICar
    {
        /// ======================================================================

        public ACarData data;
        public ACarArea area;
        public ACarSensor sensor;

        /// ======================================================================

        public ECarID id
        {
            get { return data.id; }
        }
        public ECarState state
        {
            get { return data.state; }
        }
        public Vector3 areaSize
        {
            get { return area.volume.size; }
        }
        public Vector3 position
        {
            get { return transform.position; }
        }
      
        /// ======================================================================

        public event Action<ICar, ECarState> StateChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            data.StateChanged += OnStateChanged;
        }
        private void OnDisable()
        {
            data.StateChanged -= OnStateChanged;
        }

        public bool IsWasStop()
        {
            if (data.isWasStopBeforeLight)
                return true;

            if (data.isWasStopBeforeOtherCar)
                return true;

            return false;
        }
        public int GetPassReward(ICarDescsRepository repo)
        {
            var desc = repo.Get(data.id);
            if (IsWasStop())
                return desc.passReward;
            else
                return desc.passReward * 2;
        }

        private void OnStateChanged(ECarState state)
        {
            StateChanged(this, state);
        }

        /// ======================================================================
    }
}