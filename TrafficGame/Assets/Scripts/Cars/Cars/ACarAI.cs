﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
    public class ACarAI : MonoBehaviour
    {
        /// ======================================================================

        public ACarData data;
        public ACarArea area;
        public ACarPhysic physic;

        /// ======================================================================

        private void OnEnable()
        {
            data.StateChanged += OnStateChanged;
            physic.Collisioned += OnPhysicCollisioned;
            area.RouteFinishEnter += OnRouteFinishAreaEnter;

            data.TrafficLightPositionChanged += OnTrafficLightPositionChanged;
            data.TrafficLightDistanceChanged += OnTrafficLightDistanceChanged;
            data.OtherCarsDistanceChanged += OnOtherCarsDistanceChanged;

            data.TrafficLightChanging += OnTrafficLightChanging;
            data.TrafficLightChanged += OnTrafficLightChanged;
        }
        private void OnDisable()
        {
            data.StateChanged -= OnStateChanged;
            physic.Collisioned -= OnPhysicCollisioned;
            area.RouteFinishEnter -= OnRouteFinishAreaEnter;

            data.TrafficLightPositionChanged -= OnTrafficLightPositionChanged;
            data.TrafficLightDistanceChanged -= OnTrafficLightDistanceChanged;
            data.OtherCarsDistanceChanged -= OnOtherCarsDistanceChanged;

            data.TrafficLightChanging -= OnTrafficLightChanging;
            data.TrafficLightChanged -= OnTrafficLightChanged;
        }

        private void OnStateChanged(ECarState state)
        {
            switch (state)
            {
                case ECarState.Start:
                    data.state = ECarState.Drive;
                    break;
                case ECarState.StopBeforeCar:
                    data.isWasStopBeforeOtherCar = true;
                    break;
                case ECarState.StopBeforeLight:
                    data.isWasStopBeforeLight = true;
                    break;
            }
        }
        private void OnPhysicCollisioned(Collision collision)
        {
            if (data.state != ECarState.Crash)
                data.state = ECarState.Crash;
        }

        private void OnRouteFinishAreaEnter(ICarRoute route)
        {
            if (data.state == ECarState.Drive)
                if (route == data.route)
                    data.state = ECarState.Finish;
        }

        private void OnTrafficLightPositionChanged(ETrafficLightPosition position)
        {
            if (position == ETrafficLightPosition.Near)
                if (data.trafficLight != null)
                    if (data.trafficLight.signal == ETrafficLightSignal.Red)
                        data.state = ECarState.StopBeforeLight;

            if (position == ETrafficLightPosition.After)
                if (data.state == ECarState.StopBeforeLight)
                    data.state = ECarState.Drive;
        }
        private void OnTrafficLightDistanceChanged(ETrafficLightDistance distance)
        {
            if (distance == ETrafficLightDistance.Near)
                if (data.trafficLightPosition == ETrafficLightPosition.Before)
                    data.trafficLightPosition = ETrafficLightPosition.Near;

            if (distance == ETrafficLightDistance.Far)
                if (data.trafficLightPosition == ETrafficLightPosition.Near)
                    data.trafficLightPosition = ETrafficLightPosition.After;

        }
        private void OnOtherCarsDistanceChanged(EOtherCarsDistance distance)
        {
            if (data.trafficLight == null) return;
            if (data.trafficLightPosition == ETrafficLightPosition.After) return;

            if (distance == EOtherCarsDistance.Near)
                if (data.state == ECarState.Drive)
                    data.state = ECarState.StopBeforeCar;

            if (distance == EOtherCarsDistance.Far)
                if (data.state == ECarState.StopBeforeCar)
                    data.state = ECarState.Drive;
        }

        private void OnTrafficLightChanging(ITrafficLight light)
        {
            if (light != null)
                light.SignalChanged -= OnTrafficLightSignalChanged;
        }
        private void OnTrafficLightChanged(ITrafficLight light)
        {
            if (light != null)
                light.SignalChanged += OnTrafficLightSignalChanged;
        }
        private void OnTrafficLightSignalChanged(ITrafficLight light, ETrafficLightSignal signal)
        {
            if (data.trafficLightPosition != ETrafficLightPosition.Near)
                return;

            if (signal == ETrafficLightSignal.Yellow)
                data.state = ECarState.Drive;

            if (data.state == ECarState.StopBeforeLight)
                if (signal == ETrafficLightSignal.Green)
                    data.state = ECarState.Drive;

            if (data.state == ECarState.Drive)
                if (signal == ETrafficLightSignal.Red)
                    data.state = ECarState.StopBeforeLight;
        }

        /// ======================================================================
    }
}