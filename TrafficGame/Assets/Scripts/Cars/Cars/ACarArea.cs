﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
	public class ACarArea : MonoBehaviour
	{
        /// ======================================================================

        public BoxCollider volume;

        /// ======================================================================

        public event Action<ICar> OtherCarEntered = delegate { };

        public event Action<ICarRoute> RouteStartExit = delegate { };
        public event Action<ICarRoute> RouteFinishEnter = delegate { };
      
        /// ======================================================================

        private void OnTriggerEnter(Collider other)
        {
            if (ZCars.IsPartOfCar(other))
                OtherCarEntered(ZCars.GetCar(other));

            if (ZCarRoutes.IsPartOfRoute(other))
                if (ZCarRoutes.IsFinishArea(other))
                    RouteFinishEnter(ZCarRoutes.GetRoute(other));
        }
        private void OnTriggerExit(Collider other)
        {
            if (ZCarRoutes.IsPartOfRoute(other))
                if (ZCarRoutes.IsStartArea(other))
                    RouteStartExit(ZCarRoutes.GetRoute(other));
        }

        /// ======================================================================
    }
}