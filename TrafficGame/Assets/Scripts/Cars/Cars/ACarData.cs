﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
    public class ACarData : MonoBehaviour
    {
        /// ======================================================================

        private ECarState _state;
        private ETrafficLightPosition _trafficLightPosition;
        private ETrafficLightDistance _trafficLightDistance;
        private EOtherCarsDistance _otherCarsDistance;

        private ECarID _id;
        private ICarRoute _route;
        private ITrafficLight _trafficLight;

        private bool _isWasStopBeforeLight = false;
        private bool _isWasStopBeforeOtherCar = false;

        /// ======================================================================

        public ECarState state
        {
            get { return _state; }
            set
            {
                _state = value;
                StateChanged(_state);
            }
        }
        public ETrafficLightPosition trafficLightPosition
        {
            get { return _trafficLightPosition; }
            set
            {
                _trafficLightPosition = value;
                TrafficLightPositionChanged(_trafficLightPosition);
            }
        }
        public ETrafficLightDistance trafficLightDistance
        {
            get { return _trafficLightDistance; }
            set
            {
                _trafficLightDistance = value;
                TrafficLightDistanceChanged(_trafficLightDistance);
            }
        }
        public EOtherCarsDistance otherCarsDistance
        {
            get { return _otherCarsDistance; }
            set
            {
                _otherCarsDistance = value;
                OtherCarsDistanceChanged(_otherCarsDistance);
            }
        }

        public ECarID id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }
        public ICarRoute route
        {
            get { return _route; }
            set
            {
                RouteChanging(_route);
                _route = value;
                RouteChanged(_route);
            }
        }
        public ITrafficLight trafficLight
        {
            get { return _trafficLight; }
            set
            {
                TrafficLightChanging(_trafficLight);
                _trafficLight = value;
                TrafficLightChanged(_trafficLight);
            }
        }

        public bool isWasStopBeforeLight
        {
            get { return _isWasStopBeforeLight; }
            set
            {
                _isWasStopBeforeLight = value;
            }
        }
        public bool isWasStopBeforeOtherCar
        {
            get { return _isWasStopBeforeOtherCar; }
            set
            {
                _isWasStopBeforeOtherCar = value;
            }
        }

        /// ======================================================================

        public event Action<ECarState> StateChanged = delegate { };
        public event Action<ETrafficLightPosition> TrafficLightPositionChanged = delegate { };
        public event Action<ETrafficLightDistance> TrafficLightDistanceChanged = delegate { };
        public event Action<EOtherCarsDistance> OtherCarsDistanceChanged = delegate { };

        public event Action<ICarRoute> RouteChanging = delegate { };
        public event Action<ICarRoute> RouteChanged = delegate { };

        public event Action<ITrafficLight> TrafficLightChanging = delegate { };
        public event Action<ITrafficLight> TrafficLightChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            StateChanged += OnStateChanged;
        }
        private void OnDisable()
        {
            StateChanged -= OnStateChanged;
        }

        private void OnStateChanged(ECarState state)
        {
            switch (state)
            {
                case ECarState.Idle:
                    ResetData();
                    ResetReferences();
                    break;
            }
        }
        private void ResetData()
        {
            _trafficLightPosition = ETrafficLightPosition.Before;
            _trafficLightDistance = ETrafficLightDistance.Far;
            _otherCarsDistance = EOtherCarsDistance.Far;

            _isWasStopBeforeLight = false;
            _isWasStopBeforeOtherCar = false;
        }
        private void ResetReferences()
        {
            _route = null;
            _trafficLight = null;
        }

        /// ======================================================================
    }
}