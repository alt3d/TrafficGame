﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
	public class ACarMotor : MonoBehaviour
	{
        /// ======================================================================

        public ACarData data;
        public Rigidbody body;

        /// ======================================================================

        private void FixedUpdate()
        {
            switch (data.state)
            {
                case ECarState.Drive:
                    Drive();
                    break;
                case ECarState.StopBeforeLight:
                case ECarState.StopBeforeCar:
                    Wait();
                    break;
            }
        }
        private void Drive()
        {
            body.velocity = transform.forward * ZCars.carSpeed;
        }
        private void Wait()
        {
            body.velocity = Vector3.zero;
        }

        /// ======================================================================
    }
}