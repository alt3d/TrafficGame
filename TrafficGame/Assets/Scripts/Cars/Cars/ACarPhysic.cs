﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
    public class ACarPhysic : MonoBehaviour
    {
        /// ======================================================================

        public ACarData data;
        public Rigidbody body;
        public Transform com;

        /// ======================================================================

        public event Action<Collision> Collisioned = delegate { };

        /// ======================================================================

        private void Awake()
        {
            ResetCom();
        }
        private void OnEnable()
        {
            data.StateChanged += OnStateChanged;
        }
        private void OnDisable()
        {
            data.StateChanged -= OnStateChanged;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Collisioned(collision);   
        }

        private void OnStateChanged(ECarState state)
        {
            switch (state)
            {
                case ECarState.Idle:
                case ECarState.Start:
                    MakeKinematic();
                    ResetCom();
                    break;
                case ECarState.Crash:
                    MakePhysic();
                    break;
            }
        }
        private void MakePhysic()
        {
            body.useGravity = true;
        }
        private void MakeKinematic()
        {
            body.isKinematic = true;
            body.useGravity = false;
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
            body.isKinematic = false;
        }
        private void ResetCom()
        {
            body.centerOfMass = com.localPosition;
        }

        /// ======================================================================
    }
}