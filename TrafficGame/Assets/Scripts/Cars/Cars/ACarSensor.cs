﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
    public class ACarSensor : MonoBehaviour
    {
        /// ======================================================================

        public ACarData data;

        private const int otherCarsMask = 1 << ALayers.carAreas;
        private const int trafficLighstMask = 1 << ALayers.trafficLightAreas;

        private const float otherCarsDistance = 2f;
        private const float trafficLightsDistance = 2f;

        /// ======================================================================

        private void Update()
        {
            switch (data.state)
            {
                case ECarState.Drive:
                case ECarState.StopBeforeCar:
                case ECarState.StopBeforeLight:
                    UpdateTrafficLightsDistance();
                    UpdateOtherCarsDistance();
                    break;
            }
        }
        private void UpdateOtherCarsDistance()
        {
            if (Physics.Raycast(transform.position, transform.forward, otherCarsDistance, otherCarsMask))
            {
                data.otherCarsDistance = EOtherCarsDistance.Near;
            }
            else
            {
                data.otherCarsDistance = EOtherCarsDistance.Far;
            }
        }
        private void UpdateTrafficLightsDistance()
        {
            if (Physics.Raycast(transform.position, transform.forward, trafficLightsDistance, trafficLighstMask))
            {
                data.trafficLightDistance = ETrafficLightDistance.Near;
            }
            else
            {
                data.trafficLightDistance = ETrafficLightDistance.Far;
            }
        }

        /// ======================================================================
    }
}