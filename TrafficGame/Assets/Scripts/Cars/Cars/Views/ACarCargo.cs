﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
	public class ACarCargo : MonoBehaviour
	{
        /// ======================================================================

        [SerializeField] protected ACarData data;
        [SerializeField] protected Rigidbody carBody;
        [SerializeField] protected Transform com;
        [SerializeField] protected AFixedJoint[] joints;
        [SerializeField] protected float force;
        [SerializeField] protected float mass;

        private Rigidbody[] bodies;

        /// ======================================================================

        private void Awake()
        {
            bodies = new Rigidbody[joints.Length];
            for (int i = 0; i < bodies.Length; i++)
            {
                bodies[i] = joints[i].GetComponent<Rigidbody>();
            }

            Setup();
        }
        private void OnEnable()
        {
            Restore();
            
            data.StateChanged += OnStateChanged;
        }
        private void OnDisable()
        {
            data.StateChanged -= OnStateChanged;
        }

        protected void Setup()
        {
            foreach (var item in joints)
            {
                item.Setup();
            }
        }
        private void Restore()
        {
            for (int i = 0; i < joints.Length; i++)
            {
                joints[i].Restore(force);
                joints[i].Attach(carBody);
                bodies[i].mass = 0.0f;
                bodies[i].isKinematic = true;
                bodies[i].useGravity = false;
                bodies[i].velocity = Vector3.zero;
                bodies[i].angularVelocity = Vector3.zero;
                bodies[i].centerOfMass = com.localPosition;
                bodies[i].isKinematic = false;
            }
        }

        private void OnStateChanged(ECarState state)
        {
            if (state == ECarState.Crash)
            {
                foreach (var body in bodies)
                {
                    body.mass = mass;
                    body.useGravity = true;
                    body.ResetCenterOfMass();
                }
            }
        }

        /// ======================================================================
    }
}