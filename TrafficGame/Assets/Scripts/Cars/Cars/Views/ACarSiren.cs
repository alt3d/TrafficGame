﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars.Cars
{
    public class ACarSiren : MonoBehaviour
    {
        /// ======================================================================

        [SerializeField] protected ACarData data;
        [SerializeField] protected float delay;

        [Space]
        [SerializeField] protected Renderer[] leftRenderers;
        [SerializeField] protected Material leftMaterialOff;
        [SerializeField] protected Material leftMaterialOn;

        [Space]
        [SerializeField] protected Renderer[] rightRenderers;
        [SerializeField] protected Material rightMaterialOff;
        [SerializeField] protected Material rightMaterialOn;

        [Space]
        [SerializeField] protected Projector projector;
        [SerializeField] protected Transform projectorParent;
        [SerializeField] protected Material leftProjectorMaterial;
        [SerializeField] protected Material rightProjectorMaterial;
       
        [Space]
        [SerializeField] protected AudioSource source;

        private Coroutine cFlash;
        private const float leftProjectorAngle = 90;
        private const float rightProjectorAngle = 270;

        private EAxis _axis = EAxis.Left;
        private EAxis side
        {
            get { return _axis; }
            set
            {
                _axis = value;
                OnSideChanged(_axis);
            }
        }

        /// ======================================================================

        private void OnEnable()
        {
            data.StateChanged += OnStateChanged;
        }
        private void OnDisable()
        {
            data.StateChanged -= OnStateChanged;
        }

        private void OnStateChanged(ECarState state)
        {
            switch (state)
            {
                case ECarState.Start:
                    StartFlash();
                    StartSound();
                    break;
                case ECarState.Idle:
                    StopFlash();
                    StopSound();
                    break;
                case ECarState.Crash:
                    StopSound();
                    break;
            }
        }
        private void OnSideChanged(EAxis axis)
        {
            if (axis == EAxis.Left)
            {
                ChangeMaterial(leftRenderers, leftMaterialOn);
                ChangeMaterial(rightRenderers, rightMaterialOff);

                RotateProjector(leftProjectorAngle);
                ChangeMaterial(projector, leftProjectorMaterial);
            }
            else if (axis == EAxis.Right)
            {
                ChangeMaterial(leftRenderers, leftMaterialOff);
                ChangeMaterial(rightRenderers, rightMaterialOn);

                RotateProjector(rightProjectorAngle);
                ChangeMaterial(projector, rightProjectorMaterial);
            }
            else
            {
                throw new Exception(axis.ToString());
            }
        }

        private void StartFlash()
        {
            if (projector != null)
                projector.enabled = true;

            if (cFlash != null) StopCoroutine(cFlash);
            cFlash = StartCoroutine(DoFlash());
        }
        private void StopFlash()
        {
            if (projector != null)
                projector.enabled = false;

            if (cFlash != null) StopCoroutine(cFlash);
        }
        private IEnumerator DoFlash()
        {
            side = EAxis.Right;
            while (true)
            {
                side = UAxis.GetOpposite(side);
                yield return new WaitForSeconds(delay);
            }
        }

        private void StartSound()
        {
            source.Play();
        }
        private void StopSound()
        {
            source.Stop();
        }

        private void ChangeMaterial(Renderer[] renderers, Material material)
        {
            foreach (var renderer in renderers)
            {
                renderer.sharedMaterial = material;
            }
        }

        private void ChangeMaterial(Projector projector, Material material)
        {
            if (projector == null)
                return;

            projector.material = material;
        }
        private void RotateProjector(float angle)
        {
            if (projectorParent == null)
                return;

            var angles = projectorParent.localEulerAngles;
            angles.y = angle;
            projectorParent.localEulerAngles = angles;
        }

        /// ======================================================================
    }
}