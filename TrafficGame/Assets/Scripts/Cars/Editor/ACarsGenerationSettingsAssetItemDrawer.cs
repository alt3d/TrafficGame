﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEditor;

namespace Alt3d.TrafficGame.Cars
{
    [CustomPropertyDrawer(typeof(ACarsGenerationSettingsAsset.AItem))]
    public class ACarsGenerationSettingsAssetItemDrawer : PropertyDrawer
	{
        /// ======================================================================

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 1;

            var carRect = new Rect(position.x, position.y, 150, position.height);
            var cityRect = new Rect(position.x + 155, position.y, 75, position.height);
            var villageRect = new Rect(position.x + 240, position.y, 75, position.height);

            EditorGUI.PropertyField(carRect, property.FindPropertyRelative("car"), GUIContent.none);
            EditorGUI.PropertyField(cityRect, property.FindPropertyRelative("city"), GUIContent.none);
            EditorGUI.PropertyField(villageRect, property.FindPropertyRelative("village"), GUIContent.none);

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        /// ======================================================================
    }
}