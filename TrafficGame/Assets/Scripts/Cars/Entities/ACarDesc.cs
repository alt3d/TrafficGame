﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarDesc : ICarDesc
	{
		/// ======================================================================

        public ECarID id { get; set; }
        public GameObject prefab { get; set; }
        public int passReward { get; set; }
        public int crashReward { get; set; }
        public int freePassRewardFactor { get; set; }

		/// ======================================================================
	}
}