﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarDescAsset : ScriptableObject
    {
        /// ======================================================================

        [SerializeField] protected GameObject _prefab;
        [SerializeField] protected int _passReward = 100;
        [SerializeField] protected int _crashReward = 50;
        [SerializeField] protected int _freePassRewardFactor = 1;

        /// ======================================================================

        public GameObject prefab
        {
            get { return _prefab; }
        }
        public int passReward
        {
            get { return _passReward; }
        }
        public int crashReward
        {
            get { return _crashReward; }
        }
        public int freePassRewardFactor
        {
            get { return _freePassRewardFactor; }
        }

        /// ======================================================================
    }
}