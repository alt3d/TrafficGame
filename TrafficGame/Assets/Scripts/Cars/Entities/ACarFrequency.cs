﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public class ACarFrequency : ICarFrequency
	{
        /// ======================================================================

        public ECarID car { get; set; }
        public int weight { get; set; }

        /// ======================================================================
    }
}