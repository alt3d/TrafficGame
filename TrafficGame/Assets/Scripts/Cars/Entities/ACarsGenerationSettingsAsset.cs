﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsGenerationSettingsAsset : ScriptableObject, ICarsGenerationSettings
	{
        /// ======================================================================

        [SerializeField] protected List<AItem> items = new List<AItem>();

        /// ======================================================================

        public List<ICarFrequency> GetFrequency(ETownID town)
        {
            var result = new List<ICarFrequency>();
            foreach (var item in items)
            {
                var frequency = new ACarFrequency();
                frequency.car = item.car;

                switch (town)
                {
                    case ETownID.City:
                        frequency.weight = item.city;
                        break;
                    case ETownID.Village:
                        frequency.weight = item.village;
                        break;
                    default:
                        throw new Exception(town.ToString());
                }
                result.Add(frequency);
            }

            return result;
        }

        /// ======================================================================

        [Serializable]
        public struct AItem
        {
            public ECarID car;
            //[Range(0, 100)]
            public int city;
            //[Range(0, 100)]
            public int village;
        }

		/// ======================================================================
	}
}