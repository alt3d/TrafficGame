﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICar 
	{
		/// ======================================================================

        ECarID id { get; }
        ECarState state { get; }
        Vector3 areaSize { get; }
        Vector3 position { get; }

        /// ======================================================================

        event Action<ICar, ECarState> StateChanged;

        /// ======================================================================

        bool IsWasStop();
        int GetPassReward(ICarDescsRepository repo);

        /// ======================================================================
    }
}