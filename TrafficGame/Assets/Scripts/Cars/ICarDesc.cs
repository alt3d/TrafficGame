﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarDesc 
	{
		/// ======================================================================

        ECarID id { get; }
        GameObject prefab { get; }
        int passReward { get; }
        int crashReward { get; }
        int freePassRewardFactor { get; }

        /// ======================================================================
    }
}