﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarDescsRepository 
	{
        /// ======================================================================

        ICarDesc Get(ECarID id);
        List<ICarDesc> GetAll();

		/// ======================================================================
	}
}