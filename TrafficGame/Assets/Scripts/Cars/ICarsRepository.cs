﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ICarsRepository 
	{
        /// ======================================================================

        event Action<ICar> LevelCarAdded;
        event Action<ICar> LevelCarRemoved;

        event Action<ICar> PoolCarAdded;
        event Action<ICar> PoolCarRemoved;

        /// ======================================================================
    }
}