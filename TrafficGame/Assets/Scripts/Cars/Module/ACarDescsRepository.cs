﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarDescsRepository : MonoBehaviour, ICarDescsRepository
    {
        /// ======================================================================

        private readonly ADictionary<ECarID, ACarDesc> dic = new ADictionary<ECarID, ACarDesc>();

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
        }

        public ICarDesc Get(ECarID id)
        {
            return dic.Get(id);
        }
        public List<ICarDesc> GetAll()
        {
            var result = new List<ICarDesc>();
            foreach (var desc in dic.GetAllValues())
            {
                result.Add(desc);
            }
            return result;
        }

        private IEnumerator DoLoadApp()
        {
            LoadAssets();
            yield return null;
        }
        private void LoadAssets()
        {
            var path = AResources.carDescFolder;
            var assets = UResources.LoadAll<ACarDescAsset>(path);
            foreach (var asset in assets)
            {
                var desc = new ACarDesc();
                desc.id = UEnums.Parse<ECarID>(asset.name);
                desc.prefab = asset.prefab;
                desc.passReward = asset.passReward;
                desc.crashReward = asset.crashReward;
                desc.freePassRewardFactor = asset.freePassRewardFactor;
                dic.Add(desc.id, desc);
            }
        }

        /// ======================================================================
    }
}