﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsContainer : MonoBehaviour
	{
        /// ======================================================================

        private ACarsRepository repository;

        private Transform poolParent;
        private Transform levelParent;

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;
                
        /// ======================================================================

        private void Awake()
        {
            repository = AServices.Get<ACarsRepository>();

            appLoadAction = new DAppLoadAction(DoLoadApp);
            appActions = AServices.Get<IAppActionsRepository>();
        }
        private void OnEnable()
        {
            repository.LevelCarAdded += OnLevelCarAdded;
            repository.PoolCarAdded += OnPoolCarAdded;

            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            repository.LevelCarAdded -= OnLevelCarAdded;
            repository.PoolCarAdded -= OnPoolCarAdded;

            appActions.Remove(appLoadAction);
        }

        private void OnLevelCarAdded(ICar car)
        {
            MoveCarOnLevel(car as ACar);
        }
        private void MoveCarOnLevel(ACar car)
        {
            car.transform.parent = levelParent;
        }

        private void OnPoolCarAdded(ICar car)
        {
            MoveCarInPool(car as ACar);
        }
        private void MoveCarInPool(ACar car)
        {
            car.transform.parent = poolParent;
            car.transform.localPosition = Vector3.zero;
            car.transform.localEulerAngles = Vector3.zero;
        }

        private IEnumerator DoLoadApp()
        {
            CreateContainers();
            yield return null;
        }
        private void CreateContainers()
        {
            var name = AScene.carsRootGoName;
            var parent = new GameObject(name);

            name = AScene.poolCarsGoName;
            poolParent = new GameObject(name).transform;
            poolParent.parent = parent.transform;

            name = AScene.levelCarsGoName;
            levelParent = new GameObject(name).transform;
            levelParent.parent = parent.transform;

            poolParent.gameObject.SetActive(false);
            poolParent.transform.position = Vector3.down * 10f;
        }

        /// ======================================================================
    }
}