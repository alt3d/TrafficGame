﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsEvents : MonoBehaviour, ICarsEvents
	{
        /// ======================================================================

        private ICarsRepository cars;
        private ICarCountersRepository counters;
        private ITown town;

        /// ======================================================================

        public event Action<ICar> CarPassed = delegate { };
        public event Action<ICar> CarCrashed = delegate { };

        /// ======================================================================

        private void Awake()
        {
            cars = AServices.Get<ICarsRepository>();
            counters = AServices.Get<ICarCountersRepository>();
            town = AServices.Get<ITown>();
        }
        private void OnEnable()
        {
            cars.LevelCarAdded += OnCarAdded;
            cars.LevelCarRemoved += OnCarRemoved;

            counters.LevelCounterAdded += OnCounterAdded;
            counters.LevelCounterRemoved += OnCounterRemoved;
        }
        private void OnDisable()
        {
            cars.LevelCarAdded -= OnCarAdded;
            cars.LevelCarRemoved -= OnCarRemoved;

            counters.LevelCounterAdded -= OnCounterAdded;
            counters.LevelCounterRemoved -= OnCounterRemoved;
        }

        private void OnCarAdded(ICar car)
        {
            car.StateChanged += OnCarStateChanged;
        }
        private void OnCarRemoved(ICar car)
        {
            car.StateChanged -= OnCarStateChanged;
        }
        private void OnCarStateChanged(ICar car, ECarState state)
        {
            if (state == ECarState.Crash)
                CarCrashed(car);
        }

        private void OnCounterAdded(ICarCounter counter)
        {
            counter.CarExited += OnCarExitFromCounter;
        }
        private void OnCounterRemoved(ICarCounter counter)
        {
            counter.CarExited -= OnCarExitFromCounter;
        }
        private void OnCarExitFromCounter(ICarCounter counter, ICar car)
        {
            if (town.state == ETownState.Play)
                CarPassed(car);
        }

        /// ======================================================================
    }
}