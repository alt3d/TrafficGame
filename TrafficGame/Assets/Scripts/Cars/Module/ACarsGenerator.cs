﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsGenerator : MonoBehaviour
	{
        /// ======================================================================

        private ILevel level;
        private ITown town;
        private ICarRoutesRepository routesRepo;
        private ITrafficLightsRepository lightsRepo;

        private ACarsPool pool;
        private ACarDescsRepository descsRepository;

        private bool isGeneration;
        private Coroutine cGeneration;

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
            town = AServices.Get<ITown>();
            routesRepo = AServices.Get<ICarRoutesRepository>();
            lightsRepo = AServices.Get<ITrafficLightsRepository>();

            pool = AServices.Get<ACarsPool>();
            descsRepository = AServices.Get<ACarDescsRepository>();
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
            town.StateChanged += OnTownStateChanged;
        }
        private void OnDisable()
        {
            level.StateChanged -= OnLevelStateChanged;
            town.StateChanged -= OnTownStateChanged;
        }

        private void OnLevelStateChanged(ELevelState state)
        {
            switch (state)
            {
                case ELevelState.Play:
                    StartGeneration();
                    break;
                case ELevelState.Stop:
                    StopGeneration();
                    break;
            }
        }
        private void OnTownStateChanged(ETownState state)
        {
            switch (state)
            {
                case ETownState.Crash:
                    StopGeneration();
                    break;
            }
        }

        private void StartGeneration()
        {
            if (isGeneration) StopGeneration();

            isGeneration = true;
            cGeneration = StartCoroutine(DoGeneration());
        }
        private void StopGeneration()
        {
            if (isGeneration == false) return;

            isGeneration = false;
            if (cGeneration != null) StopCoroutine(cGeneration);
        }
        private IEnumerator DoGeneration()
        {
            while (true)
            {
                var delay = SeletDelay();
                yield return new WaitForSeconds(delay);

                var route = SelectRoute();
                if (route.isStartBusy == false)
                {
                    var desc = SelectCar();
                    var car = pool.Get(desc.id);

                    var size = car.areaSize;
                    if (route.CastStartSpace(size))
                    {
                        car.data.route = route;
                        car.transform.position = route.startPosition;
                        car.transform.rotation = route.startRotation;
                        car.transform.position += ZCars.carOffset;

                        var light = lightsRepo.GetLevel(route.town, route.direction);
                        if (light != null) car.data.trafficLight = light;

                        car.data.state = ECarState.Start;
                    }
                    else
                    {
                        Debug.Log("Будущая машина не помещается в стартовую зону");
                        pool.Push(car);
                    }
                }
                
                yield return null;
            }
        }

        private float SeletDelay()
        {
            var count = routesRepo.GetAllLevelRoutes().Count;
            var min = 1f / count;
            var max = 1f / count * 2f;
            return Random.Range(min, max);
        }
        private ICarDesc SelectCar()
        {
            var settings = AServices.Get<ICarsGenerationSettings>();
            var list = settings.GetFrequency(town.town);

            var sum = 0;
            foreach (var item in list)
            {
                sum += item.weight;
            }

            var weight = Random.Range(0, sum);

            var counter = 0;
            var id = list[0].car;
            bool found = false;
            for (int i = 0; i < list.Count; i++)
            {
                counter += list[i].weight;
                if (counter > weight)
                {
                    id = list[i].car;
                    found = true;
                    break;
                }
            }

            if (found == false)
                id = list[list.Count - 1].car;

            return descsRepository.Get(id);
        }
        private ICarRoute SelectRoute()
        {
            var routes = routesRepo.GetAllLevelRoutes();
            if (routes.Count == 0)
                throw new Exception(); 

            var random = Random.Range(0, routes.Count);
            return routes[random];
        }

        /// ======================================================================
    }
}