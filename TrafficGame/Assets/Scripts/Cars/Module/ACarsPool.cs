﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsPool : MonoBehaviour
	{
        /// ======================================================================

        private ACarsRepository carsRepo;
        private ACarDescsRepository descsRepo;
        
        /// ======================================================================

        private void Awake()
        {
            carsRepo = AServices.Get<ACarsRepository>();
            descsRepo = AServices.Get<ACarDescsRepository>();
        }

        public ACar Get(ECarID id)
        {
            var car = GetFromPool(id);
            if (car == null) car = CreateNew(id);

            carsRepo.RemovePoolCar(car);
            carsRepo.AddLevelCar(car);

            return car;
        }
        private ACar GetFromPool(ECarID id)
        {
            foreach (var car in carsRepo.GetPoolCars())
            {
                if (car.data.id == id) return car;
            }

            return null;
        }
        private ACar CreateNew(ECarID id)
        {
            var desc = descsRepo.Get(id);
            var go = UGameObject.Instantiate(desc.prefab);
            var car = go.GetComponent<ACar>();

            car.data.id = id;
            carsRepo.AddPoolCar(car);

            return car;
        }

        public void Push(ACar car)
        {
            carsRepo.RemoveLevelCar(car);
            carsRepo.AddPoolCar(car);
        }

        /// ======================================================================
    }
}