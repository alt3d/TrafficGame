﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsRepository : MonoBehaviour, ICarsRepository
	{
        /// ======================================================================

        private readonly AList<ACar> levelCars = new AList<ACar>();
        private readonly AList<ACar> poolCars = new AList<ACar>();

        /// ======================================================================

        public event Action<ICar> LevelCarAdded = delegate { };
        public event Action<ICar> LevelCarRemoved = delegate { };

        public event Action<ICar> PoolCarAdded = delegate { };
        public event Action<ICar> PoolCarRemoved = delegate { };
     
        /// ======================================================================

        private void OnEnable()
        {
            levelCars.Added += OnLevelCarAdded;
            levelCars.Removed += OnLevelCarRemoved;

            poolCars.Added += OnPoolCarAdded;
            poolCars.Removed += OnPoolCarRemoved;
        }
        private void OnDisable()
        {
            levelCars.Added -= OnLevelCarAdded;
            levelCars.Removed -= OnLevelCarRemoved;

            poolCars.Added -= OnPoolCarAdded;
            poolCars.Removed -= OnPoolCarRemoved;
        }

        /// ======================================================================

        public List<ACar> GetLevelCars()
        {
            return levelCars.GetAll();
        }
        public void AddLevelCar(ACar car)
        {
            levelCars.Add(car);
        }
        public void RemoveLevelCar(ACar car)
        {
            levelCars.Remove(car);
        }

        public List<ACar> GetPoolCars()
        {
            return poolCars.GetAll();
        }
        public void AddPoolCar(ACar car)
        {
            poolCars.Add(car);
        }
        public void RemovePoolCar(ACar car)
        {
            poolCars.Remove(car);
        }

        /// ======================================================================

        private void OnLevelCarAdded(ACar car)
        {
            LevelCarAdded(car);
        }
        private void OnLevelCarRemoved(ACar car)
        {
            LevelCarRemoved(car);
        }

        private void OnPoolCarAdded(ACar car)
        {
            PoolCarAdded(car);
        }
        private void OnPoolCarRemoved(ACar car)
        {
            PoolCarRemoved(car);
        }

        /// ======================================================================
    }
}