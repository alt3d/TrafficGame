﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Cars
{
	public class ACarsTerminator : MonoBehaviour
	{
        /// ======================================================================

        private ACarsRepository carsRepo;
        private ACarsPool carsPool;
        private ITown town;

        private ILevelActionsRepository levelActions;
        private DLevelExitAction levelExitAction;

        /// ======================================================================

        private void Awake()
        {
            carsRepo = AServices.Get<ACarsRepository>();
            carsPool = AServices.Get<ACarsPool>();
            town = AServices.Get<ITown>();

            levelActions = AServices.Get<ILevelActionsRepository>();
            levelExitAction = new DLevelExitAction(DoExitLevel);
        }
        private void OnEnable()
        {
            carsRepo.LevelCarAdded += OnLevelCarAdded;
            carsRepo.LevelCarRemoved += OnLevelCarRemoved;

            levelActions.Add(levelExitAction);
        }
        private void OnDisable()
        {
            carsRepo.LevelCarAdded -= OnLevelCarAdded;
            carsRepo.LevelCarRemoved -= OnLevelCarRemoved;

            levelActions.Remove(levelExitAction);
        }

        /// ======================================================================

        private void OnLevelCarAdded(ICar car)
        {
            car.StateChanged += OnCarStateChanged;
        }
        private void OnLevelCarRemoved(ICar car)
        {
            car.StateChanged -= OnCarStateChanged;
        }
        private void OnCarStateChanged(ICar car, ECarState state)
        {
            if (state == ECarState.Finish)
                if (town.state == ETownState.Play)
                    DeleteCar(car as ACar);
        }

        private void DeleteCar(ACar car)
        {
            car.data.state = ECarState.Idle;
            carsPool.Push(car);
        }

        private IEnumerator DoExitLevel()
        {
            DeleteLevelCars();
            yield return null;
        }
        private void DeleteLevelCars()
        {
            foreach (var car in carsRepo.GetLevelCars())
            {
                DeleteCar(car);
            }
        }
        
        /// ======================================================================
    }
}