﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Cars;

namespace Alt3d.TrafficGame
{
	public static class ZCars 
	{
        /// ======================================================================

        public const float carSpeed = 35f;
        public static readonly Vector3 carOffset = Vector3.up * 0.4f;

        /// ======================================================================

        public static bool IsPartOfCar(Collider collider)
        {
            var layer = collider.gameObject.layer;

            if (layer == ALayers.carAreas) return true;
            return false;
        }
        public static ACar GetCar(Collider collider)
        {
            var layer = collider.gameObject.layer;

            if (layer == ALayers.carAreas)
            {
                var parent = collider.transform.parent;
                return parent.GetComponent<ACar>();
            }

            throw new AException(collider);
        }

        public static string GetHumanName(ECarID id)
        {
            var result = id.ToString();
            var last = result.Length - 1;
            result = result.Remove(last, 1);
            return result;
        }

        public static void Init(IContainer container)
        {
            var cars = container.AddComponent<ACarsRepository>();
            container.Register<ACarsRepository>(cars);
            container.Register<ICarsRepository>(cars);

            var descs = container.AddComponent<ACarDescsRepository>();
            container.Register<ACarDescsRepository>(descs);
            container.Register<ICarDescsRepository>(descs);

            var events = container.AddComponent<ACarsEvents>();
            container.Register<ICarsEvents>(events);

            var generator = container.AddComponent<ACarsGenerator>();
            container.Register<ACarsGenerator>(generator);

            var terminator = container.AddComponent<ACarsTerminator>();
            container.Register<ACarsTerminator>(terminator);

            var carsContainer = container.AddComponent<ACarsContainer>();
            container.Register<ACarsContainer>(carsContainer);

            var pool = container.AddComponent<ACarsPool>();
            container.Register<ACarsPool>(pool);

            var settings = container.LoadAsset<ACarsGenerationSettingsAsset>("Game/CarsGenerationSettings");
            container.Register<ICarsGenerationSettings>(settings);
        }

        /// ======================================================================
    }
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum ECarID
    {
        HatchbackA,
        AmbulanceA,
        TaxiA,
        TruckA,
        TruckB,
        TruckC,
        RubbishTruckA,
        PoliceA,
        VanA,
        FireTruckA,
        TracktorA,
        TracktorB,
        MixerTruckA
    }

    public enum ECarState
    {
        Idle,
        Start,
        Finish,
        Drive,
        StopBeforeLight,
        StopBeforeCar,
        Crash
    }

    public enum EOtherCarsDistance
    {
        Far,
        Near
    }

    public enum ETrafficLightDistance
    {
        Far,
        Near
    }

    public enum ETrafficLightPosition
    {
        Before,
        Near,
        After
    }

    /// ======================================================================
}