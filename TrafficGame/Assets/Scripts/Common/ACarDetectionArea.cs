﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public class ACarDetectionArea : MonoBehaviour
	{
        /// ======================================================================

        public BoxCollider volume;
        private static readonly int mask = 1 << ALayers.carAreas;

        /// ======================================================================

        public event Action<ICar> CarEntered = delegate { };
        public event Action<ICar> CarExited = delegate { };

        /// ======================================================================

        private void OnTriggerEnter(Collider other)
        {
            if (ULayers.IsMaskContains(mask, other))
                if (ZCars.IsPartOfCar(other))
                    CarEntered(ZCars.GetCar(other));
        }
        private void OnTriggerExit(Collider other)
        {
            if (ULayers.IsMaskContains(mask, other))
                if (ZCars.IsPartOfCar(other))
                    CarExited(ZCars.GetCar(other));
        }

        /// ======================================================================
    }
}