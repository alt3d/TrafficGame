﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public static class ALayers 
	{
        /// ======================================================================

        public const int carsRouteStartAreas = 11;
        public const int carsRouteFinishAreas = 12;
        public const int carsCounterAreas = 13;

        public const int cars = 15;
        public const int carAreas = 16;
        public const int carDetectionAreas = 17;

        public const int trafficLightAreas = 20;
        public const int background = 21;
        public const int roads = 22;
        public const int buildings = 23;
        public const int staticProps = 24;
        public const int dynamicProps = 25;

        /// ======================================================================

        public static Dictionary<int, string> GetAll()
        {
            var result = new Dictionary<int, string>();

            result.Add(carsRouteStartAreas, "carsRouteStartAreas");
            result.Add(carsRouteFinishAreas, "carsRouteFinishAreas");
            result.Add(carsCounterAreas, "carsCounterAreas");

            result.Add(cars, "cars");
            result.Add(carAreas, "carAreas");
            result.Add(carDetectionAreas, "carDetectionAreas");

            result.Add(trafficLightAreas, "trafficLightAreas");
            result.Add(background, "background");
            result.Add(roads, "roads");
            result.Add(buildings, "buildings");
            result.Add(staticProps, "staticProps");
            result.Add(dynamicProps, "dynamicProps");

            return result;
        }

        /// ======================================================================
    }
}