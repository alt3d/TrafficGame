﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
    public static class AProduct
    {
        /// ======================================================================

        private const string _company = "Alt3d Studio";
        private const string _product = "Traffic Control";
        private const string _version = "1.0.0";
        private const string _appID = "com.alt3d.TrafficGame";

        private const string _iosAdsID = "1547182";
        private const string _androidAdsID = "1546828";

        private const bool _isDebug = true;

        /// ======================================================================

        public static string company
        {
            get { return _company; }
        }
        public static string product
        {
            get { return _product; }
        }
        public static string version
        {
            get { return _version; }
        }
        public static string appID
        {
            get { return _appID; }
        }
        public static string adsID
        {
            get
            {
#if UNITY_IOS
                return _iosAdsID;
#elif UNITY_ANDROID
                return _androidAdsID;
#else
                return _androidAdsID;
#endif
            }
        }
        public static bool isDebug
        {
            get { return false; }
        }

        /// ======================================================================
    }
}