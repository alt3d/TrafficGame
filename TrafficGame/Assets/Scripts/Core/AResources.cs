﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public static class AResources 
	{
        /// ======================================================================

        public const string townDescsFolder = "Towns";
        public const string carDescFolder = "Cars";
        public const string propsDescFolder = "Props";

        private const string gameFolder = "Game";
        public const string gameSettingsPath = gameFolder + "/GameSettings";
        public const string soundsSetPath = gameFolder + "/SoundsSet";

		/// ======================================================================
	}
}