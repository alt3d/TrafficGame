﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public static class AScene 
	{
        /// ======================================================================

        private const string coreName = "CORE";
        private const string debugName = "DEBUG";
        private const string uiName = "UI";
        private const string townsName = "TOWNS";
        
        private const string trafficLightsName = "TrafficLights";
        private const string carRoutesName = "CarRoutes";
        private const string carCountersName = "CarCounters";

        private const string screenCanvasName = uiName + "/ScreenCanvas";
        private const string debugCanvasName = uiName + "/DebugCanvas";

        public const string carsRootGoName = "Cars";
        public const string levelCarsGoName = "Level";
        public const string poolCarsGoName = "Pool";

        /// ======================================================================

        public static GameObject GetCore()
        {
            return UGameObject.FindGo(coreName);
        }
        public static GameObject GetDebug()
        {
            return UGameObject.FindGo(debugName);
        }

        /// ======================================================================

        public static Transform GetTownsParent()
        {
            return UGameObject.FindTrn(townsName);
        }
        public static Transform GetTownParent(ETownID id)
        {
            var parents = GetAllTownParents();
            foreach (var parent in parents)
            {
                if (parent.gameObject.name == id.ToString()) return parent;
            }
            throw new Exception("Town parent not found: " + id);
        }
        public static List<Transform> GetAllTownParents()
        {
            var result = new List<Transform>();

            var go = UGameObject.FindTrn(townsName);
            var array = UGameObject.GetChilds(go);
            foreach (var item in array)
            {
                result.Add(item);
            }

            return result;
        }
        private static List<Transform> GetAllTownsChilds(string name)
        {
            var result = new List<Transform>();

            var parents = GetAllTownParents();
            foreach (var parent in parents)
            {
                var go = parent.Find(name);
                if (go != null) result.Add(go);
            }

            return result;
        }

        public static List<Transform> GetTrafficLightsParents()
        {
            var name = trafficLightsName;
            return GetAllTownsChilds(name);
        }
        public static List<Transform> GetCarRoutesParents()
        {
            var name = carRoutesName;
            return GetAllTownsChilds(name);
        }
        public static List<Transform> GetCarCounterParents()
        {
            var name = carCountersName;
            return GetAllTownsChilds(name);
        }

        /// ======================================================================

        private static Transform GetScreenCanvas()
        {
            return UGameObject.FindTrn(screenCanvasName);
        }
        private static Transform GetDebugCanvas()
        {
            return UGameObject.FindTrn(debugCanvasName);
        }

        public static Transform[] GetScreenMenus()
        {
            var parent = GetScreenCanvas();
            return UGameObject.GetChilds(parent);
        }
        public static Transform[] GetDebugMenus()
        {
            var parent = GetDebugCanvas();
            return UGameObject.GetChilds(parent);
        }

        /// ======================================================================
    }
}