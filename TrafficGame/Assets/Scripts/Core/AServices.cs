﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Services;

namespace Alt3d.TrafficGame
{
	public static class AServices 
    {
        /// ======================================================================

        private static readonly AContainer container;

        /// ======================================================================

        public static T Get<T>()
        {
            var t = typeof(T);

            if (container.dic.ContainsKey(t) == false)
                throw new Exception("Type not found in services: " + t.ToString());

            return (T)container.dic[t];
        }
        static AServices()
        {
            container = new AContainer();
           
            ZMain.Init(container);
            ZActions.Init(container);

            ZStreams.Init(container);
            ZApps.Init(container);
            ZGames.Init(container);
            ZLevels.Init(container);

            ZInputs.Init(container);
            ZUIs.Init(container);
            ZSounds.Init(container);
            ZPlayers.Init(container);

            ZAdverts.Init(container);
            ZAchivs.Init(container);

            ZCars.Init(container);
            ZCarRoutes.Init(container);
            ZCarCounters.Init(container);

            ZTowns.Init(container);
            ZTrafficLights.Init(container);
            ZProps.Init(container);

            container.go.SetActive(true);
        }

        /// ======================================================================
    }
}