﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarCounters;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarCountersChecker : MonoBehaviour
	{
        /// ======================================================================

        private ICarCountersRepository repo;

        /// ======================================================================

        private void Awake()
        {
            repo = AServices.Get<ICarCountersRepository>();
        }
        private void OnEnable()
        {
            repo.GameCounterAdded += OnGameCounterAdded;
        }
        private void OnDisable()
        {
            repo.GameCounterAdded -= OnGameCounterAdded;
        }

        private void OnGameCounterAdded(ICarCounter counter)
        {
            CheckComponents(counter as ACarCounter);
            CheckArea(counter as ACarCounter);
        }
        private void CheckComponents(ACarCounter counter)
        {
            Debug.Assert(counter.data != null, counter);
            Debug.Assert(counter.area != null, counter);
        }
        private void CheckArea(ACarCounter counter)
        {
            var area = counter.area;
            Debug.Assert(area.volume != null);
            Debug.Assert(area.volume.isTrigger, area);
            Debug.Assert(area.gameObject.layer == ALayers.carsCounterAreas, area);
        }

        /// ======================================================================
    }
}