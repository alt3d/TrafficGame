﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarCounters;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarCountersGizmos : MonoBehaviour
	{
        /// ======================================================================

        public Color color = Color.blue;

        /// ======================================================================

        private void OnDrawGizmos()
        {
            foreach (var counter in FindObjectsOfType<ACarCounter>())
            {
                UDraw.Box(counter.area.volume, color);
            }
        }

        /// ======================================================================
    }
}