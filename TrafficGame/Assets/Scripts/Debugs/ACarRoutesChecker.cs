﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarRoutes;
using Alt3d.TrafficGame.CarRoutes.Routes;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarRoutesChecker : MonoBehaviour
	{
        /// ======================================================================

        private ICarRoutesRepository repo;

        /// ======================================================================

        private void Awake()
        {
            repo = AServices.Get<ICarRoutesRepository>();
        }
        private void OnEnable()
        {
            repo.GameRouteAdded += OnGameRouteAdded;
        }
        private void OnDisable()
        {
            repo.GameRouteRemoved -= OnGameRouteAdded;
        }

        private void OnGameRouteAdded(ICarRoute route)
        {
            CheckComponents(route as ACarRoute);
            CheckStartArea(route as ACarRoute);
            CheckFinishArea(route as ACarRoute);
        }
        private void CheckComponents(ACarRoute route)
        {
            Debug.Assert(route.data != null, route);
            Debug.Assert(route.startArea != null, route);
            Debug.Assert(route.finishArea != null, route);
            Debug.Assert(route.GetComponent<ACarRouteController>() != null, route);
        }
        private void CheckStartArea(ACarRoute route)
        {
            var area = route.startArea;
            Debug.Assert(area.volume != null, area);
            Debug.Assert(area.volume.isTrigger, area);
            Debug.Assert(area.gameObject.layer == ALayers.carsRouteStartAreas, area);
        }
        private void CheckFinishArea(ACarRoute route)
        {
            var area = route.finishArea;
            Debug.Assert(area.volume != null, area);
            Debug.Assert(area.volume.isTrigger, area);
            Debug.Assert(area.gameObject.layer == ALayers.carsRouteFinishAreas, area);
        }

        /// ======================================================================
    }
}