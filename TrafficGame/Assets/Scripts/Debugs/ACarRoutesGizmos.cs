﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.CarRoutes;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarRoutesGizmos : MonoBehaviour
	{
        /// ======================================================================

        public Color lineColor = Color.yellow;
        public Color startColor = Color.green;
        public Color finishColor = Color.blue;
        public Color busyColor = Color.red;

        /// ======================================================================

        private void OnDrawGizmos()
        {
            foreach (var route in FindObjectsOfType<ACarRoute>())
            {
                UDraw.Line(route.startArea.transform.position, route.finishArea.transform.position, lineColor);

                var color = GetStartColor(route);
                UDraw.Box(route.startArea.volume, color);
                UDraw.Box(route.finishArea.volume, finishColor);
            }
        }
        public Color GetStartColor(ACarRoute route)
        {
            if (route.data.isStartBusy)
                return busyColor;
            
            return startColor;
        }

        /// ======================================================================
    }
}