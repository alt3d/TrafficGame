﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Cars;
using Alt3d.TrafficGame.Cars.Cars;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarsChecker : MonoBehaviour
	{
        /// ======================================================================

        private ICarsRepository repo;

        /// ======================================================================

        private void Awake()
        {
            repo = AServices.Get<ICarsRepository>();
        }
        private void OnEnable()
        {
            repo.LevelCarAdded += OnLevelCarAdded;
        }
        private void OnDisable()
        {
            repo.LevelCarAdded -= OnLevelCarAdded;
        }

        private void OnLevelCarAdded(ICar car)
        {
            CheckComponents(car as ACar);
            CheckArea(car as ACar);
        }
        private void CheckComponents(ACar car)
        {
            Debug.Assert(car.data != null, car);
            Debug.Assert(car.area != null, car);
            Debug.Assert(car.sensor != null, car);

            Debug.Assert(car.GetComponent<ACarMotor>() != null, car);
            Debug.Assert(car.GetComponent<ACarPhysic>() != null, car);
            Debug.Assert(car.GetComponent<ACarPhysic>() != null, car);
            Debug.Assert(car.GetComponent<ACarAI>() != null, car);
        }
        private void CheckArea(ACar car)
        {
            var area = car.area;
            Debug.Assert(area.volume != null, area);
            Debug.Assert(area.volume.isTrigger, area);
            Debug.Assert(area.gameObject.layer == ALayers.carAreas, area);
        }

        /// ======================================================================
    }
}