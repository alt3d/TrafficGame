﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Cars;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarsGizmos : MonoBehaviour
	{
        /// ======================================================================

        public Color IdleColor = Color.yellow;
        public Color StartColor = Color.magenta;
        public Color FinishColor = Color.magenta;
        public Color DriveColor = Color.green;
        public Color StopBeforeLightColor = Color.magenta;
        public Color StopBeforeCarColor = Color.magenta;
        public Color CrashColor = Color.red;

        /// ======================================================================

        private void OnDrawGizmos()
        {
            foreach (var car in FindObjectsOfType<ACar>())
            {
                UDraw.Box(car.area.volume, GetColor(car));
            }
        }
        private Color GetColor(ACar car)
        {
            switch (car.data.state)
            {
                case ECarState.Crash: return CrashColor;
                case ECarState.Drive: return DriveColor;
                case ECarState.Finish: return FinishColor;
                case ECarState.Idle: return IdleColor;
                case ECarState.Start: return StartColor;
                case ECarState.StopBeforeCar: return StopBeforeCarColor;
                case ECarState.StopBeforeLight: return StopBeforeLightColor;
                default: throw new Exception(car.data.state.ToString());
            }
        }

        /// ======================================================================
    }
}