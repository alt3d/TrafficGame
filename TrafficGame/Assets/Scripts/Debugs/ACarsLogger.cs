﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACarsLogger : MonoBehaviour
	{
        /// ======================================================================

        public bool isAddedOnLevel;
        public bool isRemovedFromLevel;

        [Space]
        public bool isAddedToPool;
        public bool isRemovedFromPool;

        private ICarsRepository collection;

        /// ======================================================================

        private void Awake()
        {
            collection = AServices.Get<ICarsRepository>();
        }
        private void OnEnable()
        {
            collection.LevelCarAdded += OnLevelCarAdded;
            collection.LevelCarRemoved += OnLevelCarRemoved;

            collection.PoolCarAdded += OnPoolCarAdded;
            collection.PoolCarRemoved += OnPoolCarRemoved;
        }
        private void OnDisable()
        {
            collection.LevelCarAdded -= OnLevelCarAdded;
            collection.LevelCarRemoved -= OnLevelCarRemoved;

            collection.PoolCarAdded -= OnPoolCarAdded;
            collection.PoolCarRemoved -= OnPoolCarRemoved;
        }

        private void OnLevelCarAdded(ICar car)
        {
            if (isAddedOnLevel)
                Debug.Log("Car added on level: " + car.id);
        }
        private void OnLevelCarRemoved(ICar car)
        {
            if (isRemovedFromLevel)
                Debug.Log("Car removed from level: " + car.id);
        }

        private void OnPoolCarAdded(ICar car)
        {
            if (isAddedToPool)
                Debug.Log("Car added to poll: " + car.id);
        }
        private void OnPoolCarRemoved(ICar car)
        {
            if (isRemovedFromPool)
                Debug.Log("Car removed from poll: " + car.id);
        }

        /// ======================================================================
    }
}