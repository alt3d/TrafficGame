﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class ACoreLogger : MonoBehaviour
	{
        /// ======================================================================

        public bool isAppState;
        public bool isGameState;
        public bool isLevelState;
        public bool isPause;
        public bool isScreen;

        private IApp app;
        private IGame game;
        private ILevel level;
        private IUI ui;

        /// ======================================================================

        private void Awake()
        {
            app = AServices.Get<IApp>();
            game = AServices.Get<IGame>();
            level = AServices.Get<ILevel>();
            ui = AServices.Get<IUI>();
        }
        private void OnEnable()
        {
            app.StateChanged += OnAppStateChanged;
            game.StateChanged += OnGameStateChanged;
            level.StateChanged += OnLevelStateChanged;
            level.PauseChanged += OnPauseChanged;
            ui.ScreenChanged += OnScreenChanged;
        }
        private void OnDisable()
        {
            app.StateChanged -= OnAppStateChanged;
            game.StateChanged -= OnGameStateChanged;
            level.StateChanged -= OnLevelStateChanged;
            level.PauseChanged -= OnPauseChanged;
            ui.ScreenChanged -= OnScreenChanged;
        }

        /// ======================================================================

        private void OnAppStateChanged(EAppState state)
        {
            if (isAppState)
                Debug.Log("App state: " + state);
        }
        private void OnGameStateChanged(EGameState state)
        {
            if (isGameState)
                Debug.Log("Game state: " + state);
        }
        private void OnLevelStateChanged(ELevelState state)
        {
            if (isLevelState)
                Debug.Log("Level state: " + state);
        }

        private void OnPauseChanged(EPauseState state)
        {
            if (isPause)
                Debug.Log("Pause: " + state);
        }
        private void OnScreenChanged(EScreen screen)
        {
            if (isScreen)
                Debug.Log("Screen: " + screen);
        }

        /// ======================================================================
    }
}