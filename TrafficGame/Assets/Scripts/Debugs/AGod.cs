﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class AGod : MonoBehaviour
	{
        /// ======================================================================

        private IMenusRepository menus;

        /// ======================================================================

        private void Awake()
        {
            AServices.Get<IPropsEvents>().PropsBroken += AGod_PropsBroken;
            AServices.Get<ICarsEvents>().CarPassed += OnCarPassed;
            AServices.Get<ITown>().SignalChanged += OnSignalChanged;

            if (Application.isEditor == false)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
                return;
            }

            menus = AServices.Get<IMenusRepository>();
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
                menus.Get(EMenuID.DebugCoreInfo).Toggle();

            if (Input.GetKeyDown(KeyCode.F2))
                menus.Get(EMenuID.DebugStatsInfo).Toggle();



            if (Input.GetKeyDown(KeyCode.KeypadPeriod))
                SetMasterVolume(0f);

            if (Input.GetKeyDown(KeyCode.Keypad3))
                SetMasterVolume(0.3f);

            if (Input.GetKeyDown(KeyCode.Keypad6))
                SetMasterVolume(0.6f);

            if (Input.GetKeyDown(KeyCode.Keypad9))
                SetMasterVolume(0.9f);

            if (Input.GetKeyDown(KeyCode.KeypadMultiply))
                SetMasterVolume(1.0f);
        }

        private void SetMasterVolume(float value)
        {
            AServices.Get<IGameSettings>().ChangeMasterVolume(value);
        }

        private void AGod_PropsBroken(EPropsID obj)
        {
            //Debug.Log(obj);
        }
        private void OnCarPassed(ICar car)
        {
            
        }
        private void OnSignalChanged(ETrafficLightSignal signal)
        {
            
        }

        /// ======================================================================
    }
}