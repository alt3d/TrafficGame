﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class AMeterorsGenerator : MonoBehaviour
	{
        /// ======================================================================

        public GameObject prefab;
        public float force;

        private readonly List<GameObject> meteors = new List<GameObject>();
        private ILevel level;

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
        }
        private void OnDisable()
        {
            level.StateChanged -= OnLevelStateChanged;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse2)) GenerateMeteor();
        }

        /// ======================================================================

        private void OnLevelStateChanged(ELevelState state)
        {
            if (state == ELevelState.Stop)
            {
                var array = meteors.ToArray();
                for (int i = 0; i < array.Length; i++)
                {
                    Destroy(array[i]);
                }
            }
            meteors.Clear();
        }
        private void GenerateMeteor()
        {
            var go = UGameObject.Instantiate(prefab);
            go.transform.parent = transform;
            meteors.Add(go);

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            go.transform.position = ray.origin;

            var body = go.GetComponent<Rigidbody>();
            body.AddForce(ray.direction * force);
        }

        /// ======================================================================
    }
}