﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class AScreenshotMaker : MonoBehaviour
	{
        /// ======================================================================

        [SerializeField] protected int superSize = 1;

        /// ======================================================================

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var data = Guid.NewGuid().ToString();
                var path = Application.persistentDataPath + "/Screens/" + data + ".png";
                Application.CaptureScreenshot(path, superSize);
            }
        }

        /// ======================================================================
    }
}