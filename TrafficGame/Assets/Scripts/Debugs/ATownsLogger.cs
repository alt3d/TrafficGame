﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Debugs
{
	public class ATownsLogger : MonoBehaviour
	{
        /// ======================================================================

        public bool isTown;
        public bool isState;
        public bool isSignal;

        private ITown town;

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();
        }
        private void OnEnable()
        {
            town.TownChanged += OnTownChanged;
            town.StateChanged += OnStateChanged;
            town.SignalChanged += OnSignalChanged;
        }
        private void OnDisable()
        {
            town.TownChanged -= OnTownChanged;
            town.StateChanged -= OnStateChanged;
            town.SignalChanged -= OnSignalChanged;
        }

        /// ======================================================================

        private void OnTownChanged(ETownID town)
        {
            if (isTown)
                Debug.Log("Town town: " + town);
        }
        private void OnStateChanged(ETownState state)
        {
            if (isState)
                Debug.Log("Town state: " + state);
        }
        private void OnSignalChanged(ETrafficLightSignal signal)
        {
            if (isSignal)
                Debug.Log("Town signal: " + signal);
        }

        /// ======================================================================
    }
}