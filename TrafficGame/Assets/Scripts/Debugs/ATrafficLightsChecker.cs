﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.TrafficLights;

namespace Alt3d.TrafficGame.Debugs
{
	public class ATrafficLightsChecker : MonoBehaviour
	{
        /// ======================================================================

        private ITrafficLightsRepository repo;

        /// ======================================================================

        private void Awake()
        {
            repo = AServices.Get<ITrafficLightsRepository>();
        }
        private void OnEnable()
        {
            repo.GameLightAdded += OnGameLightAdded;
        }
        private void OnDisable()
        {
            repo.GameLightAdded -= OnGameLightAdded;
        }

        private void OnGameLightAdded(ITrafficLight light)
        {
            CheckComponents(light as ATrafficLight);
            CheckArea(light as ATrafficLight);
        }
        private void CheckComponents(ATrafficLight light)
        {
            Debug.Assert(light.data != null, light);
            Debug.Assert(light.area != null, light);
        }
        private void CheckArea(ATrafficLight light)
        {
            var area = light.area;
            Debug.Assert(area.volume != null, area);
            Debug.Assert(area.volume.isTrigger, area);
            Debug.Assert(area.gameObject.layer == ALayers.trafficLightAreas, area);
        }

        /// ======================================================================
    }
}