﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.TrafficLights;

namespace Alt3d.TrafficGame.Debugs
{
	public class ATrafficLightsGizmos : MonoBehaviour
	{
        /// ======================================================================

        public Color redColor = Color.red;
        public Color greenColor = Color.green;
        public Color yellowColor = Color.yellow;

        /// ======================================================================

        private void OnDrawGizmos()
        {
            foreach (var light in FindObjectsOfType<ATrafficLight>())
            {
                UDraw.Box(light.area.volume, GetColor(light.data.signal));
            }   
        }
        private Color GetColor(ETrafficLightSignal signal)
        {
            switch (signal)
            {
                case ETrafficLightSignal.Green: return greenColor;
                case ETrafficLightSignal.Red: return redColor;
                case ETrafficLightSignal.Yellow: return yellowColor;
                default: throw new Exception(signal.ToString());
            }
        }

        /// ======================================================================
    }
}