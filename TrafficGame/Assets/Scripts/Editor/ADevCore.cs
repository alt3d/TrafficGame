﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace Alt3d.TrafficGame.Dev
{
    [InitializeOnLoad]
	public static class ADevCore 
	{
        /// ======================================================================

        static ADevCore()
        {
            SetupPlayerSettings();
            SetupLayers();
        }

        /// ======================================================================

        private static void SetupPlayerSettings()
        {
            PlayerSettings.companyName = AProduct.company;
            PlayerSettings.productName = AProduct.product;
            PlayerSettings.bundleVersion = AProduct.version;

            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Standalone, AProduct.appID);
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, AProduct.appID);
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, AProduct.appID);

            PlayerSettings.allowedAutorotateToPortrait = false;
            PlayerSettings.allowedAutorotateToPortraitUpsideDown = false;
            PlayerSettings.allowedAutorotateToLandscapeLeft = true;
            PlayerSettings.allowedAutorotateToLandscapeRight = true;
        }
        private static void SetupLayers()
        {
            var dic = ALayers.GetAll();
            var manager = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0]);
            var array = manager.FindProperty("layers");

            for (int i = 8; i < 32; i++)
            {
                var layer = array.GetArrayElementAtIndex(i);
                if (dic.ContainsKey(i)) layer.stringValue = dic[i];
                else layer.stringValue = string.Empty;
            }

            manager.ApplyModifiedPropertiesWithoutUndo();
        }

        /// ======================================================================
    }
}