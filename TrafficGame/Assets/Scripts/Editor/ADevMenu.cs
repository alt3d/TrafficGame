﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Alt3d.TrafficGame.Dev
{
	public static class ADevMenu 
	{
        /// ======================================================================

        [MenuItem("Dev/Menus Editor", false, 101)]
        private static void OpenMenusEditorWindow()
        {
            EditorWindow.GetWindow<AMenusEditorWindow>("Menus Editor");
        }

        [MenuItem("Dev/Towns Editor", false, 102)]
        private static void OpenLevelsEditorWindow()
        {
            EditorWindow.GetWindow<ATownsEditorWindow>("Towns Editor");
        }

        [MenuItem("Tools/Objects Replacer", false, 101)]
        private static void OpenAObjectsReplacerWindow()
        {
            EditorWindow.GetWindow<AObjectsReplacer>("Objects Replacer");
        }

        /// ======================================================================
    }
}