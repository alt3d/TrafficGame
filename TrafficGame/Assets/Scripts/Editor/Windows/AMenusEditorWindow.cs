﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Alt3d.TrafficGame.UIs.Menus;

namespace Alt3d.TrafficGame.Dev
{
	public class AMenusEditorWindow : EditorWindow
    {
        /// ======================================================================

        private void OnGUI()
        {
            var allMenus = FindObjectsOfType<AMenu>();
           
            var parents = AScene.GetScreenMenus();
            var gameMenus = new AMenu[parents.Length];
            for (int i = 0; i < parents.Length; i++)
            {
                gameMenus[i] = parents[i].GetComponent<AMenu>();
            }

            parents = AScene.GetDebugMenus();
            var debugMenus = new AMenu[parents.Length];
            for (int i = 0; i < parents.Length; i++)
            {
                debugMenus[i] = parents[i].GetComponent<AMenu>();
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("All Menus", EditorStyles.centeredGreyMiniLabel);
            DrawCloseAllButton(allMenus);

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Game Menus", EditorStyles.centeredGreyMiniLabel);
            for (int i = 0; i < gameMenus.Length; i++)
            {
                DrawMenuButtons(gameMenus[i], allMenus);
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Debug Menus", EditorStyles.centeredGreyMiniLabel);
            for (int i = 0; i < debugMenus.Length; i++)
            {
                DrawMenuButtons(debugMenus[i], allMenus);
            }
        }

        private void DrawCloseAllButton(AMenu[] array)
        {
            if (GUILayout.Button("Close All"))
            {
                CloseAllMenus(array);
            }
        }
        private void DrawMenuButtons(AMenu menu, AMenu[] array)
        {
            EditorGUILayout.BeginHorizontal();

            GUI.color = (menu.content.activeSelf) ? Color.yellow : Color.white;

            if (GUILayout.Button("+/-", GUILayout.Width(32)))
            {
                ToggleMenu(menu);
            }

            if (GUILayout.Button(menu.name))
            {
                CloseAllMenus(array);
                FocusOnMenu(menu);
                OpenMenu(menu);
            }

            if (GUILayout.Button("F", GUILayout.Width(32)))
            {
                FocusOnMenu(menu);
            }

            GUI.color = Color.white;
            EditorGUILayout.EndHorizontal();
        }

        /// ======================================================================

        private void CloseAllMenus(AMenu[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                CloseMenu(array[i]);
            }
        }
        private void FocusOnMenu(AMenu menu)
        {
            Selection.activeGameObject = menu.gameObject;
        }

        private void CloseMenu(AMenu menu)
        {
            menu.content.SetActive(false);
        }
        private void OpenMenu(AMenu menu)
        {
            menu.content.SetActive(true);
        }
        private void ToggleMenu(AMenu menu)
        {
            var activity = menu.content.activeSelf;
            menu.content.SetActive(!activity);
        }

        /// ======================================================================
    }
}