﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEditor;

namespace Alt3d.TrafficGame.Dev
{
	public class AObjectsReplacer : EditorWindow
	{
        /// ======================================================================

        private GameObject target;

        /// ======================================================================

        private void OnGUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            target = (GameObject)EditorGUILayout.ObjectField("Target: ", target, typeof(GameObject), false);

            EditorGUILayout.Space();
            if (GUILayout.Button("Repace"))
            {
                if (target != null)
                {
                    var array = Selection.gameObjects;
                    for (int i = 0; i < array.Length; i++)
                    {
                        var src = array[i];

                        var go = (GameObject)PrefabUtility.InstantiatePrefab(target);
                        go.transform.parent = src.transform.parent;
                        go.transform.position = src.transform.position;
                        go.transform.rotation = src.transform.rotation;
                        go.transform.localScale = src.transform.localScale;
                        //go.name = src.name;

                        DestroyImmediate(src);
                    }
                }
            }
        }

        /// ======================================================================
    }
}