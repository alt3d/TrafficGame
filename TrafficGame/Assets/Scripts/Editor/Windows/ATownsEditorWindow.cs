﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Alt3d.TrafficGame.Dev
{
	public class ATownsEditorWindow : EditorWindow
	{
        /// ======================================================================

        private void OnGUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("All Levels", EditorStyles.centeredGreyMiniLabel);
            if (GUILayout.Button("Hide All")) HideAllTowns();
            if (GUILayout.Button("Show All")) ShowAllTowns();

            var towns = GetAllTowns();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Toggle Level", EditorStyles.centeredGreyMiniLabel);
            for (int i = 0; i < towns.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("" + towns[i])) ToggleTown(towns[i]);
                if (GUILayout.Button("F", GUILayout.Width(32))) FocusOnTown(towns[i]);
                EditorGUILayout.EndHorizontal();
            }
        }

        /// ======================================================================

        private List<ETownID> GetAllTowns()
        {
            return ZTowns.GetAppTowns();
        }
        private Transform GetTown(ETownID town)
        {
            var parent = UGameObject.FindTrn("TOWNS");
            return parent.Find(town.ToString());
        }

        private void FocusOnTown(ETownID town)
        {
            var trn = GetTown(town);
            Selection.activeGameObject = trn.gameObject;
        }

        private void ShowAllTowns()
        {
            var aTowns = GetAllTowns();
            foreach (var town in aTowns)
            {
                ShowTown(town);
            }
        }
        private void HideAllTowns()
        {
            var aTowns = GetAllTowns();
            foreach (var town in aTowns)
            {
                HideTown(town);
            }
        }
   
        private void ShowTown(ETownID town)
        {
            ChangeTownActivity(town, true);
        }
        private void HideTown(ETownID town)
        {
            ChangeTownActivity(town, false);
        }
        private void ToggleTown(ETownID town)
        {
            HideAllTowns();
            ShowTown(town);
        }

        private void ChangeTownActivity(ETownID town, bool activity)
        {
            var trn = GetTown(town);
            trn.gameObject.SetActive(activity);
        }

        /// ======================================================================
    }
}