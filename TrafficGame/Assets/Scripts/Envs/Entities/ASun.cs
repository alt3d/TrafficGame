﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Envs
{
	public class ASun : MonoBehaviour
	{
        /// ======================================================================

        public Light source;
        private IGameSettings settings;

        /// ======================================================================

        private void Awake()
        {
            settings = AServices.Get<IGameSettings>();
        }
        private void OnEnable()
        {
            settings.ShadowsActivityChanged += OnShadowsActivityChanged;
        }
        private void OnDisable()
        {
            settings.ShadowsActivityChanged -= OnShadowsActivityChanged;
        }

        private void OnShadowsActivityChanged(bool activity)
        {
            if (activity) source.shadows = LightShadows.Soft;
            else source.shadows = LightShadows.None;
        }

        /// ======================================================================
    }
}