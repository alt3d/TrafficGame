﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Game
{
	public class AGame : MonoBehaviour, IGame
    {
        /// ======================================================================

        private EGameState _state = EGameState.Stop;

        /// ======================================================================

        public EGameState state
        {
            get { return _state; }
            set
            {
                _state = value;
                StateChanged(_state);
            }
        }

        /// ======================================================================

        public event Action<EGameState> StateChanged = delegate { };

        /// ======================================================================
    }
}