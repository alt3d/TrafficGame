﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Game
{
	public class AGameSettings : MonoBehaviour, IGameSettings
    {
        /// ======================================================================

        private bool _shadowsActivity = false;
        private float _masterVolume = 1f;

        private bool _isEnableInterstitialAdvert = true;
        private int _levelsBetweenInterstitialAdvert = 2;

        /// ======================================================================

        public bool shadowsActivity
        {
            get { return _shadowsActivity; }
            set
            {
                _shadowsActivity = value;
                ShadowsActivityChanged(_shadowsActivity);
            }
        }
        public float masterVolume
        {
            get { return _masterVolume; }
            set
            {
                _masterVolume = value;
                MasterVolumeChanged(_masterVolume);
            }
        }

        public int levelsBetweenInterstitialAdvert
        {
            get { return _levelsBetweenInterstitialAdvert; }
            set { _levelsBetweenInterstitialAdvert = value; }
        }
        public bool isEnableInterstitialAdvert
        {
            get { return _isEnableInterstitialAdvert; }
            set { _isEnableInterstitialAdvert = value; }
        }

        /// ======================================================================

        public event Action<bool> ShadowsActivityChanged = delegate { };
        public event Action<float> MasterVolumeChanged = delegate { };

        /// ======================================================================

        public void ToggleShadowsActivity()
        {
            shadowsActivity = !shadowsActivity;
        }
        public void ChangeMasterVolume(float value)
        {
            masterVolume = Mathf.Clamp01(value);
        }

        /// ======================================================================
    }
}