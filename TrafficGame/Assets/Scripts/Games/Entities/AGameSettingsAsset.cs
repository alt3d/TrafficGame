﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Game
{
	public class AGameSettingsAsset : ScriptableObject
	{
		/// ======================================================================

        [SerializeField] protected bool _shadowsActivity = true;
        [SerializeField] protected float _masterVolume = 1f;

        [Space]
        [SerializeField] protected bool _isEnableInterstitialAdvert = true;
        [SerializeField] protected int _levelsBetweenInterstitialAdvert = 2;

        /// ======================================================================

        public bool shadowsActivity
        {
            get { return _shadowsActivity; }
        }
        public float masterVolume
        {
            get { return _masterVolume; }
        }

        public bool isEnableInterstitialAdvert
        {
            get { return _isEnableInterstitialAdvert; }
        }
        public int levelsBetweenInterstitialAdvert
        {
            get { return _levelsBetweenInterstitialAdvert; }
        }
        
        /// ======================================================================
    }
}