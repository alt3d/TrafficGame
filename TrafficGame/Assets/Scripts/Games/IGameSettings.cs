﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IGameSettings 
	{
        /// ======================================================================

        bool shadowsActivity { get;  }
        float masterVolume { get; }

        bool isEnableInterstitialAdvert { get; }
        int levelsBetweenInterstitialAdvert { get; }

        /// ======================================================================

        event Action<bool> ShadowsActivityChanged;
        event Action<float> MasterVolumeChanged;

        /// ======================================================================

        void ToggleShadowsActivity();
        void ChangeMasterVolume(float value);

        /// ======================================================================
    }
}