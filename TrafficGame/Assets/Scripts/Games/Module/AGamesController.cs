﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Game
{
	public class AGamesController : MonoBehaviour
	{
        /// ======================================================================

        private AGameSettings settings;
        private AGameSettingsAsset settingsAsset;
        private IApp app;
        private IStream stream;

        private IAppActionsRepository appActions;
        private IGameActionsRepository gameActions;
        private DAppLoadAction appLoadAction;
        private DGameLoadAction gameLoadAction;

        private const string shadowsActivityKey = "gameSettings_shadowsActivity";
        private const string masterVolumeKey = "gameSettings_masterVolume";

        /// ======================================================================

        private void Awake()
        {
            settings = AServices.Get<AGameSettings>();
            settingsAsset = AServices.Get<AGameSettingsAsset>();

            app = AServices.Get<IApp>();
            stream = AServices.Get<IStream>();

            appActions = AServices.Get<IAppActionsRepository>();
            gameActions = AServices.Get<IGameActionsRepository>();

            appLoadAction = new DAppLoadAction(DoLoadApp);
            gameLoadAction = new DGameLoadAction(DoLoadGame);
        }
        private void OnEnable()
        {
            settings.ShadowsActivityChanged += OnShadowsActivityChanged;
            settings.MasterVolumeChanged += OnMasterVolumeChanged;

            appActions.Add(appLoadAction);
            gameActions.Add(gameLoadAction);
        }
        private void OnDisable()
        {
            settings.ShadowsActivityChanged -= OnShadowsActivityChanged;
            settings.MasterVolumeChanged -= OnMasterVolumeChanged;

            appActions.Remove(appLoadAction);
            gameActions.Remove(gameLoadAction);
        }

        private void OnShadowsActivityChanged(bool activity)
        {
            if (app.state != EAppState.Game)
                return;

            var key = shadowsActivityKey;
            stream.SaveBool(key, activity);
        }
        private void OnMasterVolumeChanged(float volume)
        {
            if (app.state != EAppState.Game)
                return;

            var key = masterVolumeKey;
            stream.SaveFloat(key, volume);
        }

        private IEnumerator DoLoadApp()
        {
            LoadSettingsFromAsset();
            yield return null;
        }
        private void LoadSettingsFromAsset()
        {
            settings.shadowsActivity = settingsAsset.shadowsActivity;
            settings.masterVolume = settingsAsset.masterVolume;
            settings.isEnableInterstitialAdvert = settingsAsset.isEnableInterstitialAdvert;
            settings.levelsBetweenInterstitialAdvert = settingsAsset.levelsBetweenInterstitialAdvert;
        }
        
        private IEnumerator DoLoadGame()
        {
            LoadSettingsFromPrefs();
            LoadMasterVolume();
            yield return null;
        }
        private void LoadSettingsFromPrefs()
        {
            LoadShadowsActivity();
        }
        private void LoadShadowsActivity()
        {
            var key = shadowsActivityKey;
            if (stream.HasKey(key) == false)
                return;

            var shadowsActivity = stream.LoadBool(key);
            if (settings.shadowsActivity != shadowsActivity)
                settings.shadowsActivity = shadowsActivity;
        }
        private void LoadMasterVolume()
        {
            var key = masterVolumeKey;
            if (!stream.HasKey(key))
                return;

            var volume = stream.LoadFloat(key);
            settings.masterVolume = volume;
        }

        /// ======================================================================
    }
}