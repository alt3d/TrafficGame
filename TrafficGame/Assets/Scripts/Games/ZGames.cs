﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Game;

namespace Alt3d.TrafficGame
{
	public static class ZGames 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var game = container.AddComponent<AGame>();
            container.Register<AGame>(game);
            container.Register<IGame>(game);

            var settings = container.AddComponent<AGameSettings>();
            container.Register<AGameSettings>(settings);
            container.Register<IGameSettings>(settings);

            var path = AResources.gameSettingsPath;
            var settingsAsset = container.LoadAsset<AGameSettingsAsset>(path);
            container.Register<AGameSettingsAsset>(settingsAsset);

            container.AddComponent<AGamesController>();
        }

		/// ======================================================================
	}
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum EGameState
    {
        Stop,
        Launch,
        Menu,
        LevelLaunch,
        LevelPlay,
        LevelQuit,
        LevelResult,
        InterstitialAdvert,
        Quit
    }

    /// ======================================================================
}