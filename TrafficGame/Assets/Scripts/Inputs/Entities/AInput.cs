﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Inputs.Inputs;

namespace Alt3d.TrafficGame.Inputs
{
	public abstract class AInput : MonoBehaviour, IInput
    {
        /// ======================================================================

        protected IUI ui { get; private set; }

        /// ======================================================================

        public event Action ClickOverUI = delegate { };
        public event Action ClickOverScene = delegate { };

        /// ======================================================================

        private void Awake()
        {
            ui = AServices.Get<IUI>();
        }
        private void Update()
        {
            CheckTouch();
        }

        /// ======================================================================

        protected abstract void CheckTouch();

        /// ======================================================================

        protected void RiseClickOverUI()
        {
            ClickOverUI();
        }
        protected void RiseClickOverScene()
        {
            ClickOverScene();
        }

        /// ======================================================================

        public static AInput Create(GameObject go)
        {
            if (Application.isMobilePlatform)
                return go.AddComponent<AInputMobile>();
            else
                return go.AddComponent<AInputEditor>();
        }

        /// ======================================================================
    }
}