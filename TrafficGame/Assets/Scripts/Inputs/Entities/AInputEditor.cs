﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Inputs.Inputs
{
	public class AInputEditor : AInput
	{
        /// ======================================================================

        protected override void CheckTouch()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                var overUI = ui.IsPointerOverUI();
                if (overUI) RiseClickOverUI();
                else RiseClickOverScene();
            }
        }

        /// ======================================================================
    }
}