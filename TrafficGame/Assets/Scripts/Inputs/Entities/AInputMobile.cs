﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Inputs.Inputs
{
	public class AInputMobile : AInput
	{
        /// ======================================================================

        protected override void CheckTouch()
        {
            if (Input.touchCount == 1)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    var overUI = ui.IsPointerOverUI();
                    if (overUI) RiseClickOverUI();
                    else RiseClickOverScene();
                }
            }
        }

        /// ======================================================================
    }
}