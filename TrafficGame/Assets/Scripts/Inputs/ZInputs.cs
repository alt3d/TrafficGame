﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Inputs;


namespace Alt3d.TrafficGame
{
	public static class ZInputs 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var input = AInput.Create(container.go);
            container.Register<IInput>(input);
        }

		/// ======================================================================
	}
}