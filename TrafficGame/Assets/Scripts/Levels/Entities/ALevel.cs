﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Levels
{
	public class ALevel : MonoBehaviour, ILevel
    {
        /// ======================================================================

        private ELevelState _state = ELevelState.Stop;
        private EPauseState _pause = EPauseState.Off;
        private float _time = 0;

        /// ======================================================================

        public ELevelState state
        {
            get { return _state; }
            set
            {
                _state = value;
                StateChanged(_state);
            }
        }
        public EPauseState pause
        {
            get { return _pause; }
            set
            {
                _pause = value;
                PauseChanged(_pause);
            }
        }
        public float time
        {
            get { return _time; }
            set
            {
                _time = value;
            }
        }

        /// ======================================================================

        public event Action<ELevelState> StateChanged = delegate { };
        public event Action<EPauseState> PauseChanged = delegate { };

        /// ======================================================================

        public void SetPause()
        {
            if (state == ELevelState.Play)
                pause = EPauseState.On;
        }
        public void UnsetPause()
        {
            if (state == ELevelState.Play)
                pause = EPauseState.Off;
        }
        public void TogglePause()
        {
            if (state == ELevelState.Play)
                pause = ZLevels.GetOpposite(pause);
        }

        /// ======================================================================
    }
}