﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Levels
{
	public class AStats : MonoBehaviour, IStats
	{
        /// ======================================================================

        private int _passedCarsCount;
        private int _passedCarsReward;

        private int _crashedCarsCount;
        private int _crashedCarsReward;

        private int _brokenPropsCount;
        private int _brokenPropsReward;

        private int _unlockedAchivsCount;
        private int _unlockedAchivsReward;

        private int _signalCarsPassed;
        
        private ICarsEvents carsEvents;
        private ICarDescsRepository carDescs;
        private IAchivsEvents achivsEvents;
        private IPropsEvents propsEvents;
        private IPropsDescsRepository propsRepo;
        private ITown town;

        private ILevelActionsRepository levelActions;
        private DLevelLoadAction levelLoadAction;

        /// ======================================================================

        public int passedCarsCount
        {
            get { return _passedCarsCount; }
            set
            {
                _passedCarsCount = value;
                PassedCarsCountChanged(_passedCarsCount);
            }
        }
        public int passedCarsReward
        {
            get { return _passedCarsReward; }
            set
            {
                _passedCarsReward = value;
                PassedCarsRewardChanged(_passedCarsReward);
            }
        }

        public int crashedCarsCount
        {
            get { return _crashedCarsCount; }
            set
            {
                _crashedCarsCount = value;
                CrashedCarsCountChanged(_crashedCarsCount);
            }
        }
        public int crashedCarsReward
        {
            get { return _crashedCarsReward; }
            set
            {
                _crashedCarsReward = value;
                CrashedCarsRewardChanged(_crashedCarsReward);
            }
        }

        public int brokenPropsCount
        {
            get { return _brokenPropsCount; }
            set
            {
                _brokenPropsCount = value;
                BrokenPropsCountChanged(_brokenPropsCount);
            }
        }
        public int brokenPropsReward
        {
            get { return _brokenPropsReward; }
            set
            {
                _brokenPropsReward = value;
                BrokenPropsRewardChanged(_brokenPropsReward);
            }
        }

        public int unlockedAchivsCount
        {
            get { return _unlockedAchivsCount; }
            set
            {
                _unlockedAchivsCount = value;
                UnlockedAchivsCountChanged(_unlockedAchivsCount);
            }
        }
        public int unlockedAchivsReward
        {
            get { return _unlockedAchivsReward; }
            set
            {
                _unlockedAchivsReward = value;
                UnlockedAchivsRewardChanged(_unlockedAchivsReward);
            }
        }

        public int oneSignalCarPassed
        {
            get { return _signalCarsPassed; }
            set
            {
                _signalCarsPassed = value;
                OneSignalCarPassedChanged(_signalCarsPassed);
            }
        }

        /// ======================================================================

        public event Action<int> PassedCarsCountChanged = delegate { };
        public event Action<int> PassedCarsRewardChanged = delegate { };

        public event Action<int> CrashedCarsCountChanged = delegate { };
        public event Action<int> CrashedCarsRewardChanged = delegate { };

        public event Action<int> BrokenPropsCountChanged = delegate { };
        public event Action<int> BrokenPropsRewardChanged = delegate { };

        public event Action<int> UnlockedAchivsCountChanged = delegate { };
        public event Action<int> UnlockedAchivsRewardChanged = delegate { };

        public event Action<int> OneSignalCarPassedChanged = delegate { };

        /// ======================================================================

        private void Awake()
        {
            carsEvents = AServices.Get<ICarsEvents>();
            carDescs = AServices.Get<ICarDescsRepository>();
            achivsEvents = AServices.Get<IAchivsEvents>();
            propsEvents = AServices.Get<IPropsEvents>();
            propsRepo = AServices.Get<IPropsDescsRepository>();
            town = AServices.Get<ITown>();

            levelActions = AServices.Get<ILevelActionsRepository>();
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
        }
        private void OnEnable()
        {
            carsEvents.CarPassed += OnCarPassed;
            carsEvents.CarCrashed += OnCarCrashed;
            propsEvents.PropsBroken += OnPropsBroken;
            achivsEvents.AchivUnlocked += OnAchivUnlocked;
            town.SignalChanged += OnTownSignalChanged;

            levelActions.Add(levelLoadAction);
        }
        private void OnDisable()
        {
            carsEvents.CarPassed -= OnCarPassed;
            carsEvents.CarCrashed -= OnCarCrashed;
            propsEvents.PropsBroken -= OnPropsBroken;
            achivsEvents.AchivUnlocked -= OnAchivUnlocked;
            town.SignalChanged -= OnTownSignalChanged;

            levelActions.Remove(levelLoadAction);
        }

        private void OnCarPassed(ICar car)
        {
            passedCarsCount++;
            passedCarsReward += car.GetPassReward(carDescs);

            oneSignalCarPassed++;
        }
        private void OnCarCrashed(ICar car)
        {
            crashedCarsCount++;

            var desc = carDescs.Get(car.id);
            crashedCarsReward += desc.crashReward;
        }
        private void OnPropsBroken(EPropsID id)
        {
            brokenPropsCount++;
            brokenPropsReward += propsRepo.GetBrokeReward(id);
        }
        private void OnAchivUnlocked(IAchiv achiv)
        {
            unlockedAchivsCount++;
            unlockedAchivsReward += achiv.desc.unlockReward;
        }
        private void OnTownSignalChanged(ETrafficLightSignal signal)
        {
            if (signal == ETrafficLightSignal.Green)
            {
                oneSignalCarPassed = 0;
            }
        }

        private IEnumerator DoLoadLevel()
        {
            ResetData();
            yield return null;
        }
        private void ResetData()
        {
            passedCarsCount = 0;
            passedCarsReward = 0;

            crashedCarsCount = 0;
            crashedCarsReward = 0;

            brokenPropsCount = 0;
            brokenPropsReward = 0;

            unlockedAchivsCount = 0;
            unlockedAchivsReward = 0;

            oneSignalCarPassed = 0;
        }

        /// ======================================================================
    }
}