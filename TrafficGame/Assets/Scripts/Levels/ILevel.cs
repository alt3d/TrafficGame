﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ILevel 
	{
        /// ======================================================================

        ELevelState state { get; }
        EPauseState pause { get; }
        float time { get; }

        /// ======================================================================

        void SetPause();
        void UnsetPause();
        void TogglePause();

        /// ======================================================================

        event Action<ELevelState> StateChanged;
        event Action<EPauseState> PauseChanged;

        /// ======================================================================
    }
}