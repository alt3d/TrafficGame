﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IStats 
	{
		/// ======================================================================

        int passedCarsCount { get; }
        int passedCarsReward { get; }

        int crashedCarsCount { get; }
        int crashedCarsReward { get; }

        int brokenPropsCount { get; }
        int brokenPropsReward { get; }

        int unlockedAchivsCount { get; }
        int unlockedAchivsReward { get; }

        int oneSignalCarPassed { get; }

        /// ======================================================================

        event Action<int> PassedCarsCountChanged;
        event Action<int> PassedCarsRewardChanged;

        event Action<int> CrashedCarsCountChanged;
        event Action<int> CrashedCarsRewardChanged;

        event Action<int> BrokenPropsCountChanged;
        event Action<int> BrokenPropsRewardChanged;

        event Action<int> UnlockedAchivsCountChanged;
        event Action<int> UnlockedAchivsRewardChanged;

        event Action<int> OneSignalCarPassedChanged;

        /// ======================================================================
    }
}