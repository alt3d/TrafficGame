﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Levels
{
	public class ALevelsController : MonoBehaviour
	{
        /// ======================================================================

        private ALevel level;
        private ITown town;

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ALevel>();
            town = AServices.Get<ITown>();
        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
            level.PauseChanged += OnLevelPauseChanged;
        }
        private void OnDisable()
        {
            level.StateChanged -= OnLevelStateChanged;
            level.PauseChanged -= OnLevelPauseChanged;
        }

        private void Update()
        {
            if (level.state == ELevelState.Play)
                if (town.state == ETownState.Play)
                    level.time += Time.deltaTime;
        }

        private void OnLevelStateChanged(ELevelState state)
        {
            switch (state)
            {
                case ELevelState.Play:
                    level.time = 0f;
                    level.pause = EPauseState.Off;
                    break;
            }
        }
        private void OnLevelPauseChanged(EPauseState state)
        {
            switch (state)
            {
                case EPauseState.On:
                    Time.timeScale = 0f;
                    break;
                case EPauseState.Off:
                    Time.timeScale = 1f;
                    break;
            }
        }

        /// ======================================================================
    }
}