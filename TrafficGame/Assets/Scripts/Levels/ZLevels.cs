﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Levels;

namespace Alt3d.TrafficGame
{
	public static class ZLevels 
	{
        /// ======================================================================

        public static EPauseState GetOpposite(EPauseState state)
        {
            switch (state)
            {
                case EPauseState.On: return EPauseState.Off;
                case EPauseState.Off: return EPauseState.On;
                default: throw new Exception(state.ToString());
            }
        }

        public static void Init(IContainer container)
        {
            var level = container.AddComponent<ALevel>();
            container.Register<ALevel>(level);
            container.Register<ILevel>(level);

            var stats = container.AddComponent<AStats>();
            container.Register<IStats>(stats);

            container.AddComponent<ALevelsController>();
        }

		/// ======================================================================
	}
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum ELevelState
    {
        Stop,
        Play
    }
    public enum EPauseState
    {
        On,
        Off
    }


    /// ======================================================================
}