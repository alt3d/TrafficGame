﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.App;
using Alt3d.TrafficGame.Game;
using Alt3d.TrafficGame.Levels;
using Alt3d.TrafficGame.Towns;

namespace Alt3d.TrafficGame.Main
{
	public class AMain : MonoBehaviour, IMain
	{
        /// ======================================================================

        private AApp app;
        private AGame game;
        private ALevel level;
        private ATown town;
        private IInterstitialAdvert ads;

        private IAppActionsRepository appActions;
        private IGameActionsRepository gameActions;
        private ILevelActionsRepository levelActions;

        /// ======================================================================

        private void Awake()
        {
            app = AServices.Get<AApp>();
            game = AServices.Get<AGame>();
            level = AServices.Get<ALevel>();
            town = AServices.Get<ATown>();
            ads = AServices.Get<IInterstitialAdvert>();

            appActions = AServices.Get<IAppActionsRepository>();
            gameActions = AServices.Get<IGameActionsRepository>();
            levelActions = AServices.Get<ILevelActionsRepository>();
        }
        private IEnumerator Start()
        {
            yield return null;
            LaunchApp();
        }
        private void OnApplicationQuit()
        {
            QuitApp();
        }

        /// ======================================================================

        private void LaunchApp()
        {
            StartCoroutine(DoLaunchApp());
        }
        private IEnumerator DoLaunchApp()
        {
            app.state = EAppState.Launch;

            yield return null;
            DisableAllTownRoots();
            yield return null;

            foreach (var action in appActions.GetLoad())
            {
                yield return action();
            }

            app.state = EAppState.Game;
            LaunchGame();
        }

        private void QuitApp()
        {
            DoQuit();
            DisableAllRoots();
        }
        private void DoQuit()
        {
            QuitGame();
            app.state = EAppState.Quit;

            foreach (var action in appActions.GetExit())
            {
                action();
            }

            app.state = EAppState.Stop;
        }
        private void DisableAllRoots()
        {
            var roots = UGameObject.GetAllRoots();
            foreach (var go in roots)
            {
                go.SetActive(false);
            }
        }

        /// ======================================================================

        private void LaunchGame()
        {
            StartCoroutine(DoLaunchGame());
        }
        private IEnumerator DoLaunchGame()
        {
            game.state = EGameState.Launch;

            foreach (var action in gameActions.GetLoad())
            {
                yield return action();
            }

            game.state = EGameState.Menu;
        }

        private void QuitGame()
        {
            DoQuitGame();
        }
        private void DoQuitGame()
        {
            game.state = EGameState.Quit;

            foreach (var action in gameActions.GetExit())
            {
                action();
            }

            game.state = EGameState.Stop;
        }

        /// ======================================================================

        public void LaunchLevel(ETownID id)
        {
            town.town = id;
            StartCoroutine(DoLaunchLevel());
        }
        private IEnumerator DoLaunchLevel()
        {
            game.state = EGameState.LevelLaunch;

            yield return null;
            EnableTownRoot(town.town);
            yield return null;

            foreach (var action in levelActions.GetLoad())
            {
                yield return action();
            }

            game.state = EGameState.LevelPlay;
            level.state = ELevelState.Play;
        }

        public void QuitLevel()
        {
            StartCoroutine(DoQuitLevel());
        }
        private IEnumerator DoQuitLevel()
        {
            level.state = ELevelState.Stop;
            game.state = EGameState.LevelQuit;

            yield return null;
            DisableTownRoot(town.town);
            yield return null;

            foreach (var action in levelActions.GetExit())
            {
                yield return action();
            }

            if (ads.IsNeedAds())
            {
                game.state = EGameState.InterstitialAdvert;
                yield return ads.DoShowAds();
            }

            game.state = EGameState.LevelResult;
        }

        /// ======================================================================

        private void EnableTownRoot(ETownID id)
        {
            var parent = AScene.GetTownParent(id);
            parent.gameObject.SetActive(true);
        }
        private void DisableTownRoot(ETownID id)
        {
            var parent = AScene.GetTownParent(id);
            parent.gameObject.SetActive(false);
        }
        private void DisableAllTownRoots()
        {
            var parents = AScene.GetAllTownParents();
            foreach (var parent in parents)
            {
                parent.gameObject.SetActive(false);
            }
        }

        /// ======================================================================
    }
}