﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Main;

namespace Alt3d.TrafficGame
{
	public static class ZMain 
	{
        /// ======================================================================

        public static void Init(IContainer container)
        {
            var main = container.AddComponent<AMain>();
            container.Register<IMain>(main);
        }

        /// ======================================================================
    }
}