﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Misc
{
	public class AFpsCounter 
	{
        /// ======================================================================

        private int _current = 0;
        private int _average = 0;

        private int[] array = new int[count];
        private int index = 0;
        private const int count = 60;
        private const float avgDivider = 1f / count;

        /// ======================================================================

        public int current
        {
            get { return _current; }
            private set { _current = value; }
        }
        public int average
        {
            get { return _average; }
            private set { _average = value; }
        }

        /// ======================================================================

        public void Add(int fps)
        {
            current = fps;
            AddToToList(fps);
            CalculateAverage();
        }
        public void Reset()
        {
            for (int i = 0; i < count; i++)
            {
                array[i] = 0;
            }
            average = 0;
        }

        private void AddToToList(int value)
        {
            array[index] = value;
            index++;
            if (index >= count) index = 0;
        }
        private void CalculateAverage()
        {
            var sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += array[i];
            }
            average = Mathf.RoundToInt(sum * avgDivider);
        }

        /// ======================================================================
    }
}