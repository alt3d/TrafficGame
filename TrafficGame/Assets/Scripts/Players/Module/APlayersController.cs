﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Players
{
	public class APlayersController : MonoBehaviour
	{
        /// ======================================================================

        private IInput input;
        private ILevel level;
        private ITown town;

        /// ======================================================================

        private void Awake()
        {
            input = AServices.Get<IInput>();
            level = AServices.Get<ILevel>();
            town = AServices.Get<ITown>();
        }
        private void OnEnable()
        {
            input.ClickOverScene += ClickOverScene;
        }
        private void OnDisable()
        {
            input.ClickOverScene -= ClickOverScene;
        }

        /// ======================================================================

        private void ClickOverScene()
        {
            if (level.state != ELevelState.Play) return;
            if (level.pause != EPauseState.Off) return;
            if (town.state != ETownState.Play) return;

            town.ToggleSignal();
        }

        /// ======================================================================
    }
}