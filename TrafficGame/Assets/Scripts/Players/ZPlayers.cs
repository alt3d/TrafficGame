﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Players;

namespace Alt3d.TrafficGame
{
	public static class ZPlayers 
	{
        /// ======================================================================

        public static void Init(IContainer container)
        {
            container.AddComponent<APlayersController>();
        }

        /// ======================================================================
    }
}