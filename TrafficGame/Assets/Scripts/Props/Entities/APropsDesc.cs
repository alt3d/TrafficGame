﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props
{
	public class APropsDesc : IPropsDesc
	{
		/// ======================================================================

        public EPropsID id { get; set; }
        public int brokeReward { get; set; }

		/// ======================================================================
	}
}