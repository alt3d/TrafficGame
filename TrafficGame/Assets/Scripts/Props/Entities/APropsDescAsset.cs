﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props
{
	public class APropsDescAsset : ScriptableObject
	{
        /// ======================================================================

        [SerializeField] private int _brokeReward = 10;

        /// ======================================================================

        public int brokeReward
        {
            get { return _brokeReward; }
        }

        /// ======================================================================
    }
}