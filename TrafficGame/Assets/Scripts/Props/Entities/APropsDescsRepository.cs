﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props
{
	public class APropsDescRepository : MonoBehaviour, IPropsDescsRepository
	{
        /// ======================================================================

        private readonly Dictionary<EPropsID, APropsDesc> dic = new Dictionary<EPropsID, APropsDesc>();

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
        }

        public IPropsDesc Get(EPropsID id)
        {
            return dic[id];
        }
        public List<IPropsDesc> GetAll()
        {
            var result = new List<IPropsDesc>();
            foreach (var item in dic.Values)
            {
                result.Add(item);
            }
            return result;
        }

        public int GetBrokeReward(EPropsID id)
        {
            if (dic.ContainsKey(id))
                return dic[id].brokeReward;

            return 0;
        }

        private IEnumerator DoLoadApp()
        {
            LoadAssets();
            yield return null;
        }
        private void LoadAssets()
        {
            var path = AResources.propsDescFolder;
            var assets = UResources.LoadAll<APropsDescAsset>(path);
            foreach (var asset in assets)
            {
                var desc = new APropsDesc();
                desc.id = UEnums.Parse<EPropsID>(asset.name);
                desc.brokeReward = asset.brokeReward;
                dic.Add(desc.id, desc);
            }
        }

        /// ======================================================================
    }
}