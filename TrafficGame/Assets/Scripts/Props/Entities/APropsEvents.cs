﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Props
{
	public class APropsEvents : MonoBehaviour, IPropsEvents
	{
        /// ======================================================================

        public event Action<EPropsID> PropsBroken = delegate { };

        /// ======================================================================

        public void RisePropsBroken(EPropsID id)
        {
            PropsBroken(id);
        }

        /// ======================================================================
    }
}