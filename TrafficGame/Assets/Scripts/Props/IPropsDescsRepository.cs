﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IPropsDescsRepository 
	{
        /// ======================================================================

        int GetBrokeReward(EPropsID id);

        /// ======================================================================
    }
}