﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IPropsEvents 
	{
        /// ======================================================================

        event Action<EPropsID> PropsBroken;

		/// ======================================================================
	}
}