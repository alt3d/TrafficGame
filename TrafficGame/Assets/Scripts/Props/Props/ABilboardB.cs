﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
    public class ABilboardB : AProps
    {
        /// ======================================================================

        [Space]
        public AFixedJoint body;
        public float bodyForce;

        [Space]
        public AFixedJoint leftColumn;
        public AFixedJoint rightColumn;
        public float columnsForce;

        [Space]
        public AudioSource source;

        /// ======================================================================

        private bool isBodyBroken { get; set; }
        private bool isLeftColumnBroken { get; set; }
        private bool isRightColumnBroken { get; set; }
        private bool isBroken
        {
            get
            {
                if (isBodyBroken) return true;
                if (isLeftColumnBroken) return true;
                if (isRightColumnBroken) return true;
                return false;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            body.Break += OnJointBroken;
            leftColumn.Break += OnJointBroken;
            rightColumn.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            body.Break -= OnJointBroken;
            leftColumn.Break -= OnJointBroken;
            rightColumn.Break -= OnJointBroken;
        }

        private void Setup()
        {
            body.Setup();
            leftColumn.Setup();
            rightColumn.Setup();
        }
        private void Restore()
        {
            isBodyBroken = false;
            isLeftColumnBroken = false;
            isRightColumnBroken = false;

            body.Restore(bodyForce);
            leftColumn.Restore(columnsForce);
            rightColumn.Restore(columnsForce);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                RiseBrokenEvent(EPropsID.BilboardB);
                source.PlayOneShot(source.clip);
            }

            if (joint == body)
                isBodyBroken = true;

            if (joint == leftColumn)
                isLeftColumnBroken = true;

            if (joint == rightColumn)
                isRightColumnBroken = true;
        }

        /// ======================================================================
    }
}