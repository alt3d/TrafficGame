﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class ACafeTableA : AProps
	{
        /// ======================================================================

        public AFixedJoint table;
        public float tableForce;

        [Space]
        public AFixedJoint[] chairs;
        public float chairsForce;

        [Space]
        public AudioSource audioSource;

        /// ======================================================================

        private bool isTableBroken { get; set; }
        private bool[] isChairBroken { get; set; }
        private bool isBroken
        {
            get
            {
                if (isTableBroken) return true;
                foreach (var chair in isChairBroken)
                {
                    if (chair) return true;
                }
                return false;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            isChairBroken = new bool[chairs.Length];
            for (int i = 0; i < chairs.Length; i++)
            {
                isChairBroken[i] = false;
            }
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            table.Break += OnJointBroken;
            foreach (var chair in chairs)
            {
                chair.Break += OnJointBroken;
            }
        }
        private void OnDisable()
        {
            table.Break -= OnJointBroken;
            foreach (var chair in chairs)
            {
                chair.Break -= OnJointBroken;
            }
        }

        private void Setup()
        {
            table.Setup();
            foreach (var chair in chairs)
            {
                chair.Setup();
            }
        }
        private void Restore()
        {
            isTableBroken = false;
            for (int i = 0; i < isChairBroken.Length; i++)
            {
                isChairBroken[i] = false;
            }

            table.Restore(tableForce);
            for (int i = 0; i < chairs.Length; i++)
            {
                chairs[i].Restore(chairsForce);
            }
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                audioSource.PlayOneShot(audioSource.clip);
                RiseBrokenEvent(EPropsID.CafeTable);
            }

            if (joint == table)
                isTableBroken = true;

            for (int i = 0; i < chairs.Length; i++)
            {
                if (joint == chairs[i])
                    isChairBroken[i] = true;
            }
        }

        /// ======================================================================
    }
}