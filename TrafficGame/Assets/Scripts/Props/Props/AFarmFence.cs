﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class AFarmFence : AProps
    {
        /// ======================================================================

        public AFixedJoint body;
        public float bodyForce;

        [Space]
        public AudioSource audioSource;

        /// ======================================================================

        private bool isBodyBroken { get; set; }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            body.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            body.Break -= OnJointBroken;
        }

        private void Setup()
        {
            body.Setup();
        }
        private void Restore()
        {
            isBodyBroken = false;
            body.Restore(bodyForce);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBodyBroken)
                return;

            var random = Random.Range(0, 10);
            if (random < 5)
                audioSource.PlayOneShot(audioSource.clip);

            RiseBrokenEvent(EPropsID.FarmFence);
            isBodyBroken = true;
        }

        /// ======================================================================
    }
}