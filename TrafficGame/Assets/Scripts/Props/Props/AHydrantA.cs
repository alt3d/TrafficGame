﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class AHydrantA : AProps
	{
        /// ======================================================================

        public AFixedJoint column;
        public float columnForce;
       
        [Space]
        public ParticleSystem jetParticles;
        public AudioSource jetSource;

        private bool isColumnBroken;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();
            column.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            column.Break -= OnJointBroken;
        }

        private void Setup()
        {
            column.Setup();
        }
        private void Restore()
        {
            isColumnBroken = false;
            column.Restore(columnForce);

            jetParticles.Stop(true);
            jetParticles.Clear(true);
            jetSource.Stop();
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isColumnBroken)
                return;

            jetParticles.Play(true);
            jetSource.Play();

            RiseBrokenEvent(EPropsID.HydrantA);
            isColumnBroken = true;
        }

        /// ======================================================================
    }
}