﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class AIcecreamShopSign : AProps
    {
        /// ======================================================================

        public Rigidbody body;
        public Transform com;
        public float rotationSpeed;

        [Space]
        public ACollider volume;
        public AudioSource audioSource;

        private Vector3 bodyStartPos;
        private Quaternion bodyStartRot;
        private bool isBodyBroken;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();

            body.centerOfMass = com.localPosition;
            bodyStartPos = body.transform.position;
            bodyStartRot = body.transform.rotation;
        }
        private void OnEnable()
        {
            Restore();
            volume.CollisionEnter += OnCollisioned;
        }
        private void OnDisable()
        {
            volume.CollisionEnter -= OnCollisioned;
        }
        private void Update()
        {
            if (isBodyBroken)
                return;

            var angle = rotationSpeed * Time.deltaTime;
            var rot = Quaternion.AngleAxis(angle, Vector3.up);
            body.transform.rotation *= rot;
        }

        private void Restore()
        {
            isBodyBroken = false;
            body.isKinematic = true;

            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
            body.Sleep();

            body.transform.position = bodyStartPos;
            body.transform.rotation = bodyStartRot;

            var angle = Random.Range(0f, 360f);
            var rot = Quaternion.AngleAxis(angle, Vector3.up);
            body.transform.rotation *= rot;
        }
        private void OnCollisioned(ACollider cldr, Collision collision)
        {
            if (isBodyBroken)
                return;

            RiseBrokenEvent(EPropsID.IcecreamShopSign);
            audioSource.PlayOneShot(audioSource.clip);

            isBodyBroken = true;
            body.isKinematic = false;
        }

        /// ======================================================================
    }
}