﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class AOuthouse : AProps
    {
        /// ======================================================================

        public AFixedJoint body;
        public float bodyForce;

        [Space]
        public AFixedJoint door;
        public float doorForce;

        [Space]
        public AudioSource audioSource;

        /// ======================================================================

        private bool isBodyBroken { get; set; }
        private bool isDoorBroken { get; set; }
        private bool isBroken
        {
            get
            {
                if (isBodyBroken) return true;
                if (isDoorBroken) return true;
                return false;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            body.Break += OnJointBroken;
            door.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            body.Break -= OnJointBroken;
            door.Break -= OnJointBroken;
        }

        private void Setup()
        {
            body.Setup();
            door.Setup();
        }
        private void Restore()
        {
            isBodyBroken = false;
            isDoorBroken = false;

            body.Restore(bodyForce);
            door.Restore(doorForce);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                audioSource.PlayOneShot(audioSource.clip);
                RiseBrokenEvent(EPropsID.Outhouse);
            }

            if (joint == body)
                isBodyBroken = true;

            if (joint == door)
                isDoorBroken = true;
        }

        /// ======================================================================
    }
}