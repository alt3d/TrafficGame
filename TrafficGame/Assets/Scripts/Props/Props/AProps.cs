﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Props
{
	public abstract class AProps : MonoBehaviour
	{
        /// ======================================================================

        private APropsEvents events;

        /// ======================================================================

        protected virtual void Awake()
        {
            events = AServices.Get<APropsEvents>();
        }

        protected void RiseBrokenEvent(EPropsID id)
        {
            events.RisePropsBroken(id);
        }

        /// ======================================================================
    }
}