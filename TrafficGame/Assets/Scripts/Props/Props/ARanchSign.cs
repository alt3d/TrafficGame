﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class ARanchSign : AProps
    {
        /// ======================================================================

        public AFixedJoint center;
        public float centerForce;

        [Space]
        public AFixedJoint left;
        public AFixedJoint right;
        public float sideForce;

        [Space]
        public AFixedJoint sign;
        public float signForce;

        [Space]
        public AudioSource audioSource;

        /// ======================================================================

        private bool isCenterBroken { get; set; }
        private bool isLeftBroken { get; set; }
        private bool isRightBroken { get; set; }
        private bool isSignBroken { get; set; }
        private bool isBroken
        {
            get
            {
                if (isCenterBroken) return true;
                if (isLeftBroken) return true;
                if (isRightBroken) return true;
                if (isSignBroken) return true;
                return false;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            center.Break += OnJointBroken;
            left.Break += OnJointBroken;
            right.Break += OnJointBroken;
            sign.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            center.Break -= OnJointBroken;
            left.Break -= OnJointBroken;
            right.Break -= OnJointBroken;
            sign.Break -= OnJointBroken;
        }

        private void Setup()
        {
            center.Setup();
            left.Setup();
            right.Setup();
            sign.Setup();
        }
        private void Restore()
        {
            isCenterBroken = false;
            isLeftBroken = false;
            isRightBroken = false;
            isSignBroken = false;

            center.Restore(centerForce);
            left.Restore(sideForce);
            right.Restore(sideForce);
            sign.Restore(signForce);

            sign.Attach(center);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                audioSource.PlayOneShot(audioSource.clip);
                RiseBrokenEvent(EPropsID.RanchSign);
            }

            if (joint == center)
                isCenterBroken = true;

            if (joint == left)
                isCenterBroken = true;

            if (joint == right)
                isCenterBroken = true;

            if (joint == sign)
                isCenterBroken = true;
        }

        /// ======================================================================
    }
}