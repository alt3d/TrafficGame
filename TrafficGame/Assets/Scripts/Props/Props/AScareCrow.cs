﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class AScareCrow : AProps
    {
        /// ======================================================================

        public AFixedJoint body;
        public float bodyForce;

        [Space]
        public AudioSource audioSource;

        /// ======================================================================

        private bool isBodyBroken { get; set; }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            body.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            body.Break -= OnJointBroken;
        }

        private void Setup()
        {
            body.Setup();
        }
        private void Restore()
        {
            isBodyBroken = false;
            body.Restore(bodyForce);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBodyBroken)
                return;

            audioSource.PlayOneShot(audioSource.clip);
            RiseBrokenEvent(EPropsID.ScareCrow);
            isBodyBroken = true;
        }

        /// ======================================================================
    }
}