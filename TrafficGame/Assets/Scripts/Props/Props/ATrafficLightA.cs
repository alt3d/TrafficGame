﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
	public class ATrafficLightA : AProps
	{
        /// ======================================================================

        public AFixedJoint column;
        public float columnForce;

        [Space]
        public AFixedJoint lamp;
        public float lampForce;

        [Space]
        public AudioSource source;

        /// ======================================================================

        private bool isColumnBroken { get; set; }
        private bool isLampBroken { get; set; }
        private bool isBroken
        {
            get
            {
                return isColumnBroken && isLampBroken;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();

            column.Break += OnJointBroken;
            lamp.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            column.Break -= OnJointBroken;
            lamp.Break -= OnJointBroken;
        }

        private void Setup()
        {
            column.Setup();
            lamp.Setup();
        }
        private void Restore()
        {
            isColumnBroken = false;
            isLampBroken = false;

            column.Restore(columnForce);
            lamp.Restore(lampForce);
            lamp.Attach(column);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                RiseBrokenEvent(EPropsID.TrafficLightA);
                source.PlayOneShot(source.clip);
            }

            if (joint == column)
            {
                isColumnBroken = true;
            }

            if (joint == lamp)
            {
                isLampBroken = true;
            }
        }

        /// ======================================================================
    }
}