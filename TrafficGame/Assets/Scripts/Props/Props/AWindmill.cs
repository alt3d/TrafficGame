﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Props.Props
{
    public class AWindmill : AProps
    {
        /// ======================================================================

        public AFixedJoint body;
        public float bodyForce;

        [Space]
        public AFixedJoint tail;
        public float tailForce;

        [Space]
        public AFixedJoint blades;
        public float bladesForce;

        [Space]
        public Transform bladesMesh;
        public float bladesRotationSpeed;
        public float bladesFadeTime;

        [Space]
        public AudioSource audioSource;

        private Coroutine cUpdate;

        /// ======================================================================

        private bool isBodyBroken { get; set; }
        private bool isTailBroken { get; set; }
        private bool isBladesBroken { get; set; }
        private bool isBroken
        {
            get
            {
                if (isBodyBroken) return true;
                if (isTailBroken) return true;
                if (isBladesBroken) return true;
                return false;
            }
        }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Setup();
        }
        private void OnEnable()
        {
            Restore();
            if (cUpdate != null) StopCoroutine(cUpdate);
            cUpdate = StartCoroutine(DoRotateBlades());

            body.Break += OnJointBroken;
            tail.Break += OnJointBroken;
            blades.Break += OnJointBroken;
        }
        private void OnDisable()
        {
            body.Break -= OnJointBroken;
            tail.Break -= OnJointBroken;
            blades.Break -= OnJointBroken;
        }

        private void Setup()
        {
            body.Setup();
            tail.Setup();
            blades.Setup();
        }
        private void Restore()
        {
            isBodyBroken = false;
            isTailBroken = false;
            isBladesBroken = false;

            body.Restore(bodyForce);
            tail.Restore(tailForce);
            blades.Restore(bladesForce);

            tail.Attach(body);
            blades.Attach(body);
        }

        private void OnJointBroken(AFixedJoint joint)
        {
            if (isBroken)
                return;

            if (isBroken == false)
            {
                audioSource.PlayOneShot(audioSource.clip);
                RiseBrokenEvent(EPropsID.Windmill);

                if (cUpdate != null) StopCoroutine(cUpdate);
                cUpdate = StartCoroutine(DoFadeBlades());
            }

            if (joint == body)
                isBodyBroken = true;

            if (joint == tail)
                isTailBroken = true;

            if (joint == blades)
                isBladesBroken = true;
        }

        private IEnumerator DoRotateBlades()
        {
            while (true)
            {
                var angle = bladesRotationSpeed * Time.deltaTime;
                var rot = Quaternion.AngleAxis(angle, Vector3.forward);
                bladesMesh.localRotation *= rot;

                yield return null;
            }
        }
        private IEnumerator DoFadeBlades()
        {
            var counter = bladesFadeTime;
            while(counter > 0)
            {
                var lerp = counter / bladesFadeTime;
                var speed = Mathf.Lerp(0, bladesRotationSpeed, lerp);
                var angle = speed * Time.deltaTime;
                var rot = Quaternion.AngleAxis(angle, Vector3.forward);
                bladesMesh.localRotation *= rot;

                counter -= Time.deltaTime;
                yield return null;
            }
        }

        /// ======================================================================
    }
}