﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Props;

namespace Alt3d.TrafficGame
{
	public static class ZProps 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var events = container.AddComponent<APropsEvents>();
            container.Register<APropsEvents>(events);
            container.Register<IPropsEvents>(events);

            var repo = container.AddComponent<APropsDescRepository>();
            container.Register<IPropsDescsRepository>(repo);
        }

		/// ======================================================================
	}
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum EPropsID
    {
        TrafficLightA,
        HydrantA,
        StreetLamp,
        BilboardA,
        BilboardB,
        PaperBoxA,
        CafeTable,
        HotelSignA,
        IcecreamShopSign,
        //-----
        FarmFence,
        Outhouse,
        RanchSign,
        Windmill,
        WaterTower,
        ScareCrow
    }

    /// ======================================================================
}