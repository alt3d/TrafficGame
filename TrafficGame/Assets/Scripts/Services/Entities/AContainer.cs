﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Services
{
	public class AContainer : IContainer
	{
        /// ======================================================================

        public Dictionary<Type, object> dic { get; private set; }
        public GameObject go { get; private set; }

        /// ======================================================================

        public T AddComponent<T>() where T : MonoBehaviour
        {
            var t = typeof(T);

            if (dic.ContainsKey(t))
                throw new Exception("Services contains type: " + t.ToString());

            return go.AddComponent<T>();
        }
        public T LoadAsset<T>(string path) where T : ScriptableObject
        {
            var t = typeof(T);

            if (dic.ContainsKey(t))
                throw new Exception("Services contains type: " + t.ToString());

            return UResources.Load<T>(path);
        }
        public T CreateClass<T>()
        {
            var t = typeof(T);

            if (dic.ContainsKey(t))
                throw new Exception("Services contains type: " + t.ToString());

            if (t.IsSubclassOf(typeof(MonoBehaviour)))
                throw new Exception("Can't constuct MonoBehaviour: " + t.ToString());

            if (t.IsSubclassOf(typeof(ScriptableObject)))
                throw new Exception("Can't constuct ScriptableObject: " + t.ToString());

            return Activator.CreateInstance<T>();
        }
        public void Register<T>(object o)
        {
            var t = typeof(T);

            if (o == null)
                throw new Exception("Instance is null: " + t.ToString());

            if ((o is T) == false)
                throw new Exception("Instance can't cast to type: " + o.GetType().ToString() + " => " + t.ToString());

            if (dic.ContainsKey(t))
                throw new Exception("Services contains type: " + t.ToString());

            dic.Add(t, o);
        }

        /// ======================================================================

        public AContainer()
        {
            dic = new Dictionary<Type, object>();

            go = new GameObject("CORE");
            go.hideFlags = HideFlags.NotEditable;
            go.SetActive(false);
        }

        /// ======================================================================
    }
}