﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IContainer 
	{
        /// ======================================================================

        GameObject go { get; }

        /// ======================================================================

        T AddComponent<T>() where T : MonoBehaviour;
        T LoadAsset<T>(string path) where T : ScriptableObject;
        T CreateClass<T>();

        void Register<T>(object o);

        /// ======================================================================
    }
}