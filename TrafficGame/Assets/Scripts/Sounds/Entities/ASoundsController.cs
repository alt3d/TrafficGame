﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEngine.Audio;

namespace Alt3d.TrafficGame.Sounds
{
	public class ASoundsController : MonoBehaviour
	{
        /// ======================================================================

        private IGame game;
        private IGameSettings gameSettings;

        private AudioMixer mixer;
        private AudioMixerSnapshot normalSnap;
        private AudioMixerSnapshot adsSnap;
        private const float timeForTransition = 0f;

        /// ======================================================================

        private void Awake()
        {
            game = AServices.Get<IGame>();
            gameSettings = AServices.Get<IGameSettings>();

            mixer = Resources.Load<AudioMixer>("Audio/AudioMixer");
            normalSnap = mixer.FindSnapshot("Normal");
            adsSnap = mixer.FindSnapshot("Ads");
        }
        private void OnEnable()
        {
            game.StateChanged += OnGameStateChanged;
            gameSettings.MasterVolumeChanged += OnMasterVolumeChanged;
        }

        private void OnDisable()
        {
            game.StateChanged -= OnGameStateChanged;
            gameSettings.MasterVolumeChanged -= OnMasterVolumeChanged;
        }

        private void OnGameStateChanged(EGameState state)
        {
            if (state == EGameState.InterstitialAdvert)
                adsSnap.TransitionTo(timeForTransition);
            else
                normalSnap.TransitionTo(timeForTransition);
        }
        private void OnMasterVolumeChanged(float volume)
        {
            var min = -50f;
            var max = 0f;
            var value = Mathf.Lerp(min, max, volume);
            mixer.SetFloat("MasterVolume", value);
        }

        /// ======================================================================
    }
}