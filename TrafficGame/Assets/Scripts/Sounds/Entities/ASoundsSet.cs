﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Sounds
{
	public class ASoundsSet : ScriptableObject 
	{
        /// ======================================================================

        [SerializeField] protected AudioClip _menuBack;
        [SerializeField] protected AudioClip _townBack;

        [Space]
        [SerializeField] protected AudioClip _buttonClick;
        [SerializeField] protected AudioClip _townSignalChanged;
        [SerializeField] protected AudioClip _townCrash;
        [SerializeField] protected AudioClip _achivUnlocked;
        [SerializeField] protected AudioClip _carPassed;

        /// ======================================================================

        public AudioClip menuBack
        {
            get { return _menuBack; }
        }
        public AudioClip townBack
        {
            get { return _townBack; }
        }

        public AudioClip buttonClick
        {
            get { return _buttonClick; }
        }
        public AudioClip townSignalChanged
        {
            get { return _townSignalChanged; }
        }
        public AudioClip townCrash
        {
            get { return _townCrash; }
        }
        public AudioClip achivUnlocked
        {
            get { return _achivUnlocked; }
        }
        public AudioClip carPassed
        {
            get { return _carPassed; }
        }

        /// ======================================================================
    }
}