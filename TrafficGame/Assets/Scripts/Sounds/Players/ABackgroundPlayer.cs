﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Sounds
{
	public class ABackgroundPlayer : MonoBehaviour
	{
        /// ======================================================================

        public AudioSource source;

        private IGame game;
        private ILevel level;
        private ITown town;
        private ASoundsSet set;

        /// ======================================================================

        private void Awake()
        {
            game = AServices.Get<IGame>();
            level = AServices.Get<ILevel>();
            town = AServices.Get<ITown>();
            set = AServices.Get<ASoundsSet>();
        }
        private void OnEnable()
        {
            game.StateChanged += OnGameStateChanged;
            level.StateChanged += OnLevelStateChanged;
            town.StateChanged += OnTownStateChanged;
        }
        private void OnDisable()
        {
            game.StateChanged -= OnGameStateChanged;
            level.StateChanged -= OnLevelStateChanged;
            town.StateChanged -= OnTownStateChanged;
        }

        private void OnGameStateChanged(EGameState state)
        {
            switch (state)
            {
                case EGameState.LevelPlay:
                    StopMenuMusic();
                    StartTownMusic();
                    break;
                case EGameState.Menu:
                    StopTownMusic();
                    StartMenuMusic();
                    break;
            }
        }
        private void OnLevelStateChanged(ELevelState state)
        {
            if (state == ELevelState.Stop)
            {
                if (town.state == ETownState.Play)
                {
                    StopTownMusic();
                    StartMenuMusic();
                }
            }
        }
        private void OnTownStateChanged(ETownState state)
        {
            switch (state)
            {
                case ETownState.Crash:
                    StopTownMusic();
                    StartMenuMusic();
                    break;
            }
        }

        private void StartTownMusic()
        {
            var clip = set.townBack;

            source.clip = clip;
            source.time = Alt3d.USounds.GetRandomTime(clip);

            source.Play();
        }
        private void StopTownMusic()
        {
            source.Stop();
        }

        private void StartMenuMusic()
        {
            var clip = set.menuBack;

            source.clip = clip;
            source.time = Alt3d.USounds.GetRandomTime(clip);

            source.Play();
        }
        private void StopMenuMusic()
        {
            source.Stop();
        }

        /// ======================================================================
    }
}