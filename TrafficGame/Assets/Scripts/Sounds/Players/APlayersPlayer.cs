﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.Sounds
{
	public class APlayersPlayer : MonoBehaviour
	{
        /// ======================================================================

        public AudioSource source;

        private IAchivsEvents achivsEvents;
        private ASoundsSet soundsSet;

        /// ======================================================================

        private void Awake()
        {
            achivsEvents = AServices.Get<IAchivsEvents>();
            soundsSet = AServices.Get<ASoundsSet>();
        }
        private void OnEnable()
        {
            achivsEvents.AchivUnlocked += OnAchivUnlocked;
        }
        private void OnDisable()
        {
            achivsEvents.AchivUnlocked -= OnAchivUnlocked;
        }

        private void OnAchivUnlocked(IAchiv achiv)
        {
            source.PlayOneShot(soundsSet.achivUnlocked);
        }

        /// ======================================================================
    }
}