﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Sounds
{
	public class ATownsPlayer : MonoBehaviour
	{
        /// ======================================================================

        public AudioSource source;

        private ITown town;
        private ILevel level;
        private IStats stats;
        private ASoundsSet set;

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();
            level = AServices.Get<ILevel>();
            stats = AServices.Get<IStats>();
            set = AServices.Get<ASoundsSet>();
        }
        private void OnEnable()
        {
            town.SignalChanged += OnTownSignalChanged;
            town.StateChanged += OnTownStateChanged;

            stats.PassedCarsCountChanged += OnPassedCarsCountChanged;
            stats.CrashedCarsCountChanged += OnCrashedCarsCountChanged;
            stats.BrokenPropsCountChanged += OnBrokenPropsCountChanged;
            stats.UnlockedAchivsCountChanged += OnUnlockedAchivsCountChanged;
        }
        private void OnDisable()
        {
            town.SignalChanged -= OnTownSignalChanged;
            town.StateChanged -= OnTownStateChanged;

            stats.PassedCarsCountChanged -= OnPassedCarsCountChanged;
            stats.CrashedCarsCountChanged -= OnCrashedCarsCountChanged;
            stats.BrokenPropsCountChanged -= OnBrokenPropsCountChanged;
            stats.UnlockedAchivsCountChanged -= OnUnlockedAchivsCountChanged;
        }

        private void OnTownSignalChanged(ETrafficLightSignal signal)
        {
            if (level.state == ELevelState.Play)
                if (town.state == ETownState.Play)
                    if (level.time > 0.5f)
                        source.PlayOneShot(set.townSignalChanged);
        }
        private void OnTownStateChanged(ETownState state)
        {
            if (level.state == ELevelState.Play)
                if (state == ETownState.Crash)
                    source.PlayOneShot(set.townCrash);
        }

        private void OnPassedCarsCountChanged(int count)
        {
            if (level.state == ELevelState.Play)
                if (town.state == ETownState.Play)
                    source.PlayOneShot(set.carPassed);
        }
        private void OnCrashedCarsCountChanged(int count)
        {
            //if (level.state == ELevelState.Play)
            //    source.PlayOneShot(set.carPassed);
        }
        private void OnBrokenPropsCountChanged(int count)
        {
            //if (level.state == ELevelState.Play)
            //        source.PlayOneShot(set.carPassed);
        }
        private void OnUnlockedAchivsCountChanged(int count)
        {
            //if (level.state == ELevelState.Play)
            //    source.PlayOneShot(set.carPassed);
        }

        /// ======================================================================
    }
}