﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Sounds
{
	public class AUIsPlayer : MonoBehaviour
	{
        /// ======================================================================

        public AudioSource source;
        private IUI ui;
        private ASoundsSet set;

        /// ======================================================================

        private void Awake()
        {
            ui = AServices.Get<IUI>();
            set = AServices.Get<ASoundsSet>();
        }
        private void OnEnable()
        {
            ui.ButtonClicked += OnUiButtonClicked;
        }
        private void OnDisable()
        {
            ui.ButtonClicked -= OnUiButtonClicked;
        }

        private void OnUiButtonClicked()
        {
            source.PlayOneShot(set.buttonClick);
        }

        /// ======================================================================
    }
}