﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Sounds;

namespace Alt3d.TrafficGame
{
	public static class ZSounds 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var path = AResources.soundsSetPath;
            var soundsSet = container.LoadAsset<ASoundsSet>(path);
            container.Register<ASoundsSet>(soundsSet);

            container.AddComponent<ASoundsController>();
        }

        /// ======================================================================
    }
}