﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Streams
{
	public abstract class AStream : MonoBehaviour, IStream
    {
        /// ======================================================================

        private readonly string delimiter = "|";
        private readonly char[] delimiterChars = { '|' };

        /// ======================================================================

        public abstract bool HasKey(string key);
        public abstract void DeleteKey(string key);

        public abstract void EraseAll();
        public abstract void SaveAll();

        public abstract void SaveString(string key, string value);
        public abstract string LoadString(string key);

        public abstract void SaveBool(string key, bool value);
        public abstract bool LoadBool(string key);

        public abstract void SaveFloat(string key, float value);
        public abstract float LoadFloat(string key);

        public abstract void SaveInt(string key, int value);
        public abstract int LoadInt(string key);

        public void SaveArray(string key, string[] value)
        {
            var line = MakeLineFromArray(value);
            SaveString(key, line);
        }
        public string[] LoadArray(string key)
        {
            if (HasKey(key) == false) return new string[0];

            var line = LoadString(key);
            return MakeArrayFromLine(line);
        }

        /// ======================================================================

        private string MakeLineFromArray(string[] array)
        {
            var result = string.Empty;
            for (int i = 0; i < array.Length; i++)
            {
                if (i != array.Length - 1) result += array[i] + delimiter;
                else result += array[i];
            }
            return result;
        }
        private string[] MakeArrayFromLine(string line)
        {
            return line.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
        }

        /// ======================================================================

        public static AStream Create(GameObject go)
        {
            if (Application.isMobilePlatform)
                return go.AddComponent<AStreamPrefs>();
            else
                return go.AddComponent<AStreamXml>();
        }
        protected AStream()
        {

        }

        /// ======================================================================
    }
}