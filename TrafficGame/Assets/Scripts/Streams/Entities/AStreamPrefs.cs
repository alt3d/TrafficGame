﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Streams
{
	public class AStreamPrefs : AStream
	{
        /// ======================================================================

        public override bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }
        public override void DeleteKey(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }

        public override void EraseAll()
        {
            PlayerPrefs.DeleteAll();
        }
        public override void SaveAll()
        {
            PlayerPrefs.Save();
        }

        public override void SaveString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }
        public override string LoadString(string key)
        {
            if (HasKey(key) == false) return string.Empty;
            return PlayerPrefs.GetString(key);
        }

        public override void SaveBool(string key, bool value)
        {
            if (value) PlayerPrefs.SetString(key, "True");
            else PlayerPrefs.SetString(key, "False");
        }
        public override bool LoadBool(string key)
        {
            if (HasKey(key) == false) return false;

            var value = PlayerPrefs.GetString(key);
            if (value == "True") return true;
            else return false;
        }

        public override void SaveFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }
        public override float LoadFloat(string key)
        {
            if (HasKey(key) == false) return 0;
            return PlayerPrefs.GetFloat(key);
        }

        public override void SaveInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }
        public override int LoadInt(string key)
        {
            if (HasKey(key) == false) return 0;
            return PlayerPrefs.GetInt(key);
        }


        /// ======================================================================
    }
}