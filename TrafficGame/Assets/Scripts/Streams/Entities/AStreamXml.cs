﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Alt3d.TrafficGame.Streams
{
	public class AStreamXml : AStream
    {
        /// ======================================================================

        private string path { get; set; }
        private XmlDocument doc { get; set; }
        private XmlElement root { get; set; }

        /// ======================================================================

        private void Awake()
        {
            path = Application.persistentDataPath + "/Save.xml";

            if (File.Exists(path) == false)
            {
                var writter = new XmlTextWriter(path, null);
                writter.WriteStartDocument();
                writter.WriteStartElement("TG");
                writter.WriteEndElement();
                writter.Close();
            }

            doc = new XmlDocument();
            doc.Load(path);
            root = doc.DocumentElement;
        }
        private void OnDestroy()
        {
            doc.Save(path);
        }

        /// ======================================================================

        public override bool HasKey(string key)
        {
            var node = FindNode(key);
            return node != null;
        }
        public override void DeleteKey(string key)
        {
            if (HasKey(key) == false) return;
            var node = FindNode(key);
            root.RemoveChild(node);
        }

        public override void EraseAll()
        {
            root.RemoveAll();
        }
        public override void SaveAll()
        {
            doc.Save(path);
        }

        public override void SaveString(string key, string value)
        {
            var node = FindOrCreateNode(key);
            node.InnerText = value;
            SaveAll();
        }
        public override string LoadString(string key)
        {
            if (HasKey(key) == false) return string.Empty;
            return FindNode(key).InnerText;
        }

        public override void SaveBool(string key, bool value)
        {
            var node = FindOrCreateNode(key);
            node.InnerText = value.ToString();
            SaveAll();
        }
        public override bool LoadBool(string key)
        {
            if (HasKey(key) == false) return false;
            var value = FindNode(key).InnerText;
            return UText.ParseBool(value);
        }

        public override void SaveFloat(string key, float value)
        {
            var node = FindOrCreateNode(key);
            node.InnerText = value.ToString();
            SaveAll();
        }
        public override float LoadFloat(string key)
        {
            if (HasKey(key) == false) return 0;
            var value = FindNode(key).InnerText;
            return UText.ParseFloat(value);
        }

        public override void SaveInt(string key, int value)
        {
            var node = FindOrCreateNode(key);
            node.InnerText = value.ToString();
            SaveAll();
        }
        public override int LoadInt(string key)
        {
            if (HasKey(key) == false) return 0;
            var value = FindNode(key).InnerText;
            return UText.ParseInt(value);
        }

        /// ======================================================================

        private XmlNode FindNode(string key)
        {
            return root.SelectSingleNode(key);
        }
        private XmlNode CreateNode(string key)
        {
            var node = doc.CreateElement(key);
            root.AppendChild(node);
            return node;
        }
        private XmlNode FindOrCreateNode(string key)
        {
            var node = root.SelectSingleNode(key);
            if (node != null) return node;

            node = doc.CreateElement(key);
            root.AppendChild(node);
            return node;
        }

        /// ======================================================================
    }
}