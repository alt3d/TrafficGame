﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IStream 
	{
        /// ======================================================================

        bool HasKey(string key);
        void DeleteKey(string key);

        void EraseAll();
        void SaveAll();

        void SaveString(string key, string value);
        string LoadString(string key);

        void SaveBool(string key, bool value);
        bool LoadBool(string key);

        void SaveFloat(string key, float value);
        float LoadFloat(string key);

        void SaveInt(string key, int value);
        int LoadInt(string key);

        void SaveArray(string key, string[] value);
        string[] LoadArray(string key);

        /// ======================================================================
    }
}