﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using Alt3d.TrafficGame.Streams;

namespace Alt3d.TrafficGame
{
	public static class ZStreams 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var stream = AStream.Create(container.go);
            container.Register<IStream>(stream);
        }

		/// ======================================================================
	}
}