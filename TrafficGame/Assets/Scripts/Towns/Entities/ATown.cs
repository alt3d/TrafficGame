﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Towns
{
    public class ATown : MonoBehaviour, ITown
    {
        /// ======================================================================

        protected ETownID _town;
        protected ETownState _state;
        protected ETrafficLightSignal _signal;

        private ILevel level;
        private ILevelActionsRepository levelActions;
        private DLevelLoadAction levelLoadAction;
       
        /// ======================================================================

        public ETownID town
        {
            get { return _town; }
            set
            {
                _town = value;
                TownChanged(_town);
            }
        }
        public ETownState state
        {
            get { return _state; }
            set
            {
                _state = value;
                StateChanged(_state);
            }
        }
        public ETrafficLightSignal signal
        {
            get { return _signal; }
            set
            {
                _signal = value;
                SignalChanged(_signal);
            }
        }

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();

            levelActions = AServices.Get<ILevelActionsRepository>();
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
        }
        private void OnEnable()
        {
            levelActions.Add(levelLoadAction);
        }
        private void OnDisable()
        {
            levelActions.Remove(levelLoadAction);
        }
        
        public void ToggleSignal()
        {
            if (level.state == ELevelState.Play)
                signal = ZTrafficLights.GetOppositeSignal(signal);
        }

        private IEnumerator DoLoadLevel()
        {
            yield return null;
            ResetData();
        }
        private void ResetData()
        {
            _signal = ETrafficLightSignal.Red;
        }

        /// ======================================================================

        public event Action<ETownID> TownChanged = delegate { };
        public event Action<ETownState> StateChanged = delegate { };
        public event Action<ETrafficLightSignal> SignalChanged = delegate { };

        /// ======================================================================
    }
}