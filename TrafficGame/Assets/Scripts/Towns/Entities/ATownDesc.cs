﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Towns.Data
{
	public class ATownDesc : ITownDesc
	{
        /// ======================================================================

        public ETownID town { get; set; }
        public Sprite sprite { get; set; }
        public string humanName { get; set; }

        /// ======================================================================
    }
}