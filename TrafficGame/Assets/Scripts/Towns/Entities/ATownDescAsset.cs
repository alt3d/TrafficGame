﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Towns.Data
{
	public class ATownDescAsset : ScriptableObject
	{
        /// ======================================================================

        [SerializeField] protected string _humanName;
        [SerializeField] protected Sprite _sprite;

        /// ======================================================================

        public string humanName
        {
            get { return _humanName; }
        }
        public Sprite sprite
        {
            get { return _sprite; }
        }

        /// ======================================================================
    }
}