﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ITown 
	{
        /// ======================================================================

        ETownID town { get; }
        ETownState state { get; }
        ETrafficLightSignal signal { get; }

        /// ======================================================================

        event Action<ETownID> TownChanged;
        event Action<ETownState> StateChanged;
        event Action<ETrafficLightSignal> SignalChanged;

        /// ======================================================================

        void ToggleSignal();

        /// ======================================================================
    }
}