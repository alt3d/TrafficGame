﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ITownDesc 
	{
        /// ======================================================================

        ETownID town { get; }
        Sprite sprite { get;  }
        string humanName { get; }

        /// ======================================================================
    }
}