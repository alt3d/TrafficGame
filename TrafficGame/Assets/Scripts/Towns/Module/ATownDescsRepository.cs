﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Towns.Data;

namespace Alt3d.TrafficGame.Towns
{
    public class ATownDescsRepository : MonoBehaviour, ITownDescsRepository
    {
        /// ======================================================================

        private readonly ADictionary<ETownID, ATownDesc> dic = new ADictionary<ETownID, ATownDesc>();

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
        }

        public ITownDesc Get(ETownID id)
        {
            return dic.Get(id);
        }

        private IEnumerator DoLoadApp()
        {
            LoadAssets();
            yield return null;
        }
        private void LoadAssets()
        {
            var path = AResources.townDescsFolder;
            var assets = UResources.LoadAll<ATownDescAsset>(path);
            foreach (var asset in assets)
            {
                var desc = new ATownDesc();
                desc.town = UEnums.Parse<ETownID>(asset.name);
                desc.sprite = asset.sprite;
                desc.humanName = asset.humanName;
                dic.Add(desc.town, desc);
            }
        }

        /// ======================================================================
    }
}