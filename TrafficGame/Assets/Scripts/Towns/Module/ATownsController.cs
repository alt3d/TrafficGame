﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Towns
{
	public class ATownsController : MonoBehaviour
	{
        /// ======================================================================

        private ILevel level;
        private ATown town;
        private ICarsRepository cars;

        /// ======================================================================

        private void Awake()
        {
            level = AServices.Get<ILevel>();
            town = AServices.Get<ATown>();
            cars = AServices.Get<ICarsRepository>();

        }
        private void OnEnable()
        {
            level.StateChanged += OnLevelStateChanged;
            town.StateChanged += OnTownsStateChanged;

            cars.LevelCarAdded += OnCarAdded;
            cars.LevelCarRemoved += OnCarRemoved;
        }
        private void OnDisable()
        {
            level.StateChanged -= OnLevelStateChanged;
            town.StateChanged -= OnTownsStateChanged;

            cars.LevelCarAdded -= OnCarAdded;
            cars.LevelCarRemoved -= OnCarRemoved;
        }

        private void OnLevelStateChanged(ELevelState state)
        {
            switch (state)
            {
                case ELevelState.Play:
                    town.state = ETownState.Play;
                    break;
            }
        }
        private void OnTownsStateChanged(ETownState state)
        {
            switch (state)
            {
                case ETownState.Crash:
                    town.signal = ETrafficLightSignal.Yellow;
                    break;
            }
        }
       
        private void OnCarAdded(ICar car)
        {
            car.StateChanged += OnCarStateChanged;
        }
        private void OnCarRemoved(ICar car)
        {
            car.StateChanged -= OnCarStateChanged;
        }
        private void OnCarStateChanged(ICar car, ECarState state)
        {
            if (state == ECarState.Crash)
                if (town.state == ETownState.Play)
                    town.state = ETownState.Crash;
        }

        /// ======================================================================
    }
}