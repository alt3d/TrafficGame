﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.Towns
{
	public class ATownsRepository : MonoBehaviour, ITownsRepository
    {
        /// ======================================================================

        private readonly AList<ETownID> gameTowns = new AList<ETownID>();

        private IAppActionsRepository appActions;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appActions = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appActions.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appActions.Remove(appLoadAction);
        }

        public List<ETownID> GetAll()
        {
            return gameTowns.GetAll();
        }
        public ETownID GetRandom()
        {
            var all = GetAll();
            var random = UnityEngine.Random.Range(0, all.Count);
            return all[random];
        }

        private IEnumerator DoLoadApp()
        {
            SetupGameTowns();
            yield return null;
        }
        private void SetupGameTowns()
        {
            foreach (var town in ZTowns.GetAppTowns())
            {
                gameTowns.Add(town);
            }
        }

        /// ======================================================================
    }
}