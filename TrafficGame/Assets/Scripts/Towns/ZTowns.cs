﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Towns;

namespace Alt3d.TrafficGame
{
	public static class ZTowns 
	{
        /// ======================================================================

        public static List<ETownID> GetAppTowns()
        {
            return new List<ETownID>()
            {
                ETownID.City,
                ETownID.Village
            };
        }

        public static void Init(IContainer container)
        {
            var town = container.AddComponent<ATown>();
            container.Register<ATown>(town);
            container.Register<ITown>(town);

            var towns = container.AddComponent<ATownsRepository>();
            container.Register<ATownsRepository>(towns);
            container.Register<ITownsRepository>(towns);

            var descs = container.AddComponent<ATownDescsRepository>();
            container.Register<ATownDescsRepository>(descs);
            container.Register<ITownDescsRepository>(descs);

            container.AddComponent<ATownsController>();
        }

        /// ======================================================================
    }
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum ETownID
    {
        City,
        Village
    }

    public enum ETownState
    {
        Play,
        Crash
    }

    public enum EDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    /// ======================================================================
}