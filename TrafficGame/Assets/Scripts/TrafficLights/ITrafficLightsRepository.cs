﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface ITrafficLightsRepository 
	{
        /// ======================================================================

        event Action<ITrafficLight> GameLightAdded;
        event Action<ITrafficLight> GameLightRemoved;

        event Action<ITrafficLight> LevelLightAdded;
        event Action<ITrafficLight> LevelLightRemoved;

        /// ======================================================================

        ITrafficLight GetLevel(ETownID town, EDirection direction);
        List<ITrafficLight> GetAllLevel();

		/// ======================================================================
	}
}