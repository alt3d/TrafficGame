﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.TrafficLights.Lights;

namespace Alt3d.TrafficGame.TrafficLights
{
	public class ATrafficLight : MonoBehaviour, ITrafficLight
	{
        /// ======================================================================

        public ATrafficLightData data;
        public ACarDetectionArea area;

        /// ======================================================================

        public ETrafficLightSignal signal
        {
            get { return data.signal; }
        }

        /// ======================================================================

        public event Action<ITrafficLight, ETrafficLightSignal> SignalChanged = delegate { };

        /// ======================================================================

        private void OnEnable()
        {
            data.SignalChanged += OnSignalChanged;
        }
        private void OnDisable()
        {
            data.SignalChanged -= OnSignalChanged;
        }

        /// ======================================================================

        private void OnSignalChanged(ETrafficLightSignal signal)
        {
            SignalChanged(this, signal);
        }

        /// ======================================================================
    }
}