﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.TrafficLights.Lights
{
    public class ATrafficLightData : MonoBehaviour
    {
        /// ======================================================================

        [SerializeField] private ETownID _town;
        [SerializeField] private EDirection _direction;
        [SerializeField] private ETrafficLightSignal _signal;

        private ILevelActionsRepository levelActions;
        private DLevelLoadAction levelLoadAction;

        /// ======================================================================

        public ETownID town
        {
            get { return _town; }
            set
            {
                _town = value;
            }
        }
        public EDirection direction
        {
            get { return _direction; }
            set
            {
                _direction = value;
            }
        }
        public ETrafficLightSignal signal
        {
            get { return _signal; }
            set
            {
                _signal = value;
                SignalChanged(_signal);
            }
        }

        /// ======================================================================

        public event Action<ETrafficLightSignal> SignalChanged = delegate { };

        /// ======================================================================

        private void Awake()
        {
            levelActions = AServices.Get<ILevelActionsRepository>();
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
        }
        private void OnEnable()
        {
            levelActions.Add(levelLoadAction);
        }
        private void OnDisable()
        {
            levelActions.Remove(levelLoadAction);
        }

        private IEnumerator DoLoadLevel()
        {
            ResetData();
            yield return null;
        }
        private void ResetData()
        {
            signal = ETrafficLightSignal.Red;
        }

        /// ======================================================================
    }
}