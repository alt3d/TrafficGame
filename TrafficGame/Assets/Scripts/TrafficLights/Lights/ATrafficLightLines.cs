﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.TrafficLights.Lights.Views
{
	public class ATrafficLightLines : MonoBehaviour
	{
        /// ======================================================================

        public ATrafficLightData data;
        public Renderer[] renderers;
        public Material redMaterial;
        public Material greenMaterial;
        public Material yellowMaterial;
        public Material whiteMaterial;
        public float delay;

        private Coroutine cFlash;

        /// ======================================================================

        private void OnEnable()
        {
            StopFlash();
            data.SignalChanged += OnSignalChanged;
        }
        private void OnDisable()
        {
            data.SignalChanged -= OnSignalChanged;
            StopFlash();
        }

        /// ======================================================================

        private void OnSignalChanged(ETrafficLightSignal signal)
        {
            switch (signal)
            {
                case ETrafficLightSignal.Green:
                    ChangeMaterials(greenMaterial);
                    break;
                case ETrafficLightSignal.Red:
                    ChangeMaterials(redMaterial);
                    break;
                case ETrafficLightSignal.Yellow:
                    StartFlash();
                    break;
            }
        }
        private void ChangeMaterials(Material material)
        {
            foreach (var renderer in renderers)
            {
                renderer.sharedMaterial = material;
            }
        }

        private void StartFlash()
        {
            if (cFlash != null) StopCoroutine(cFlash);
            cFlash = StartCoroutine(DoFlash());
        }
        private void StopFlash()
        {
            if (cFlash != null) StopCoroutine(cFlash);
        }
        private IEnumerator DoFlash()
        {
            var yellow = true;
            while (true)
            {
                yellow = !yellow;

                if (yellow)
                    ChangeMaterials(yellowMaterial);
                else
                    ChangeMaterials(whiteMaterial);

                yield return new WaitForSeconds(delay);
            }
        }

        /// ======================================================================
    }
}