﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.TrafficLights
{
	public class ATrafficLightsController : MonoBehaviour
	{
        /// ======================================================================

        private ITown town;
        private ITrafficLightsRepository repo;
        
        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();
            repo = AServices.Get<ITrafficLightsRepository>();
        }
        private void OnEnable()
        {
            town.SignalChanged += OnTownSignalChanged;
        }
        private void OnDisable()
        {
            town.SignalChanged -= OnTownSignalChanged;
        }

        /// ======================================================================
         
        private void OnTownSignalChanged(ETrafficLightSignal signal)
        {
            ChangeAllLevelLightsSignal(signal);
        }
        private void ChangeAllLevelLightsSignal(ETrafficLightSignal signal)
        {
            foreach (var light in repo.GetAllLevel())
            {
                (light as ATrafficLight).data.signal = signal;
            }
        }

        /// ======================================================================
    }
}