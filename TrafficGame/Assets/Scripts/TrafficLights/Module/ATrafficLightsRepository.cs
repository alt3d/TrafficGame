﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.TrafficLights
{
	public class ATrafficLightsRepository : MonoBehaviour, ITrafficLightsRepository
    {
        /// ======================================================================

        private readonly AList<ATrafficLight> gameLights = new AList<ATrafficLight>();
        private readonly AList<ATrafficLight> levelLights = new AList<ATrafficLight>();

        private ITown town;
        private IAppActionsRepository appActions;
        private ILevelActionsRepository levelActions;

        private DAppLoadAction appLoadAction;
        private DLevelLoadAction levelLoadAction;
        private DLevelExitAction levelExitAction;

        /// ======================================================================

        public event Action<ITrafficLight> GameLightAdded = delegate { };
        public event Action<ITrafficLight> GameLightRemoved = delegate { };

        public event Action<ITrafficLight> LevelLightAdded = delegate { };
        public event Action<ITrafficLight> LevelLightRemoved = delegate { };

        /// ======================================================================

        private void Awake()
        {
            town = AServices.Get<ITown>();
            appActions = AServices.Get<IAppActionsRepository>();
            levelActions = AServices.Get<ILevelActionsRepository>();

            appLoadAction = new DAppLoadAction(DoLoadApp);
            levelLoadAction = new DLevelLoadAction(DoLoadLevel);
            levelExitAction = new DLevelExitAction(DoExitLevel);
        }
        private void OnEnable()
        {
            gameLights.Added += OnGameLightAdded;
            gameLights.Removed += OnGameLightRemoved;

            levelLights.Added += OnLevelLightAdded;
            levelLights.Removed += OnLevelLightRemoved;

            appActions.Add(appLoadAction);
            levelActions.Add(levelLoadAction);
            levelActions.Add(levelExitAction);
        }
        private void OnDisable()
        {
            gameLights.Added -= OnGameLightAdded;
            gameLights.Removed -= OnGameLightRemoved;

            levelLights.Added -= OnLevelLightAdded;
            levelLights.Removed -= OnLevelLightRemoved;

            appActions.Remove(appLoadAction);
            levelActions.Remove(levelLoadAction);
            levelActions.Remove(levelExitAction);
        }

        public ITrafficLight GetLevel(ETownID town, EDirection direction)
        {
            foreach (var light in levelLights.GetAll())
            {
                if (light.data.town != town) continue;
                if (light.data.direction != direction) continue;
                return light;
            }
            return null;
        }
        public List<ITrafficLight> GetAllLevel()
        {
            var result = new List<ITrafficLight>();
            foreach (var light in levelLights.GetAll())
            {
                result.Add(light);
            }
            return result;
        }
      
        private IEnumerator DoLoadLevel()
        {
            FindLevelLight();
            yield return null;
        }
        private void FindLevelLight()
        {
            foreach (var light in gameLights.GetAll())
            {
                if (light.data.town == town.town)
                    levelLights.Add(light);
            }
        }

        private IEnumerator DoExitLevel()
        {
            ClearLevelLight();
            yield return null;
        }
        public void ClearLevelLight()
        {
            var list = levelLights.GetAll();
            foreach (var light in list)
            {
                levelLights.Remove(light);
            }
        }

        private IEnumerator DoLoadApp()
        {
            FindGameLights();
            SetupGameLights();
            yield return null;
        }
        private void FindGameLights()
        {
            var parents = AScene.GetTrafficLightsParents();
            foreach (var parent in parents)
            {
                var childs = UGameObject.GetChilds(parent);
                foreach (var child in childs)
                {
                    var light = child.gameObject.GetComponent<ATrafficLight>();
                    gameLights.Add(light);
                }
            }
        }
        private void SetupGameLights()
        {
            foreach (var light in gameLights.GetAll())
            {
                var text = light.transform.parent.parent.name;
                light.data.town = UEnums.Parse<ETownID>(text);

                text = light.gameObject.name;
                light.data.direction = UEnums.Parse<EDirection>(text);
            }
        }

        /// ======================================================================

        private void OnGameLightAdded(ATrafficLight light)
        {
            GameLightAdded(light);
        }
        private void OnGameLightRemoved(ATrafficLight light)
        {
            GameLightRemoved(light);
        }

        private void OnLevelLightAdded(ATrafficLight light)
        {
            LevelLightAdded(light);
        }
        private void OnLevelLightRemoved(ATrafficLight light)
        {
            LevelLightRemoved(light);
        }

        /// ======================================================================
    }
}