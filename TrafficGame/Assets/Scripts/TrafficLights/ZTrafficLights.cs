﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.TrafficLights;

namespace Alt3d.TrafficGame
{
	public static class ZTrafficLights 
	{
        /// ======================================================================

        public static ETrafficLightSignal GetOppositeSignal(ETrafficLightSignal signal)
        {
            switch (signal)
            {
                case ETrafficLightSignal.Green: return ETrafficLightSignal.Red;
                case ETrafficLightSignal.Red: return ETrafficLightSignal.Green;
                case ETrafficLightSignal.Yellow: return ETrafficLightSignal.Yellow;
                default: throw new Exception(signal.ToString());
            }
        }

        /// ======================================================================

        public static void Init(IContainer container)
        {
            var repo = container.AddComponent<ATrafficLightsRepository>();
            container.Register<ITrafficLightsRepository>(repo);

            container.AddComponent<ATrafficLightsController>();
        }

        /// ======================================================================
    }
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum ETrafficLightSignal
    {
        Green,
        Red,
        Yellow
    }

    /// ======================================================================
}