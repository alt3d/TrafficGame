﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugControlsMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        public AButton coreInfoButton;
        public AButton statsInfoButton;
        public AButton settingsButton;

        [Space]
        public AButton fpsCounterButton;
        public AButton firstActionButton;
        public AButton secondActionButton;
        public AButton thirdActionButton;

        private IMenusRepository menus;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            menus = AServices.Get<IMenusRepository>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
            coreInfoButton.Clicked += OnButtonClicked;
            statsInfoButton.Clicked += OnButtonClicked;
            settingsButton.Clicked += OnButtonClicked;

            fpsCounterButton.Clicked += OnButtonClicked;
            firstActionButton.Clicked += OnButtonClicked;
            secondActionButton.Clicked += OnButtonClicked;
            thirdActionButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
            coreInfoButton.Clicked -= OnButtonClicked;
            statsInfoButton.Clicked -= OnButtonClicked;
            settingsButton.Clicked -= OnButtonClicked;

            fpsCounterButton.Clicked -= OnButtonClicked;
            firstActionButton.Clicked -= OnButtonClicked;
            secondActionButton.Clicked -= OnButtonClicked;
            thirdActionButton.Clicked -= OnButtonClicked;
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);

            if (button == closeButton)
            {
                menus.Get(EMenuID.DebugGod).Open();
                Close();
            }

            if (button == coreInfoButton)
                menus.Get(EMenuID.DebugCoreInfo).Toggle();

            if (button == statsInfoButton)
                menus.Get(EMenuID.DebugStatsInfo).Toggle();

            if (button == settingsButton)
                menus.Get(EMenuID.DebugSettings).Toggle();

            if (button == fpsCounterButton)
                menus.Get(EMenuID.DebugFpsCounter).Toggle();

            if (button == firstActionButton)
            {
                var stream = AServices.Get<IStream>();
                stream.EraseAll();
            }

            if (button == secondActionButton)
            {
                
            }

            if (button == thirdActionButton)
            {
                
            }
        }

        /// ======================================================================
    }
}