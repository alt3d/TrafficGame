﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugCoreInfoMenu : AMenu
    {
        /// ======================================================================

        public ALabel infoLabel;

        private IApp app;
        private IGame game;
        private ILevel level;
        private IUI ui;
        private ITown town;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            app = AServices.Get<IApp>();
            game = AServices.Get<IGame>();
            level = AServices.Get<ILevel>();
            ui = AServices.Get<IUI>();
            town = AServices.Get<ITown>();
        }
        private void Update()
        {
            if (isClosed) return;

            var text = string.Empty;

            text = UText.AddLine(text, GetProductInfo());
            text = UText.AddLine(text, GetAppInfo());
            text = UText.AddLine(text, GetGameInfo());
            text = UText.AddLine(text, GetLevelInfo());
            text = UText.AddLine(text, GetUiInfo());
            text = UText.AddLine(text, GetTownInfo());

            infoLabel.text.text = text;
        }

        /// ======================================================================

        private string GetProductInfo()
        {
            var result = "Product:";
            result = UText.AddLine(result, "\tVersion: " + AProduct.version);
            return result;
        }
        private string GetAppInfo()
        {
            var result = "App:";
            result = UText.AddLine(result, "\tState: " + app.state);
            result = UText.AddLine(result, "\tFocus: " + app.focus);
            return result;
        }
        private string GetGameInfo()
        {
            var result = "Game:";
            result = UText.AddLine(result, "\tState: " + game.state);
            return result;
        }
        private string GetLevelInfo()
        {
            var result = "Level:";
            result = UText.AddLine(result, "\tState: " + level.state);
            result = UText.AddLine(result, "\tPause: " + level.pause);
            return result;
        }
        private string GetUiInfo()
        {
            var result = "UI:";
            result = UText.AddLine(result, "\tScreen: " + ui.screen);
            return result;
        }
        private string GetTownInfo()
        {
            var result = "Town:";
            result = UText.AddLine(result, "\tTown: " + town.town);
            result = UText.AddLine(result, "\tState: " + town.state);
            return result;
        }

        /// ======================================================================
    }
}