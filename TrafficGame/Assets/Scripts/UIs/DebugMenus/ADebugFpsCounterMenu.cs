﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.Misc;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugFpsCounterMenu : AMenu
	{
        /// ======================================================================

        public ALabel fpsLabel;
        private AFpsCounter counter;

        /// ======================================================================

        private void Update()
        {
            if (isClosed) return;

            var fps = Mathf.RoundToInt(1f / Time.unscaledDeltaTime);
            counter.Add(fps);

            fpsLabel.text.text = "" + counter.average;
        }

        /// ======================================================================

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            counter = new AFpsCounter();
        }

        /// ======================================================================
    }
}