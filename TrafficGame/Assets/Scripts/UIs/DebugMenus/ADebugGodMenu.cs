﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugGodMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        private IMenusRepository menus;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            menus = AServices.Get<IMenusRepository>();

            if (AProduct.isDebug)
                Open();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);

            if (button == closeButton)
            {
                menus.Get(EMenuID.DebugControls).Open();
                Close();
            }
        }

        /// ======================================================================
    }
}