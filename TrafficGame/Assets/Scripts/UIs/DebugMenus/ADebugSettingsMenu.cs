﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugSettingsMenu : AMenu
	{
        /// ======================================================================

        public AButton pauseButton;

        [Space]
        public ASlider antialiasingSlider;
        public ALabel antializingText;

        [Space]
        public ASlider shadowsDistanceSlider;
        public ALabel shadowsDistanceText;

        [Space]
        public ASlider shadowsQualitySlider;
        public ALabel shadowsQualityText;

        private Light sun;

        /// ======================================================================

        private void Start()
        {
            sun = UGameObject.FindGo("SCENE/Sun").GetComponent<Light>();
        }
        private void OnEnable()
        {
            pauseButton.Clicked += OnButtonClicked;

            antialiasingSlider.ValueChanged += OnSliderValueChanged;
            shadowsDistanceSlider.ValueChanged += OnSliderValueChanged;
            shadowsQualitySlider.ValueChanged += OnSliderValueChanged;
        }
        private void OnDisable()
        {
            pauseButton.Clicked -= OnButtonClicked;

            antialiasingSlider.ValueChanged -= OnSliderValueChanged;
            shadowsDistanceSlider.ValueChanged -= OnSliderValueChanged;
            shadowsQualitySlider.ValueChanged -= OnSliderValueChanged;
        }

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();

            antialiasingSlider.slider.value = GetAntialiasingSliderValue(QualitySettings.antiAliasing);
            OnSliderValueChanged(antialiasingSlider, GetAntialiasingSliderValue(QualitySettings.antiAliasing));

            shadowsDistanceSlider.slider.value = QualitySettings.shadowDistance;
            OnSliderValueChanged(shadowsDistanceSlider, QualitySettings.shadowDistance);

            shadowsQualitySlider.slider.value = GetShadowQuality(QualitySettings.shadows);
            OnSliderValueChanged(shadowsQualitySlider, GetShadowQuality(QualitySettings.shadows));
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);

            if (button == pauseButton)
            {
                if (Time.timeScale > 0.1f)
                    Time.timeScale = 0f;
                else
                    Time.timeScale = 1f;
            }
        }
        protected override void OnSliderValueChanged(ASlider slider, float value)
        {
            base.OnSliderValueChanged(slider, value);

            if (slider == antialiasingSlider)
            {
                var ff = Mathf.Pow(2, value);
                var ii = (int)ff;
                QualitySettings.antiAliasing = ii;
                antializingText.text.text = "Antialiasing: " + ii;
            }

            if (slider == shadowsDistanceSlider)
            {
                QualitySettings.shadowDistance = value;
                shadowsDistanceText.text.text = "Shadows distance: " + (int)value;
            }

            if (slider == shadowsQualitySlider)
            {
                var ii = (int)value;
                var qq = GetShadowQuality(ii);
                QualitySettings.shadows = qq;
                shadowsQualityText.text.text = "Shadows quality: " + qq;

                if (sun != null)
                {
                    sun.shadows = GetLightShadows(qq);
                }
            }
        }

        private int GetAntialiasingSliderValue(int aa)
        {
            if (aa == 0) return 0;
            if (aa == 2) return 1;
            if (aa == 4) return 2;
            if (aa == 8) return 3;
            return 0;
        }
        private ShadowQuality GetShadowQuality(int vv)
        {
            if (vv == 0) return ShadowQuality.Disable;
            if (vv == 1) return ShadowQuality.HardOnly;
            if (vv == 2) return ShadowQuality.All;
            return ShadowQuality.All;
        }
        private int GetShadowQuality(ShadowQuality q)
        {
            if (q == ShadowQuality.Disable) return 0;
            if (q == ShadowQuality.HardOnly) return 1;
            if (q == ShadowQuality.All) return 2;
            return 2;
        }

        private LightShadows GetLightShadows(ShadowQuality ss)
        {
            if (ss == ShadowQuality.All) return LightShadows.Soft;
            if (ss == ShadowQuality.HardOnly) return LightShadows.Hard;
            if (ss == ShadowQuality.Disable) return LightShadows.None;
            return LightShadows.Soft;
        }

        /// ======================================================================
    }
}