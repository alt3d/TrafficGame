﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ADebugStatsInfoMenu : AMenu
	{
        /// ======================================================================

        public ALabel infoLabel;

        private IStats stats;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            stats = AServices.Get<IStats>();
        }
        private void Update()
        {
            if (isClosed) return;

            var text = string.Empty;

            text = UText.AddLine(text, "\tPassed cars count: " + stats.passedCarsCount);
            text = UText.AddLine(text, "\tCrashed cars count: " + stats.crashedCarsCount);

            infoLabel.text.text = text;
        }

        /// ======================================================================
    }
}