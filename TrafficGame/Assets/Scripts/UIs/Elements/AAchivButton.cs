﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d.TrafficGame.UIs.Elements
{
	public class AAchivButton : AButton
	{
        /// ======================================================================

        [SerializeField] protected ALabel titleLabel;
        [SerializeField] protected ALabel descriptionLabel;
        [SerializeField] protected Image starImage;

        [Space]
        [SerializeField] protected Sprite normalStar;
        [SerializeField] protected Sprite unlockedStar;

        /// ======================================================================

        public IAchiv achiv { get; set; }

        /// ======================================================================

        public void Redraw()
        {
            titleLabel.text.text = achiv.desc.title;
            descriptionLabel.text.text = achiv.desc.description;

            if (achiv.isUnlocked)
                starImage.sprite = unlockedStar;
            else
                starImage.sprite = normalStar;
        }

        /// ======================================================================
    }
}