﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Alt3d.TrafficGame.UIs.Elements
{
	public class ATownButton : AButton
	{
        /// ======================================================================

        [SerializeField] protected Image image;
        [SerializeField] protected ALabel nameLabel;

        /// ======================================================================

        public ITownDesc desc { get; set; }

        /// ======================================================================

        public void Redraw()
        {
            image.sprite = desc.sprite;
            nameLabel.text.text = desc.humanName;
        }

        /// ======================================================================
    }
}