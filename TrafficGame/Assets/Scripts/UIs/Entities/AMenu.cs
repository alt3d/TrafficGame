﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public abstract class AMenu : MonoBehaviour, IMenu
    { 
        /// ======================================================================

        private GameObject _content;
        private EMenuState _state;

        /// ======================================================================

        public GameObject content
        {
            get
            {
                if (_content == null) _content = UGameObject.GetChilds(transform)[0].gameObject;
                return _content;
            }
        }
        public EMenuState state
        {
            get { return _state; }
            set
            {
                _state = value;
                OnStateChanged(_state);
            }
        }
        public bool isOpened
        {
            get { return state == EMenuState.Open; }
        }
        public bool isClosed
        {
            get { return state == EMenuState.Close; }
        }

        /// ======================================================================

        public event Action<AMenu, EMenuState> stateChanged = delegate { };
        public event Action<AMenu> opened = delegate { };
        public event Action<AMenu> closed = delegate { };

        /// ======================================================================

        protected virtual void Awake()
        {
            _state = EMenuState.Close;
            content.SetActive(false);
        }

        public void Open()
        {
            state = EMenuState.Open;
        }
        public void Close()
        {
            state = EMenuState.Close;
        }
        public void Toggle()
        {
            switch (state)
            {
                case EMenuState.Close:
                    Open();
                    break;
                case EMenuState.Open:
                    Close();
                    break;
                default:
                    throw new ArgumentException(state.ToString());
            }
        }

        private void OnStateChanged(EMenuState state)
        {
            switch (state)
            {
                case EMenuState.Open:
                    OnBeforeOpenActions();
                    transform.SetAsLastSibling();
                    content.SetActive(true);
                    OnAfterOpenActions();
                    opened(this);
                    break;
                case EMenuState.Close:
                    OnBeforeCloseActions();
                    content.SetActive(false);
                    OnAfterCloseActions();
                    closed(this);
                    break;
                default:
                    throw new ArgumentException(_state.ToString());
            }
            stateChanged(this, state);
        }
        protected virtual void OnBeforeOpenActions()
        {

        }
        protected virtual void OnAfterOpenActions()
        {

        }
        protected virtual void OnBeforeCloseActions()
        {

        }
        protected virtual void OnAfterCloseActions()
        {

        }

        /// ======================================================================

        protected virtual void OnButtonClicked(AButton button)
        {

        }
        protected virtual void OnSliderValueChanged(ASlider slider, float value)
        {

        }
        protected virtual void OnToggleValueChanged(AToggle toggle, bool value)
        {

        }
        protected virtual void OnFieldValueChanged(AField field, string value)
        {

        }
        protected virtual void OnFieldEndEdit(AField field, string value)
        {

        }
        protected virtual void OnDropdownValueChanged(ADropdown dropdown, int value)
        {

        }

        /// ======================================================================
    }
}