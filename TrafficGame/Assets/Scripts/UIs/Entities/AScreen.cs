﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs
{
	public class AScreen 
	{
        /// ======================================================================

        public EScreen id { get; private set; }
        public EMenuID[] menus { get; private set; }

        /// ======================================================================

        public AScreen(EScreen id, EMenuID[] menus)
        {
            this.id = id;
            this.menus = menus;
        }

        /// ======================================================================
    }
}