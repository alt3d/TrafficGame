﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Alt3d.TrafficGame.UIs
{
	public class AUI : MonoBehaviour, IUI
	{
        /// ======================================================================

        private EScreen _screen = EScreen.AppLaunch;
        private EventSystem eventSystem;

        /// ======================================================================

        public EScreen screen
        {
            get { return _screen; }
            set
            {
                _screen = value;
                ScreenChanged(_screen);
            }
        }

        /// ======================================================================

        public event Action<EScreen> ScreenChanged = delegate { };
        public event Action ButtonClicked = delegate { };
 
        /// ======================================================================

        private void Awake()
        {
            eventSystem = EventSystem.current;
        }

        public bool IsPointerOverUI()
        {
            var eventData = new PointerEventData(eventSystem);
            eventData.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            var castResults = new List<RaycastResult>();
            eventSystem.RaycastAll(eventData, castResults);

            return castResults.Count > 0;
        }
        public void RiseButtonClicked()
        {
            ButtonClicked();
        }

        /// ======================================================================
    }
}