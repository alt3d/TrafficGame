﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class AAchivUnlockMenu : AMenu
	{
        /// ======================================================================

        public ALabel titleLabel;
        [SerializeField] protected ALabel descLabel;
        [SerializeField] protected GameObject parent;

        private ILevel level;
        private IAchivsEvents achivsEvents;

        private Coroutine cShow;
        private const float showDuration = 5f;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();

            level = AServices.Get<ILevel>();
            achivsEvents = AServices.Get<IAchivsEvents>();
            parent.SetActive(false);
        }
        private void OnEnable()
        {
            achivsEvents.AchivUnlocked += OnAchivUnlocked;
            level.StateChanged += OnLevelStateChanged;
        }
        private void OnDisable()
        {
            achivsEvents.AchivUnlocked -= OnAchivUnlocked;
            level.StateChanged -= OnLevelStateChanged;
        }

        private void OnAchivUnlocked(IAchiv achiv)
        {
            titleLabel.text.text = achiv.desc.title;
            descLabel.text.text = achiv.desc.description;

            if (cShow != null) StopCoroutine(cShow);
            cShow = StartCoroutine(DoShow());
        }
        private IEnumerator DoShow()
        {
            yield return null;
            //Open();
            parent.SetActive(true);

            var time = showDuration;
            while (time > 0)
            {
                time -= Time.deltaTime;
                yield return null;
            }

            parent.SetActive(false);
            //Close();
        }

        private void OnLevelStateChanged(ELevelState state)
        {
            if (state == ELevelState.Stop)
                parent.SetActive(false);
        }

        /// ======================================================================
    }
}