﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alt3d.TrafficGame.UIs.Elements;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class AAchivsShowcaseMenu : AMenu
    {
        /// ======================================================================

        public ALabel titleLabel;
        public AButton closeButton;
        public Transform gridParent;
        public ScrollRect scrollRect;
        public GameObject achivButtonPrefab;

        private readonly List<AAchivButton> achivButtons = new List<AAchivButton>();
        private IAchivsRepository achivs;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            achivs = AServices.Get<IAchivsRepository>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
        }

        /// ======================================================================

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            DeleteExistsButtons();
            AddAchivButtons();
            RedrawAchivButtons();
            RedrawTitle();
        }
        private void DeleteExistsButtons()
        {
            foreach (var item in achivButtons)
            {
                item.Clicked -= OnButtonClicked;
            }

            achivButtons.Clear();
            UGameObject.DeleteChilds(gridParent);
        }
        private void AddAchivButtons()
        {
            var all = ZAchivs.GetAppAchivs();
            foreach (var id in all)
            {
                var go = UGameObject.Instantiate(achivButtonPrefab);
                go.transform.SetParent(gridParent, false);

                var button = go.GetComponent<AAchivButton>();
                button.achiv = achivs.Get(id);

                button.Clicked += OnButtonClicked;
                achivButtons.Add(button);
            }
        }
        private void RedrawAchivButtons()
        {
            foreach (var button in achivButtons)
            {
                button.Redraw();
            }

            scrollRect.normalizedPosition = Vector2.one;
        }

        private void RedrawTitle()
        {
            var all = ZAchivs.GetAppAchivs();
            var unlocked = 0;
            foreach (var id in all) 
            {
                var achiv = achivs.Get(id);
                if (achiv.isUnlocked)
                    unlocked++;
            }

            titleLabel.text.text = "Achievements (" + unlocked + "/" + all.Count + ")";
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);

            if (button == closeButton)
                Close();
        }

        /// ======================================================================
    }
}