﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class AAppLaunchMenu : AMenu
	{
        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            Open();
        }

        /// ======================================================================
    }
}