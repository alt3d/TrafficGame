﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class AGameMain : AMenu
	{
        /// ======================================================================

        public AButton playButton;
        public AButton settingsButton;
        public AButton achivsButton;

        private AUI ui;
        private IMenusRepository menus;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            ui = AServices.Get<AUI>();
            menus = AServices.Get<IMenusRepository>();
        }
        private void OnEnable()
        {
            playButton.Clicked += OnButtonClicked;
            settingsButton.Clicked += OnButtonClicked;
            achivsButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            playButton.Clicked -= OnButtonClicked;
            settingsButton.Clicked -= OnButtonClicked;
            achivsButton.Clicked -= OnButtonClicked;
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == playButton)
                menus.Get(EMenuID.LevelSelection).Open();

            if (button == settingsButton)
                menus.Get(EMenuID.GameSettings).Open();

            if (button == achivsButton)
                menus.Get(EMenuID.AchivsShowcase).Open();
        }

        /// ======================================================================
    }
}