﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
    public class AGameSettingsMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        public AButtonWithText shadowsButton;
        public AButtonWithText masterVolumeButton;
        public AButton masterVolumePlusButton;
        public AButton masterVolumeMinusButton;

        private AUI ui;
        private IGameSettings gameSettings;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            ui = AServices.Get<AUI>();
            gameSettings = AServices.Get<IGameSettings>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
            shadowsButton.Clicked += OnButtonClicked;
            masterVolumeButton.Clicked += OnButtonClicked;
            masterVolumePlusButton.Clicked += OnButtonClicked;
            masterVolumeMinusButton.Clicked += OnButtonClicked;

            gameSettings.ShadowsActivityChanged += OnShadowsActivityChanged;
            gameSettings.MasterVolumeChanged += OnMasterVolumeChanged;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
            shadowsButton.Clicked -= OnButtonClicked;
            masterVolumeButton.Clicked -= OnButtonClicked;
            masterVolumePlusButton.Clicked -= OnButtonClicked;
            masterVolumeMinusButton.Clicked -= OnButtonClicked;

            gameSettings.ShadowsActivityChanged -= OnShadowsActivityChanged;
            gameSettings.MasterVolumeChanged -= OnMasterVolumeChanged;
        }

        private void OnShadowsActivityChanged(bool activity)
        {
            RedrawShadowsButton();
        }
        private void RedrawShadowsButton()
        {
            var text = "Shadows: Off";
            if (gameSettings.shadowsActivity) text = "Shadows: On";
            shadowsButton.text.text = text;
        }

        private void OnMasterVolumeChanged(float volume)
        {
            RedrawMasterVolumeButton();
        }
        private void RedrawMasterVolumeButton()
        {
            var text = "Volume: ";
            var volume = gameSettings.masterVolume * 100f;
            volume = Mathf.Clamp(volume, 0, 100);
            text += Mathf.RoundToInt(volume);
            masterVolumeButton.text.text = text;
        }

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            RedrawShadowsButton();
            RedrawMasterVolumeButton();
        }
        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == closeButton)
                Close();

            if (button == shadowsButton)
                gameSettings.ToggleShadowsActivity();

            if (button == masterVolumeButton)
            {
                var volume = gameSettings.masterVolume;
                if (volume >= 0.5f) gameSettings.ChangeMasterVolume(0f);
                else gameSettings.ChangeMasterVolume(1f);
            }

            if (button == masterVolumePlusButton)
            {
                var volume = gameSettings.masterVolume + 0.2f;
                gameSettings.ChangeMasterVolume(volume);
            }

            if (button == masterVolumeMinusButton)
            {
                var volume = gameSettings.masterVolume - 0.2f;
                gameSettings.ChangeMasterVolume(volume);
            }
        }

        /// ======================================================================
    }
}