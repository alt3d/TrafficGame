﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ALevelCrashMenu : AMenu
    {
        /// ======================================================================

        public AButton doneButton;

        private IMain main;
        private AUI ui;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();

            ui = AServices.Get<AUI>();
            main = AServices.Get<IMain>();
        }
        private void OnEnable()
        {
            doneButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            doneButton.Clicked -= OnButtonClicked;
        }

        /// ======================================================================

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == doneButton)
                main.QuitLevel();
        }

        /// ======================================================================
    }
}