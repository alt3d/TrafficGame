﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ALevelMainMenu : AMenu
    {
        /// ======================================================================

        public AButton settingsButton;

        private AUI ui;
        private ILevel level;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            ui = AServices.Get<AUI>();
            level = AServices.Get<ILevel>();
        }
        private void OnEnable()
        {
            settingsButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            settingsButton.Clicked -= OnButtonClicked;
        }

        /// ======================================================================

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == settingsButton)
                level.SetPause();
        }

        /// ======================================================================
    }
}