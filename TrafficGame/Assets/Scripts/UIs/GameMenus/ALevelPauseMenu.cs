﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ALevelPauseMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        public AButton resumeButton;
        public AButton settingsButton;
        public AButton menuButton;

        private ILevel level;
        private IMain main;
        private AUI ui;
        private AMenusRepository menus;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();

            level = AServices.Get<ILevel>();
            main = AServices.Get<IMain>();

            ui = AServices.Get<AUI>();
            menus = AServices.Get<AMenusRepository>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
            resumeButton.Clicked += OnButtonClicked;
            settingsButton.Clicked += OnButtonClicked;
            menuButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
            resumeButton.Clicked -= OnButtonClicked;
            settingsButton.Clicked -= OnButtonClicked;
            menuButton.Clicked -= OnButtonClicked;
        }

        /// ======================================================================

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == closeButton)
                level.UnsetPause();

            if (button == resumeButton)
                level.UnsetPause();

            if (button == settingsButton)
                menus.Get(EMenuID.GameSettings).Open();

            if (button == menuButton)
                main.QuitLevel();
        }

        /// ======================================================================
    }
}