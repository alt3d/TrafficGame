﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ALevelResultMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        public ALabel countLabel;
        public ALabel rewardLabel;
        public ALabel totalLabel;

        private AUI ui;
        private IStats stats;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            ui = AServices.Get<AUI>();
            stats = AServices.Get<IStats>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
        }

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            RedrawCount();
            RedrawReward();
            RedrawTotal();
        }
        private void RedrawCount()
        {
            var text = "Passed cars: " + stats.passedCarsCount;
            text = UText.AddLine(text, "Crashed cars: " + stats.crashedCarsCount);
            text = UText.AddLine(text, "Broken props: " + stats.brokenPropsCount);

            if (stats.unlockedAchivsCount > 0 )
                text = UText.AddLine(text, "Achievements: " + stats.unlockedAchivsCount);

            countLabel.text.text = text;
        }
        private void RedrawReward()
        {
            var text = "Reward: " + stats.passedCarsReward;
            text = UText.AddLine(text, "Damage: " + stats.crashedCarsReward);
            text = UText.AddLine(text, "Damage: " + stats.brokenPropsReward);

            if (stats.unlockedAchivsReward > 0)
                text = UText.AddLine(text, "Reward: " + stats.unlockedAchivsReward);

            rewardLabel.text.text = text;
        }
        private void RedrawTotal()
        {
            var reward =
                stats.passedCarsReward +
                stats.crashedCarsReward +
                stats.brokenPropsReward +
                stats.unlockedAchivsReward;

            var text = "Total: " + reward;
            if (stats.unlockedAchivsReward > 0)
                text = " \n" + text;

            totalLabel.text.text = text;
        }

        /// ======================================================================

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);
            ui.RiseButtonClicked();

            if (button == closeButton)
                Close();
        }

        /// ======================================================================
    }
}