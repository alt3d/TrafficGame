﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alt3d.TrafficGame.UIs.Elements;

namespace Alt3d.TrafficGame.UIs.Menus
{
    public class ALevelSelectionMenu : AMenu
    {
        /// ======================================================================

        public AButton closeButton;
        public Transform gridParent;
        public ScrollRect scrollRect;

        [Space]
        public GameObject randomTownButtonPrefab;
        public GameObject townButtonPrefab;

        private AButton randomTownButton;
        private readonly List<ATownButton> townButtons = new List<ATownButton>();

        private AUI ui;
        private IMain main { get; set; }
        private ITownsRepository towns { get; set; }
        private ITownDescsRepository townDescs { get; set; }

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();

            ui = AServices.Get<AUI>();
            main = AServices.Get<IMain>();
            towns = AServices.Get<ITownsRepository>();
            townDescs = AServices.Get<ITownDescsRepository>();
        }
        private void OnEnable()
        {
            closeButton.Clicked += OnButtonClicked;
        }
        private void OnDisable()
        {
            closeButton.Clicked -= OnButtonClicked;
        }

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            DeleteExistsButtons();
            AddRandomTownButton();
            AddTownButtons();
            RedrawTownButtons();
        }
        private void DeleteExistsButtons()
        {
            if (randomTownButton != null)
                randomTownButton.Clicked -= OnButtonClicked;

            foreach (var item in townButtons)
            {
                item.Clicked -= OnButtonClicked;
            }

            townButtons.Clear();
            UGameObject.DeleteChilds(gridParent);
        }
        private void AddRandomTownButton()
        {
            var go = UGameObject.Instantiate(randomTownButtonPrefab);
            go.transform.SetParent(gridParent, false);

            randomTownButton = go.GetComponent<AButton>();
            randomTownButton.Clicked += OnButtonClicked;
        }
        private void AddTownButtons()
        {
            var townsList = towns.GetAll();
            foreach (var town in townsList)
            {
                var go = UGameObject.Instantiate(townButtonPrefab);
                go.transform.SetParent(gridParent, false);

                var button = go.GetComponent<ATownButton>();
                button.desc = townDescs.Get(town);

                button.Clicked += OnButtonClicked;
                townButtons.Add(button);
            }
        }
        private void RedrawTownButtons()
        {
            foreach (var button in townButtons)
            {
                button.Redraw();
            }
        }

        protected override void OnButtonClicked(AButton button)
        {
            base.OnButtonClicked(button);

            if ((button is ATownButton) == false)
            {
                ui.RiseButtonClicked();
            }

            if (button == closeButton)
                ui.screen = EScreen.GameMain;

            if (button == randomTownButton)
            {
                var town = towns.GetRandom(); 
                main.LaunchLevel(town);
            }
               
            if (button is ATownButton)
            {
                var townButton = (ATownButton)button;
                if (townButtons.Contains(townButton))
                {
                    foreach (var item in townButtons)
                    {
                        if (item == button)
                            main.LaunchLevel(townButton.desc.town);
                    }
                }

            }
        }

        /// ======================================================================
    }
}