﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ALevelStatsMenu : AMenu
	{
        /// ======================================================================

        public ALabel rewardLabel;
        private IStats stats;

        /// ======================================================================

        protected override void Awake()
        {
            base.Awake();
            stats = AServices.Get<IStats>();
        }
        private void OnEnable()
        {
            stats.PassedCarsRewardChanged += OnPassedCarsRewardChanged;
            stats.CrashedCarsRewardChanged += OnCrashedCarsRewardChanged;
            stats.BrokenPropsRewardChanged += OnBrokenPropsRewardChanged;
            stats.UnlockedAchivsRewardChanged += OnUnlockedAchivsRewardChanged;
        }
        private void OnDisable()
        {
            stats.PassedCarsRewardChanged -= OnPassedCarsRewardChanged;
            stats.CrashedCarsRewardChanged -= OnCrashedCarsRewardChanged;
            stats.BrokenPropsRewardChanged -= OnBrokenPropsRewardChanged;
            stats.UnlockedAchivsRewardChanged -= OnUnlockedAchivsRewardChanged;
        }

        protected override void OnBeforeOpenActions()
        {
            base.OnBeforeOpenActions();
            RedrawRewardLabel();
        }

        private void OnPassedCarsRewardChanged(int reward)
        {
            RedrawRewardLabel();
        }
        private void OnCrashedCarsRewardChanged(int reward)
        {
            RedrawRewardLabel();
        }
        private void OnBrokenPropsRewardChanged(int reward)
        {
            RedrawRewardLabel();
        }
        private void OnUnlockedAchivsRewardChanged(int reward)
        {
            RedrawRewardLabel();
        }

        private void RedrawRewardLabel()
        {
            var reward = 
                stats.passedCarsReward +
                stats.crashedCarsReward +
                stats.unlockedAchivsReward;

            rewardLabel.text.text = reward.ToString();
        }

        /// ======================================================================
    }
}