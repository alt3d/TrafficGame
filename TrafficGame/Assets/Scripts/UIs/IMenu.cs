﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IMenu 
	{
        /// ======================================================================

        bool isOpened { get; }
        bool isClosed { get; }

        /// ======================================================================

        void Open();
        void Close();
        void Toggle();

		/// ======================================================================
	}
}