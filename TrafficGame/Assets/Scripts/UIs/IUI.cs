﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public interface IUI 
	{
        /// ======================================================================

        EScreen screen { get; }

        /// ======================================================================

        event Action<EScreen> ScreenChanged;
        event Action ButtonClicked;

        /// ======================================================================

        bool IsPointerOverUI();

        /// ======================================================================
    }
}