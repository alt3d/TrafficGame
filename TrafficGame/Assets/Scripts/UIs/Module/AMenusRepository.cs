﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame
{
	public class AMenusRepository : MonoBehaviour, IMenusRepository
    {
        /// ======================================================================

        private readonly Dictionary<EMenuID, AMenu> gameMenus = new Dictionary<EMenuID, AMenu>();
        private readonly Dictionary<EMenuID, AMenu> debugsMenus = new Dictionary<EMenuID, AMenu>();

        private IAppActionsRepository appLauncher;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appLauncher = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appLauncher.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appLauncher.Remove(appLoadAction);
        }

        /// ======================================================================

        public List<AMenu> GetAllGames()
        {
            return new List<AMenu>(gameMenus.Values);
        }
        public List<AMenu> Get(EMenuID[] array)
        {
            var result = new List<AMenu>();
            foreach (var id in array)
            {
                var menu = Get(id);
                result.Add(menu as AMenu);
            }
            return result;
        }
        public IMenu Get(EMenuID id)
        {
            if (gameMenus.ContainsKey(id))
                return gameMenus[id];

            if (debugsMenus.ContainsKey(id))
                return debugsMenus[id];

            throw new Exception("Not menu in collection: " + id.ToString());
        }

        private IEnumerator DoLoadApp()
        {
            SetupDics();
            yield return null;
        }
        private void SetupDics()
        {
            var array = AScene.GetScreenMenus();
            foreach (var go in array)
            {
                var menu = go.gameObject.GetComponent<AMenu>();
                var id = UEnums.Parse<EMenuID>(go.name);
                gameMenus.Add(id, menu);
            }

            array = AScene.GetDebugMenus();
            foreach (var go in array)
            {
                var menu = go.gameObject.GetComponent<AMenu>();
                var id = UEnums.Parse<EMenuID>(go.name);
                debugsMenus.Add(id, menu);
            }
        }

        /// ======================================================================
    }
}