﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.UIs;

namespace Alt3d.TrafficGame
{
    public class AScreensRepository : MonoBehaviour
    {
        /// ======================================================================

        private readonly Dictionary<EScreen, AScreen> dic = new Dictionary<EScreen, AScreen>();

        private IAppActionsRepository appLauncher;
        private DAppLoadAction appLoadAction;

        /// ======================================================================

        private void Awake()
        {
            appLauncher = AServices.Get<IAppActionsRepository>();
            appLoadAction = new DAppLoadAction(DoLoadApp);
        }
        private void OnEnable()
        {
            appLauncher.Add(appLoadAction);
        }
        private void OnDisable()
        {
            appLauncher.Remove(appLoadAction);
        }

        public AScreen GetScreen(EScreen id)
        {
            if (dic.ContainsKey(id)) return dic[id];
            throw new Exception("No screen in dic: " + id.ToString());
        }
        public EMenuID[] GetScreenMenus(EScreen id)
        {
            if (dic.ContainsKey(id)) return dic[id].menus;
            throw new Exception("No screen in dic: " + id.ToString());
        }

        private IEnumerator DoLoadApp()
        {
            SetupDic();
            yield return null;
        }
        private void SetupDic()
        {
            AddScreen(EScreen.AppLaunch, new EMenuID[] { EMenuID.AppLaunch });
            AddScreen(EScreen.AppQuit, new EMenuID[] { EMenuID.AppLaunch });

            AddScreen(EScreen.GameLaunch, new EMenuID[] { EMenuID.GameLaunch });
            AddScreen(EScreen.GameQuit, new EMenuID[] { EMenuID.GameLaunch });
            AddScreen(EScreen.GameMain, new EMenuID[] { EMenuID.GameMain });

            AddScreen(EScreen.LevelResult, new EMenuID[] { EMenuID.LevelSelection, EMenuID.LevelResult });
            AddScreen(EScreen.LevelCrash, new EMenuID[] { EMenuID.LevelStats, EMenuID.LevelCrash, });

            AddScreen(EScreen.LevelLaunch, new EMenuID[] { EMenuID.LevelLaunch });
            AddScreen(EScreen.LevelQuit, new EMenuID[] { EMenuID.LevelLaunch });
            AddScreen(EScreen.LevelPlay, new EMenuID[] { EMenuID.LevelStats, EMenuID.LevelMain });

            AddScreen(EScreen.AdsShow, new EMenuID[] { EMenuID.AdsBack });
        }
        private void AddScreen(EScreen id, EMenuID[] menus)
        {
            if (dic.ContainsKey(id)) throw new Exception(id.ToString());

            var screen = new AScreen(id, menus);
            dic.Add(id, screen);
        }

        /// ======================================================================
    }
}