﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs
{
	public class AUIsController : MonoBehaviour
	{
        /// ======================================================================

        private IApp app;
        private IGame game;
        private ILevel level;
        private ITown town;

        private AUI ui;
        private AMenusRepository menus;
        private AScreensRepository screens;

        /// ======================================================================

        private void Awake()
        {
            app = AServices.Get<IApp>();
            game = AServices.Get<IGame>();
            level = AServices.Get<ILevel>();
            town = AServices.Get<ITown>();

            ui = AServices.Get<AUI>();
            menus = AServices.Get<AMenusRepository>();
            screens = AServices.Get<AScreensRepository>();
        }
        private void OnEnable()
        {
            app.StateChanged += OnAppStateChanged;
            game.StateChanged += OnGameStateChanged;
            town.StateChanged += OnTownStateChanged;
            level.PauseChanged += OnLevelPauseChanged;
            ui.ScreenChanged += OnUIScreenChanged;
        }
        private void OnDisable()
        {
            app.StateChanged -= OnAppStateChanged;
            game.StateChanged -= OnGameStateChanged;
            level.PauseChanged -= OnLevelPauseChanged;
            town.StateChanged -= OnTownStateChanged;
            ui.ScreenChanged -= OnUIScreenChanged;
        }

        private void OnAppStateChanged(EAppState state)
        {
            switch (state)
            {
                case EAppState.Quit:
                    ui.screen = EScreen.AppQuit;
                    break;
            }
        }
        private void OnGameStateChanged(EGameState state)
        {
            switch (state)
            {
                case EGameState.Launch:
                    ui.screen = EScreen.GameLaunch;
                    break;
                case EGameState.Quit:
                    ui.screen = EScreen.GameQuit;
                    break;
                case EGameState.Menu:
                    ui.screen = EScreen.GameMain;
                    break;
                case EGameState.LevelLaunch:
                    ui.screen = EScreen.LevelLaunch;
                    break;
                case EGameState.LevelQuit:
                    ui.screen = EScreen.LevelQuit;
                    break;
                case EGameState.LevelPlay:
                    ui.screen = EScreen.LevelPlay;
                    break;
                case EGameState.LevelResult:
                    ui.screen = EScreen.LevelResult;
                    break;
                case EGameState.InterstitialAdvert:
                    ui.screen = EScreen.AdsShow;
                    break;
            }
        }
        private void OnTownStateChanged(ETownState state)
        {
            switch (state)
            {
                case ETownState.Crash:
                    ui.screen = EScreen.LevelCrash;
                    break;
            }
        }
        private void OnLevelPauseChanged(EPauseState state)
        {
            var menu = menus.Get(EMenuID.LevelPause);
            switch (state)
            {
                case EPauseState.On:
                    menu.Open();
                    break;
                case EPauseState.Off:
                    menu.Close();
                    break;
            }
        }
        private void OnUIScreenChanged(EScreen id)
        {
            CloseAllGameMenus();
            OpenScreenMenus(id);
        }

        private void CloseAllGameMenus()
        {
            var list = menus.GetAllGames();
            foreach (var menu in list)
            {
                menu.Close();
            }
        }
        private void OpenScreenMenus(EScreen id)
        {
            var screen = screens.GetScreen(id);
            var list = menus.Get(screen.menus);
            foreach (var menu in list)
            {
                menu.Open();
            }
        }

        /// ======================================================================
    }
}