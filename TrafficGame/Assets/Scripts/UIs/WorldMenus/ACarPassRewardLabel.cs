﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;

namespace Alt3d.TrafficGame.UIs.Elements
{
	public class ACarPassRewardLabel : MonoBehaviour
	{
        /// ======================================================================

        [SerializeField] protected float delay = 1f;
        [SerializeField] protected float speed = 1f;
        [SerializeField] protected ALabel label;

        /// ======================================================================

        public void StartShow(string text)
        {
            label.text.text = text;
            gameObject.SetActive(true);
            StartCoroutine(DoUpdate());
        }
        private IEnumerator DoUpdate()
        {
            var counter = delay;
            while(counter > 0)
            {
                transform.position += Vector3.up * speed * Time.deltaTime;

                counter -= Time.deltaTime;
                yield return null;
            }

            Destroy(gameObject);
        }

        /// ======================================================================
    }
}