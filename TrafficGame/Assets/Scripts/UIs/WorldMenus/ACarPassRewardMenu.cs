﻿using System;
using System.Collections;
using System.Collections.Generic;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using UnityEngine;
using Alt3d.TrafficGame.UIs.Elements;

namespace Alt3d.TrafficGame.UIs.Menus
{
	public class ACarPassRewardMenu : MonoBehaviour
	{
        /// ======================================================================

        [SerializeField] protected GameObject templateParent;
        [SerializeField] protected GameObject normalRewardPrefab;
        [SerializeField] protected GameObject extraRewardPrefab;
        [SerializeField] protected Vector3 offset;

        private ICarsEvents carsEvents;
        private ICarDescsRepository carDescs;
        private ITown town;
        private ILevel level;

        /// ======================================================================

        private void Awake()
        {
            carsEvents = AServices.Get<ICarsEvents>();
            carDescs = AServices.Get<ICarDescsRepository>();
            town = AServices.Get<ITown>();
            level = AServices.Get<ILevel>();
            templateParent.SetActive(false);
        }
        private void OnEnable()
        {
            carsEvents.CarPassed += OnCarPasssed;    
        }
        private void OnDisable()
        {
            carsEvents.CarPassed -= OnCarPasssed;
        }

        private void OnCarPasssed(ICar car)
        {
            if (level.state == ELevelState.Play)
                if (town.state == ETownState.Play)
                    AddRewardLabel(car);
        }
        private void AddRewardLabel(ICar car)
        {
            var go = Instantiate(GetRewardPrefab(car));
            go.transform.position = car.position + offset;
            go.transform.SetParent(transform, true);

            var reward = car.GetPassReward(carDescs);
            var label = go.GetComponent<ACarPassRewardLabel>();
            label.StartShow(reward.ToString());
        }
        private GameObject GetRewardPrefab(ICar car)
        {
            if (car.IsWasStop())
                return normalRewardPrefab;
            else
                return extraRewardPrefab;
        }

        /// ======================================================================
    }
}