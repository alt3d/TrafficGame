﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Alt3d.TrafficGame.UIs;

namespace Alt3d.TrafficGame
{
	public static class ZUIs 
	{
		/// ======================================================================

        public static void Init(IContainer container)
        {
            var ui = container.AddComponent<AUI>();
            container.Register<AUI>(ui);
            container.Register<IUI>(ui);

            var menus = container.AddComponent<AMenusRepository>();
            container.Register<AMenusRepository>(menus);
            container.Register<IMenusRepository>(menus);

            var screens = container.AddComponent<AScreensRepository>();
            container.Register<AScreensRepository>(screens);

            container.AddComponent<AUIsController>();
        }

		/// ======================================================================
	}
}

namespace Alt3d.TrafficGame
{
    /// ======================================================================

    public enum EMenuID
    {
        AppLaunch,
        GameMain,
        GameLaunch,
        LevelLaunch,
        LevelSelection,
        LevelMain,
        LevelResult,
        LevelPause,
        LevelCrash,
        GameSettings,
        CarsShop,
        AchivsShowcase,
        AchivUnlock,
        LevelStats,
        AdsBack,
        // =================
        DebugControls,
        DebugFpsCounter,
        DebugGod,
        DebugCoreInfo,
        DebugStatsInfo,
        DebugSettings
    }

    public enum EMenuKind
    {
        Game,
        Debug
    }

    public enum EMenuState
    {
        Open,
        Close
    }

    public enum EScreen
    {
        AppLaunch,
        AppQuit,
        GameLaunch,
        GameQuit,
        GameMain,
        LevelResult,
        LevelCrash,
        LevelSelection,
        LevelLaunch,
        LevelQuit,
        LevelPlay,
        AdsShow
    }

    /// ======================================================================
}