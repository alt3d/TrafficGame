﻿Shader "Traffic Game/Only Color" 
{
	Properties 
	{
		_Color ("Color", Color) = (1, 1, 1, 1)
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 150
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd 
		#include "TrafficGameLightning.cginc"

        /// ======================================================================

		fixed4 _Color;

        /// ======================================================================

		struct Input 
		{
			fixed4 color : COLOR;
		};

        /// ======================================================================

		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Albedo = CorrectGamma(_Color);
		}

        /// ======================================================================

		ENDCG
	}

	Fallback "Mobile/VertexLit"
}
