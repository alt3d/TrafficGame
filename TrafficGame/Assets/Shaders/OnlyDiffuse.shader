﻿Shader "Traffic Game/Only Diffuse" 
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 150
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd 
		#include "TrafficGameLightning.cginc"

        /// ======================================================================

		sampler2D _MainTex;

        /// ======================================================================

		struct Input 
		{
			fixed2 uv_MainTex;
		};

        /// ======================================================================

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 albedo = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = CorrectGamma(albedo);
		}

        /// ======================================================================

		ENDCG
	}

	Fallback "Mobile/VertexLit"
}
