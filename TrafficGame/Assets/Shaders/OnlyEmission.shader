﻿Shader "Traffic Game/Only Emission" 
{
	Properties 
	{
		_Color ("Color", Color) = (1, 1, 1, 1)
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 150
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd 
		#include "TrafficGameLightning.cginc"

        /// ======================================================================

		fixed4 _Color;
		fixed4 _Emission;
		sampler2D _MainTex;

        /// ======================================================================

		struct Input 
		{
			fixed4 color : COLOR;
		};

        /// ======================================================================

		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Emission = _Color;
		}

        /// ======================================================================

		ENDCG
	}

	Fallback "Mobile/VertexLit"
}
