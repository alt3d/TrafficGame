#ifndef CUSTOM_LIGHT_TG
#define CUSTOM_LIGHT_TG

inline float4 CorrectGamma(float4 src)
{
	return pow(src, 1.2);
}

#endif
