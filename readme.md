**Traffic Control**

Smash as many things as you can with Traffic Control, the addictive car crash game that your friends will want to play! 
Earn achievements from most spectacular car crashes. 
Easy to play but hard to master gameplay means you will be coming back again and again for one more crash.

**Links**

Available on [Google Play](https://play.google.com/store/apps/details?id=com.alt3d.TrafficGame&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)

**Screens**

![](http://alt3d.ru/stuff/traffic-game/traffic-game-1.jpg)

![](http://alt3d.ru/stuff/traffic-game/traffic-game-2.jpg)

![](http://alt3d.ru/stuff/traffic-game/traffic-game-3.jpg)

![](http://alt3d.ru/stuff/traffic-game/traffic-game-4.jpg)